<?php

$installer = $this;
$installer->startSetup();

$prefix = Mage::getConfig()->getTablePrefix();
$qm3k_cache_clear = $prefix."quanta_monitor_cache_clear_event";

$installer->run("CREATE TABLE `$qm3k_cache_clear` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `type` varchar(32) NOT NULL COMMENT 'Type',
        `subtype` varchar(32) NOT NULL COMMENT 'Subtype',
        `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date',
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='$qm3k_cache_clear';"
    );

$installer->endSetup();
