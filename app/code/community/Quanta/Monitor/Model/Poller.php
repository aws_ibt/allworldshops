<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Monitor

class Quanta_Monitor_Model_Poller {

  protected $to = null;
  protected $from = null;
  protected $history = null;

  public function run($from, $to) {
    $this->to = $to->getIso();
    $this->from = $from->getIso();
    $this->history = array('stores' => array(), 'events' => array());
    foreach (array('orders', 'carts', 'reindex_events', 'cache_clear_events') as $item)
      $this->{"poll_$item"}();
    return array('history' => $this->history);
  }

  protected function add_event_history($item, $data) {
    $this->history['events'][$item] = $data;
  }

  protected function add_stores_history($store_id, $item, $data) {
    $this->history['stores'][$store_id == null ? "null" : $store_id][$item] = $data;
  }

  protected function fetch($model, $field) {
    return Mage::getModel($model)->getCollection()->getConnection()
      ->fetchAll(Mage::getModel($model)->getCollection()
      ->addFieldToSelect('store_id')->addFieldToSelect($field)->getSelect()
      ->columns("sum(grand_total) AS total, count(1) AS count")
      ->where('created_at > ?', $this->from)->where('created_at <= ?', $this->to)
      ->group('store_id'));
  }

  protected function add_stores_count_and_total_items($item, $row) {
    $this->add_stores_history($row['store_id'], $item . '_count', (int)$row['count']);
    $this->add_stores_history($row['store_id'], $item . '_total', (float)$row['total']);
  }

  protected function poll_orders() {
    foreach ($this->fetch('sales/order', 'status') as $row)
      $this->add_stores_count_and_total_items(in_array($row['status'],
        Mage::helper('quanta_monitor')->excluded_order_statuses()) ?
        "unpaid_orders" : "paid_orders", $row);
  }

  protected function poll_carts() {
    foreach ($this->fetch('sales/quote', 'is_active') as $row)
      $this->add_stores_count_and_total_items($row['is_active'] == '1' ?
        "active_carts" : "inactive_carts", $row);
  }

  protected function fetch_reindex_events() {
    return Mage::getModel('index/event')->getCollection()->getConnection()
      ->fetchAll(Mage::getModel('index/event')->getCollection()
      ->addFieldToSelect('type')->addFieldToSelect('entity')->getSelect()
      ->columns('count(1) as count')->where('created_at > ?', $this->from)
      ->where('created_at <= ?', $this->to)->group('type')->group('entity'));
  }

  static function reindex_as_json($reindex) {
    return array('type' => $reindex['type'],
      'entity' => $reindex['entity'], 'count' => (int)$reindex['count']);
  }

  protected function poll_reindex_events() {
    if (count($reindexes = $this->fetch_reindex_events()) > 0)
      $this->add_event_history('reindex',
        array_map('self::reindex_as_json', $reindexes));
  }

  protected function fetch_cache_clear_events() {
    return Mage::getModel('quanta_monitor/cache_clear')->getCollection()
      ->getConnection()->fetchAll(Mage::getModel('quanta_monitor/cache_clear')
      ->getCollection()->addFieldToSelect('type')->addFieldToSelect('subtype')
      ->getSelect()->columns('count(1) as count')->where('created_at >= ?', $this->from)
      ->where('created_at <= ?', $this->to)->group('type')->group('subtype'));
  }

  static function cache_clear_as_json($cache_clear) {
    return array('type' => $cache_clear['type'],
      'subtype' => $cache_clear['subtype'], 'count' => (int)$cache_clear['count']);
  }

  protected function poll_cache_clear_events() {
    if (count($cache_clears = $this->fetch_cache_clear_events()) > 0)
      $this->add_event_history('cache_clear',
        array_map('self::cache_clear_as_json', $cache_clears));
  }
}
