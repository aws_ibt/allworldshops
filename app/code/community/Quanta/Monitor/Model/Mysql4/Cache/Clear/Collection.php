<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Monitor

class Quanta_Monitor_Model_Mysql4_Cache_Clear_Collection
extends Mage_Core_Model_Mysql4_Collection_Abstract {

  public function _construct() {
    parent::_construct();
    $this->_init('quanta_monitor/cache_clear');
  }
}
