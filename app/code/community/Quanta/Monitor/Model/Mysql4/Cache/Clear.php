<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Monitor

class Quanta_Monitor_Model_Mysql4_Cache_Clear extends Mage_Core_Model_Mysql4_Abstract {

  public function _construct() {
    $this->_init('quanta_monitor/cache_clear', 'id');
  }
}
