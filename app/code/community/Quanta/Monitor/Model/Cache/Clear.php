<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Monitor

class Quanta_Monitor_Model_Cache_Clear extends Mage_Core_Model_Abstract {

  public function _construct() {
    parent::_construct();
    $this->_init('quanta_monitor/cache_clear');
  }

  public function record($type, $subtype) {
    try {
      if (!Mage::helper('quanta_monitor')->enabled()) return $this;
      $clock = new Zend_Date(Zend_Date::now()->getTimestamp() - (60 * 60 * 24 * 7), Zend_Date::TIMESTAMP);
      $this->setCreatedAt(Zend_Date::now()->getIso())->setType($type)->setSubtype($subtype)->save();
      Mage::getSingleton('core/resource')->getConnection('core_write')->query('DELETE FROM '
        . Mage::getSingleton('core/resource')->getTableName('quanta_monitor/cache_clear')
        . ' WHERE created_at < ?', $clock->getIso());
    } catch (Exception $e) {}
    return $this;
  }
}
