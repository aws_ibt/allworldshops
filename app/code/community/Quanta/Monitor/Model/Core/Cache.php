<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Monitor

class Quanta_Monitor_Model_Core_Cache extends Mage_Core_Model_Cache {

  public function flush() {
    Mage::getModel('quanta_monitor/cache_clear')->record('flush', null);
    return parent::flush();
  }

  public function cleanType($typeCode) {
    Mage::getModel('quanta_monitor/cache_clear')->record('clean', $typeCode);
    return parent::cleanType($typeCode);
  }
}
