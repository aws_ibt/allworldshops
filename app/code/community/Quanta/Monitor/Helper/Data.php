<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Monitor

class Quanta_Monitor_Helper_Data extends Mage_Core_Helper_Abstract {

  protected $excluded_order_statuses = null;

  function enabled() {
    return Mage::app()->getStore()->getConfig('quanta_monitor/general/enabled');
  }

  function excluded_order_statuses() {
    if (!$this->excluded_order_statuses)
      $this->excluded_order_statuses = explode(',',
        Mage::app()->getStore()->getConfig('quanta_monitor/orders/statuses_excluded'));
    return $this->excluded_order_statuses;
  }
}
