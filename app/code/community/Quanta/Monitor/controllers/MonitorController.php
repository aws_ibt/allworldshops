<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Monitor

class Quanta_Monitor_MonitorController extends Mage_Core_Controller_Front_Action {

  public function runAction() {
    $data = array('status' => 'enabled');
    $this->getResponse()->clearBody();
    $this->getResponse()->clearHeaders();
    $this->getResponse()->setHeader('Content-Type', 'application/json');
    if (!Mage::helper('quanta_core')->enabled())
      $data['status'] = 'core disabled';
    else if (!Mage::helper('quanta_monitor')->enabled())
      $data['status'] = 'monitor disabled';
    else {
      $to = $this->getRequest()->getParam('to');
      $from = $this->getRequest()->getParam('from');
      $from = new Zend_Date($from ? $from : Zend_Date::now()->getTimestamp() - (60 * 5), Zend_Date::TIMESTAMP);
      $to = $to ? new Zend_Date($to, Zend_Date::TIMESTAMP) : Zend_Date::now();
      if ($to->getTimestamp() - $from->getTimestamp() > 60 * 30)
        $from = new Zend_Date($to->getTimestamp() - (60 * 30), Zend_Date::TIMESTAMP);
      $data = array_merge($data, Mage::getModel('quanta_monitor/poller')->run($from, $to));
    }
    $this->getResponse()->appendBody(Mage::helper('quanta_core')->encrypt_data(
      json_encode(array_merge(Mage::helper('quanta_core')->get_all_versions(), $data))));
    return $this;
  }
}
