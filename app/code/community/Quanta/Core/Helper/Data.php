<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Core

class Quanta_Core_Helper_Data extends Mage_Core_Helper_Abstract {

	function enabled() {
		return Mage::app()->getStore()->getConfig('quanta_core/settings/enabled');
	}

	function get_public_key() {
		return Mage::app()->getStore()->getConfig('quanta_core/settings/public_key');
	}

	function get_quanta_version() {
		$version = (array)Mage::getConfig()->getModuleConfig("Quanta_Core")->version;
		return $version[0];
	}

	function get_all_versions() {
		return array('version' => $this->get_quanta_version(),
			'magento_version' => Mage::getversion(), 'php_version' => phpversion());
	}


	function encrypt_data($data) {
		$key_string = $this->get_public_key();
		if (!function_exists('openssl_public_encrypt') ||
				!function_exists('openssl_random_pseudo_bytes') ||
	    	!function_exists('openssl_encrypt') ||
				version_compare(PHP_VERSION, "5.3.3") < 0 ||
				$key_string == "")
			return $data;
		$public_key = openssl_get_publickey($key_string);
		$password = openssl_random_pseudo_bytes(42);
		$iv = openssl_random_pseudo_bytes(16);
  	$encrypted_password = "";
		$encrypted_iv = "";
		if (!openssl_public_encrypt($password, $encrypted_password, $public_key) ||
	    !openssl_public_encrypt($iv, $encrypted_iv, $public_key))
			return openssl_error_string();
		$encrypted_data = openssl_encrypt($data, 'aes-128-cbc', $password, 1, $iv);
		return base64_encode($encrypted_password) . "#" .
			base64_encode($encrypted_iv) . "#" . base64_encode($encrypted_data);
	}
}
