<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Profiler

class Quanta_Profiler_Helper_Data extends Mage_Core_Helper_Abstract {

  function enabled() {
    return Mage::app()->getStore()->getConfig('quanta_profiler/general/enabled');
  }
}
