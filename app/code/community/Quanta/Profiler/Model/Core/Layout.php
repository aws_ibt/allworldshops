<?php

// @copyright	Quanta 2013
// @author 		Kraoz
// @category 	Quanta
// @package 	Quanta_Profiler

class Quanta_Profiler_Model_Core_Layout extends Mage_Core_Model_Layout {
	public function createBlock($type, $name='', array $attributes = array()) {
		Mage::dispatchEvent('core_layout_block_create_before', array('block_name' => $name));
		return parent::createBlock($type, $name, $attributes);
	}
}
