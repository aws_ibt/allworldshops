<?php

// @copyright   Quanta 2013
// @author      Kraoz
// @category    Quanta
// @package     Quanta_Profiler

class Quanta_Profiler_Model_Profiler {

  public function record_timer($property) {
    if ($this->inactive) return;
    $timestamp = microtime(true);
    $this->timers[$property] = ($timestamp - $this->current_timer_timestamp) * 1000;
    $this->current_timer_timestamp = $timestamp;
  }

  private function init_profiling() {
    $request = Mage::app()->getRequest();
    $this->inactive = !(Mage::helper('quanta_core')->enabled() &&
      Mage::helper('quanta_profiler')->enabled() &&
      ($request->getHeader('X-QM3K-Profiler') == "true" ||
      $request->getHeader('X-Quanta-Profiler') == "true"));
    Varien_Profiler::$quanta_profiler = $this->inactive ? null : $this;
    if ($this->inactive) return;
    $this->data = array_merge(Mage::helper('quanta_core')->get_all_versions(), array(
      'blocks' => array(),
      'timestamp' => microtime(),
      'timers' => array(0, 0, 0, 0, 0, 0, 0)
    ));
    $this->current_block_timestamp = null;
    $this->current_timer_timestamp = Varien_Profiler::$mage_start;
    $this->timers = &$this->data['timers'];
    $this->blocks = &$this->data['blocks'];
  }

  public function timer_predispatch() {
    $this->init_profiling();
    $this->record_timer(0);
  }

  public function timer_layout_load_before() {
    $this->record_timer(1);
  }

  public function timer_layout_load_after() {
    $this->record_timer(2);
  }

  public function timer_layout_render_before() {
    $this->record_timer(3);
  }

  public function timer_layout_render_after() {
    $this->record_timer(4);
  }

  public function timer_postdispatch() {
    $this->record_timer(5);
  }

  public function timer_send_response_before() {
    if ($this->inactive) return;
    $this->record_timer(6);
    $encrypted_data = Mage::helper('quanta_core')->encrypt_data(json_encode($this->data));
    Mage::app()->getResponse()->setBody(str_replace("</body>",
      "<div id='--quanta--profiler' style='display:none'>" . $encrypted_data . "</div></body>",
      Mage::app()->getResponse()->getBody()
    ));
  }

  private function init_block($block) {
    return array(1, get_class($block),
      function_exists('mageFindClassFile') ?
        str_replace(Mage::getBaseDir(), '', mageFindClassFile(get_class($block))) : null,
      method_exists($block, "getTemplate") ? $block->getTemplate() : null,
      method_exists($block, "getTemplateFile") ?
        (str_replace(Mage::getBaseDir(), '',
          Mage::getBaseDir('design')) . '/' . $block->getTemplateFile()) : null, 0);
  }

  private function add_timer_to_block($observer, $n, $timer) {
    $block = $observer->getEvent()->getBlock();
    $block_name = $block->getNameInLayout();
    if (!isset($this->blocks[$block_name]))
      $this->blocks[$block_name] = $this->init_block($block);
    else if ($n == 5)
      $this->blocks[$block_name][0] += 1;
    if (!isset($this->blocks[$block_name][$n]))
      $this->blocks[$block_name][$n] = 0;
    $this->blocks[$block_name][$n] += $timer;
  }

  public function timer_block_create_before($observer) {
    if ($this->inactive) return;
    $this->current_block_timestamp = microtime(true);
  }

  public function timer_block_create_after($observer) {
    if ($this->inactive) return;
    $this->add_timer_to_block($observer, 5,
      (microtime(true) - $this->current_block_timestamp) * 1000);
  }

  public function timer_block_render_before($observer) {
    if ($this->inactive) return;
    $this->add_timer_to_block($observer, 6, microtime(true));
  }

  public function timer_block_render_after($observer) {
    if ($this->inactive) return;
    $block = $observer->getEvent()->getBlock();
    $block_values = &$this->blocks[$block->getNameInLayout()];
    $block_values[6] = (microtime(true) - $block_values[6]) * 1000;
    if (($parent = $block->getParentBlock()))
      $block_values[7] = $parent->getNameInLayout();
  }
}
