<?php
/**
 * Innoexts
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the InnoExts Commercial License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://innoexts.com/commercial-license-agreement
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@innoexts.com so we can send you a copy immediately.
 * 
 * @category    Innoexts
 * @package     Innoexts_AdvancedPricingEnterprise
 * @copyright   Copyright (c) 2014 Innoexts (http://www.innoexts.com)
 * @license     http://innoexts.com/commercial-license-agreement  InnoExts Commercial License
 */

/**
 * Full page cache cookie
 * 
 * @category   Innoexts
 * @package    Innoexts_AdvancedPricingEnterprise
 * @author     Innoexts Team <developers@innoexts.com>
 */
class Innoexts_AdvancedPricingEnterprise_Model_Enterprise_Pagecache_Cookie 
    extends Enterprise_PageCache_Model_Cookie 
{
    /**
     * Customer address cookie
     */
    const COOKIE_CUSTOMER_ADDRESS               = 'CUSTOMER_ADDRESS';
    /**
     * Get advanced pricing helper
     * 
     * @return Innoexts_AdvancedPricing_Helper_Data
     */
    protected function getAdvancedPricingHelper()
    {
        return Mage::helper('advancedpricing');
    }
    /**
     * Update customer cookies
     * 
     * @return self
     */
    public function updateCustomerCookies()
    {
        $helper                         = $this->getAdvancedPricingHelper();
        $customerHelper                 = $helper->getCoreHelper()
            ->getCustomerHelper();
        $customerId                     = $customerHelper->getCustomerId();
        $customerGroupId                = $customerHelper->getCustomerGroupId();
        $customerAddress                = $helper->getCustomerLocatorHelper()
            ->getCustomerAddress();
        $customerAddressHash            = $helper->getCoreHelper()
            ->getAddressHelper()
            ->getHash($customerAddress);
        
        if (!$customerId || is_null($customerGroupId)) {
            $customerCookies        = new Varien_Object();
            Mage::dispatchEvent('update_customer_cookies', array('customer_cookies' => $customerCookies));
            if (!$customerId) {
                $customerId             = $customerCookies->getCustomerId();
            }
            if (is_null($customerGroupId)) {
                $customerGroupId        = $customerCookies->getCustomerGroupId();
            }
        }
        if ($customerId && !is_null($customerGroupId)) {
            $this->setObscure(self::COOKIE_CUSTOMER, 'customer_' . $customerId);
            $this->setObscure(self::COOKIE_CUSTOMER_GROUP, 'customer_group_' . $customerGroupId);
            if ($customerHelper->isLoggedIn()) {
                $this->setObscure(self::COOKIE_CUSTOMER_LOGGED_IN, 'customer_logged_in_' . $customerHelper->isLoggedIn());
            } else {
                $this->delete(self::COOKIE_CUSTOMER_LOGGED_IN);
            }
        } else {
            $this->delete(self::COOKIE_CUSTOMER);
            $this->delete(self::COOKIE_CUSTOMER_GROUP);
            $this->delete(self::COOKIE_CUSTOMER_LOGGED_IN);
        }
        
        $this->setObscure(self::COOKIE_CUSTOMER_ADDRESS, 'customer_address_'.$customerAddressHash);
        
        return $this;
    }
}