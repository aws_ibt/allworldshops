<?php
/**
 * Innoexts
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the InnoExts Commercial License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://innoexts.com/commercial-license-agreement
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@innoexts.com so we can send you a copy immediately.
 * 
 * @category    Innoexts
 * @package     Innoexts_AdvancedPricingEnterprise
 * @copyright   Copyright (c) 2014 Innoexts (http://www.innoexts.com)
 * @license     http://innoexts.com/commercial-license-agreement  InnoExts Commercial License
 */

/**
 * Full refresh price index
 * 
 * @category   Innoexts
 * @package    Innoexts_AdvancedPricingEnterprise
 * @author     Innoexts Team <developers@innoexts.com>
 */
abstract class Innoexts_AdvancedPricingEnterprise_Model_Enterprise_Catalog_Index_Action_Product_Price_Abstract 
    extends Enterprise_Catalog_Model_Index_Action_Product_Price_Abstract 
{
    /**
     * Get advanced pricing helper
     * 
     * @return Innoexts_AdvancedPricing_Helper_Data
     */
    protected function getAdvancedPricingHelper()
    {
        return Mage::helper('advancedpricing');
    }
    /**
     * Get price indexer helper
     * 
     * @return Innoexts_AdvancedPricing_Helper_Catalog_Product_Price_Indexer
     */
    protected function getProductPriceIndexerHelper()
    {
        return $this->getAdvancedPricingHelper()
            ->getProductPriceIndexerHelper();
    }
    /**
     * Get version helper
     * 
     * @return Innoexts_Core_Helper_Version
     */
    protected function getVersionHelper()
    {
        return $this->getAdvancedPricingHelper()
            ->getVersionHelper();
    }
    /**
     * Prepare tier price index table
     * 
     * @param int|array $entityIds
     * 
     * @return self
     */
    protected function _prepareTierPriceIndex($entityIds = null)
    {
        $indexerHelper      = $this->getProductPriceIndexerHelper();
        
        $adapter            = $this->_connection;
        $table              = $this->_getTable('catalog/product_index_tier_price');
        $this->_emptyTable($table);
        
        $currency           = $indexerHelper->getCurrencyExpr('cw.website_id');
        $rate               = new Zend_Db_Expr('cr.rate');
        $price              = new Zend_Db_Expr('IF (tp.website_id=0, ROUND(tp.value * cwd.rate, 4), tp.value)');
        $price              = new Zend_Db_Expr("IF (
            (tp.currency IS NOT NULL) AND (tp.currency <> ''), 
            ROUND({$price} / {$rate}, 4), 
            {$price}
        )");
            
        $columns = array(
            'entity_id'             => new Zend_Db_Expr('tp.entity_id'), 
            'customer_group_id'     => new Zend_Db_Expr('cg.customer_group_id'), 
            'website_id'            => new Zend_Db_Expr('cw.website_id'), 
            'currency'              => $currency, 
            'store_id'              => new Zend_Db_Expr('cs.store_id'), 
            'min_price'             => new Zend_Db_Expr("MIN({$price})"), 
        );
        
        $group = array(
            'tp.entity_id', 
            'cg.customer_group_id', 
            'cw.website_id', 
            $currency, 
            'cs.store_id', 
        );
        
        $select = $adapter->select()
            ->from(
                array('tp' => $this->_getTable(array('catalog/product', 'tier_price'))), 
                array()
            )
            ->join(
                array('cg' => $this->_getTable('customer/customer_group')), 
                '(tp.all_groups = 1) OR ((tp.all_groups = 0) AND tp.customer_group_id = cg.customer_group_id)', 
                array()
            )
            ->join(
                array('cw' => $this->_getTable('core/website')), 
                '(tp.website_id = 0) OR (tp.website_id = cw.website_id)', 
                array()
            )
            ->join(
                array('cwd' => $this->_getTable('catalog/product_index_website')), 
                'cw.website_id = cwd.website_id', 
                array()
            )
            ->join(
                array('csg' => $this->_getTable('core/store_group')), 
                'csg.website_id = cw.website_id', 
                array()
            )
            ->joinLeft(
                array('cr' => $this->_getTable('directory/currency_rate')), 
                implode(' AND ', array(
                    "(cr.currency_from = {$indexerHelper->getBaseCurrencyExpr('cw.website_id')})", 
                    "((tp.currency IS NULL) OR (tp.currency = cr.currency_to))"
                )), 
                array()
            )
            ->join(
                array('cs' => $this->_getTable('core/store')), 
                '(csg.group_id = cs.group_id) AND ((tp.store_id = 0) OR (tp.store_id = cs.store_id))', 
                array()
            )
            ->where(
                implode(' AND ', array(
                    '(cw.website_id != 0)', 
                    '(cs.store_id != 0)', 
                ))
            )
            ->columns($columns)
            ->group($group);
        
        if (!empty($entityIds)) {
            $select->where('tp.entity_id IN(?)', $entityIds);
        }
        $query = $select->insertFromSelect($table);
        $adapter->query($query);
        return $this;
    }
    /**
     * Prepare group price index table
     * 
     * @param int|array $entityIds
     * 
     * @return self
     */
    protected function _prepareGroupPriceIndex($entityIds = null)
    {
        $indexerHelper      = $this->getProductPriceIndexerHelper();
        
        $adapter            = $this->_connection;
        $table              = $this->_getTable('catalog/product_index_group_price');
        $this->_emptyTable($table);
        
        $currency           = $indexerHelper->getCurrencyExpr('cw.website_id');
        $rate               = new Zend_Db_Expr('cr.rate');
        $price              = new Zend_Db_Expr("IF (gp.website_id=0, ROUND(gp.value * cwd.rate, 4), gp.value)");
        $price              = new Zend_Db_Expr("IF (
            (gp.currency IS NOT NULL) AND (gp.currency <> ''), 
            ROUND({$price} / {$rate}, 4), 
            {$price}
        )");
        
        $columns = array(
            'entity_id'             => new Zend_Db_Expr('gp.entity_id'), 
            'customer_group_id'     => new Zend_Db_Expr('cg.customer_group_id'), 
            'website_id'            => new Zend_Db_Expr('cw.website_id'), 
            'currency'              => $currency, 
            'store_id'              => new Zend_Db_Expr('cs.store_id'), 
            'min_price'             => new Zend_Db_Expr("MIN({$price})"), 
        );
        
        $group = array(
            'gp.entity_id', 
            'cg.customer_group_id', 
            'cw.website_id', 
            $currency, 
            'cs.store_id', 
        );
        
        $select = $adapter->select()
            ->from(
                array('gp' => $this->_getTable(array('catalog/product', 'group_price'))), 
                array()
            )
            ->join(
                array('cg' => $this->_getTable('customer/customer_group')), 
                '(gp.all_groups = 1) OR ((gp.all_groups = 0) AND (gp.customer_group_id = cg.customer_group_id))', 
                array()
            )
            ->join(
                array('cw' => $this->_getTable('core/website')), 
                '(gp.website_id = 0) OR (gp.website_id = cw.website_id)', 
                array()
            )
            ->join(
                array('cwd' => $this->_getTable('catalog/product_index_website')), 
                'cw.website_id = cwd.website_id', 
                array()
            )
            ->join(
                array('csg' => $this->_getTable('core/store_group')), 
                'csg.website_id = cw.website_id', 
                array()
            )
            ->joinLeft(
                array('cr' => $this->_getTable('directory/currency_rate')), 
                implode(' AND ', array(
                    "(cr.currency_from = {$indexerHelper->getBaseCurrencyExpr('cw.website_id')})", 
                    "((gp.currency IS NULL) OR (gp.currency = cr.currency_to))"
                )), 
                array()
            )
            ->join(
                array('cs' => $this->_getTable('core/store')), 
                '(csg.group_id = cs.group_id) AND ((gp.store_id = 0) OR (gp.store_id = cs.store_id))', 
                array()
            )
            ->where(
                implode(' AND ', array(
                    '(cw.website_id != 0)', 
                    '(cs.store_id != 0)', 
                ))
            )
            ->columns($columns)
            ->group($group);
        
        if (!empty($entityIds)) {
            $select->where('gp.entity_id IN(?)', $entityIds);
        }
        $query = $select->insertFromSelect($table);
        $adapter->query($query);
        return $this;
    }
}