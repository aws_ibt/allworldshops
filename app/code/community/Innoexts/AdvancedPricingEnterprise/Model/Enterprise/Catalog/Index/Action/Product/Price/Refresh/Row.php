<?php
/**
 * Innoexts
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the InnoExts Commercial License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://innoexts.com/commercial-license-agreement
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@innoexts.com so we can send you a copy immediately.
 * 
 * @category    Innoexts
 * @package     Innoexts_AdvancedPricingEnterprise
 * @copyright   Copyright (c) 2014 Innoexts (http://www.innoexts.com)
 * @license     http://innoexts.com/commercial-license-agreement  InnoExts Commercial License
 */

/**
 * Refresh category flat index row
 *
 * @category   Innoexts
 * @package    Innoexts_AdvancedPricingEnterprise
 * @author     Innoexts Team <developers@innoexts.com>
 */
class Innoexts_AdvancedPricingEnterprise_Model_Enterprise_Catalog_Index_Action_Product_Price_Refresh_Row 
    extends Enterprise_Catalog_Model_Index_Action_Product_Price_Refresh_Row 
{
    /**
     * Get advanced pricing helper
     * 
     * @return Innoexts_AdvancedPricing_Helper_Data
     */
    protected function getAdvancedPricingHelper()
    {
        return Mage::helper('advancedpricing');
    }
    /**
     * Get price indexer helper
     * 
     * @return Innoexts_AdvancedPricing_Helper_Catalog_Product_Price
     */
    protected function getProductPriceHelper()
    {
        return $this->getAdvancedPricingHelper()
            ->getProductPriceHelper();
    }
    /**
     * Get price indexer helper
     * 
     * @return Innoexts_AdvancedPricing_Helper_Catalog_Product_Price_Indexer
     */
    protected function getProductPriceIndexerHelper()
    {
        return $this->getAdvancedPricingHelper()
            ->getProductPriceIndexerHelper();
    }
    /**
     * Get version helper
     * 
     * @return Innoexts_Core_Helper_Version
     */
    protected function getVersionHelper()
    {
        return $this->getAdvancedPricingHelper()
            ->getVersionHelper();
    }
    /**
     * Prepare tier price index table
     * 
     * @param int|array $entityIds
     * 
     * @return self
     */
    protected function _prepareTierPriceIndex($entityIds = null)
    {
        $indexerHelper      = $this->getProductPriceIndexerHelper();
        
        $adapter            = $this->_connection;
        $table              = $this->_getTable('catalog/product_index_tier_price');
        $this->_emptyTable($table);
        
        $currency           = $indexerHelper->getCurrencyExpr('cw.website_id');
        $rate               = new Zend_Db_Expr('cr.rate');
        $price              = new Zend_Db_Expr('IF (tp.website_id=0, ROUND(tp.value * cwd.rate, 4), tp.value)');
        $price              = new Zend_Db_Expr("IF (
            (tp.currency IS NOT NULL) AND (tp.currency <> ''), 
            ROUND({$price} / {$rate}, 4), 
            {$price}
        )");
            
        $columns = array(
            'entity_id'             => new Zend_Db_Expr('tp.entity_id'), 
            'customer_group_id'     => new Zend_Db_Expr('cg.customer_group_id'), 
            'website_id'            => new Zend_Db_Expr('cw.website_id'), 
            'currency'              => $currency, 
            'store_id'              => new Zend_Db_Expr('cs.store_id'), 
            'min_price'             => new Zend_Db_Expr("MIN({$price})"), 
        );
        
        $group = array(
            'tp.entity_id', 
            'cg.customer_group_id', 
            'cw.website_id', 
            $currency, 
            'cs.store_id', 
        );
        
        $select = $adapter->select()
            ->from(
                array('tp' => $this->_getTable(array('catalog/product', 'tier_price'))), 
                array()
            )
            ->join(
                array('cg' => $this->_getTable('customer/customer_group')), 
                '(tp.all_groups = 1) OR ((tp.all_groups = 0) AND tp.customer_group_id = cg.customer_group_id)', 
                array()
            )
            ->join(
                array('cw' => $this->_getTable('core/website')), 
                '(tp.website_id = 0) OR (tp.website_id = cw.website_id)', 
                array()
            )
            ->join(
                array('cwd' => $this->_getTable('catalog/product_index_website')), 
                'cw.website_id = cwd.website_id', 
                array()
            )
            ->join(
                array('csg' => $this->_getTable('core/store_group')), 
                'csg.website_id = cw.website_id', 
                array()
            )
            ->joinLeft(
                array('cr' => $this->_getTable('directory/currency_rate')), 
                implode(' AND ', array(
                    "(cr.currency_from = {$indexerHelper->getBaseCurrencyExpr('cw.website_id')})", 
                    "((tp.currency IS NULL) OR (tp.currency = cr.currency_to))"
                )), 
                array()
            )
            ->join(
                array('cs' => $this->_getTable('core/store')), 
                '(csg.group_id = cs.group_id) AND ((tp.store_id = 0) OR (tp.store_id = cs.store_id))', 
                array()
            )
            ->where(
                implode(' AND ', array(
                    '(cw.website_id != 0)', 
                    '(cs.store_id != 0)', 
                ))
            )
            ->columns($columns)
            ->group($group);
        
        if (!empty($entityIds)) {
            $select->where('tp.entity_id IN(?)', $entityIds);
        }
        $query = $select->insertFromSelect($table);
        $adapter->query($query);
        return $this;
    }
    /**
     * Prepare group price index table
     * 
     * @param int|array $entityIds
     * 
     * @return self
     */
    protected function _prepareGroupPriceIndex($entityIds = null)
    {
        $indexerHelper      = $this->getProductPriceIndexerHelper();
        
        $adapter            = $this->_connection;
        $table              = $this->_getTable('catalog/product_index_group_price');
        $this->_emptyTable($table);
        
        $currency           = $indexerHelper->getCurrencyExpr('cw.website_id');
        $rate               = new Zend_Db_Expr('cr.rate');
        $price              = new Zend_Db_Expr("IF (gp.website_id=0, ROUND(gp.value * cwd.rate, 4), gp.value)");
        $price              = new Zend_Db_Expr("IF (
            (gp.currency IS NOT NULL) AND (gp.currency <> ''), 
            ROUND({$price} / {$rate}, 4), 
            {$price}
        )");
        
        $columns = array(
            'entity_id'             => new Zend_Db_Expr('gp.entity_id'), 
            'customer_group_id'     => new Zend_Db_Expr('cg.customer_group_id'), 
            'website_id'            => new Zend_Db_Expr('cw.website_id'), 
            'currency'              => $currency, 
            'store_id'              => new Zend_Db_Expr('cs.store_id'), 
            'min_price'             => new Zend_Db_Expr("MIN({$price})"), 
        );
        
        $group = array(
            'gp.entity_id', 
            'cg.customer_group_id', 
            'cw.website_id', 
            $currency, 
            'cs.store_id', 
        );
        
        $select = $adapter->select()
            ->from(
                array('gp' => $this->_getTable(array('catalog/product', 'group_price'))), 
                array()
            )
            ->join(
                array('cg' => $this->_getTable('customer/customer_group')), 
                '(gp.all_groups = 1) OR ((gp.all_groups = 0) AND (gp.customer_group_id = cg.customer_group_id))', 
                array()
            )
            ->join(
                array('cw' => $this->_getTable('core/website')), 
                '(gp.website_id = 0) OR (gp.website_id = cw.website_id)', 
                array()
            )
            ->join(
                array('cwd' => $this->_getTable('catalog/product_index_website')), 
                'cw.website_id = cwd.website_id', 
                array()
            )
            ->join(
                array('csg' => $this->_getTable('core/store_group')), 
                'csg.website_id = cw.website_id', 
                array()
            )
            ->joinLeft(
                array('cr' => $this->_getTable('directory/currency_rate')), 
                implode(' AND ', array(
                    "(cr.currency_from = {$indexerHelper->getBaseCurrencyExpr('cw.website_id')})", 
                    "((gp.currency IS NULL) OR (gp.currency = cr.currency_to))"
                )), 
                array()
            )
            ->join(
                array('cs' => $this->_getTable('core/store')), 
                '(csg.group_id = cs.group_id) AND ((gp.store_id = 0) OR (gp.store_id = cs.store_id))', 
                array()
            )
            ->where(
                implode(' AND ', array(
                    '(cw.website_id != 0)', 
                    '(cs.store_id != 0)', 
                ))
            )
            ->columns($columns)
            ->group($group);
        
        if (!empty($entityIds)) {
            $select->where('gp.entity_id IN(?)', $entityIds);
        }
        $query = $select->insertFromSelect($table);
        $adapter->query($query);
        return $this;
    }
    /**
     * Prepare compound price index table
     * 
     * @param int|array $entityIds
     * @param string $attributeCode
     * @param string $table
     * @param string $indexTable
     * 
     * @return self
     */
    protected function __prepareCompoundPriceIndex($entityIds = null, $attributeCode, $table, $indexTable)
    {
        $priceHelper        = $this->getProductPriceHelper();
        $indexerHelper      = $this->getProductPriceIndexerHelper();
        $adapter            = $this->_connection;
        
        $select             = $adapter->select()
            ->from(
                array('e' => $this->_getTable('catalog/product')), 
                array()
            )
            ->join(
                array('cw' => $this->_getTable('core/website')), 
                '', 
                array()
            )
            ->join(
                array('cwd' => $this->_getTable('catalog/product_index_website')), 
                '(cw.website_id = cwd.website_id)', 
                array()
            )
            ->join(
                array('csg' => $this->_getTable('core/store_group')), 
                '(csg.website_id = cw.website_id) AND (cw.website_id != 0)', 
                array()
            )
            ->join(
                array('cs' => $this->_getTable('core/store')), 
                '(csg.group_id = cs.group_id) AND (cs.store_id != 0)', 
                array()
            )
            ->join(
                array('pw' => $this->_getTable('catalog/product_website')),
                '(pw.product_id = e.entity_id) AND (pw.website_id = cw.website_id)', 
                array()
            )
            ->joinLeft(
                array('cr' => $this->_getTable('directory/currency_rate')), 
                "(cr.currency_from = {$indexerHelper->getBaseCurrencyExpr('cw.website_id')})", 
                array()
            );
        
        $price = $indexerHelper->addAttributeToSelect($adapter, $select, $attributeCode, 'e.entity_id', 'cs.store_id');
        $select->joinLeft(array('ccgp' => $table), implode(' AND ', array(
            '(ccgp.product_id = e.entity_id)', 
            '(ccgp.currency = cr.currency_to)', 
            '(ccgp.store_id = 0)', 
        )), array());
        if (!$priceHelper->isGlobalScope()) {
            if ($priceHelper->isWebsiteScope()) {
                $select->joinLeft(array('ccp' => $table), implode(' AND ', array(
                    '(ccp.product_id = e.entity_id)', 
                    '(ccp.currency = cr.currency_to)', 
                    '(csg.group_id = cw.default_group_id) AND (ccp.store_id = csg.default_store_id)', 
                )), array());
            } else {
                $select->joinLeft(array('ccp' => $table), implode(' AND ', array(
                    '(ccp.product_id = e.entity_id)', 
                    '(ccp.currency = cr.currency_to)', 
                    '(ccp.store_id = cs.store_id)', 
                )), array());
            }
        }
        $rate       = new Zend_Db_Expr('cr.rate');
        if (!$priceHelper->isGlobalScope()) {
            $price = new Zend_Db_Expr("IF (
                ccp.price IS NOT NULL, 
                ROUND(ccp.price / {$rate}, 4), 
                IF (
                    ccgp.price IS NOT NULL, 
                    ROUND(ROUND(ccgp.price * cwd.rate, 4) / {$rate}, 4), 
                    {$price}
                )
            )");
        } else {
            $price = new Zend_Db_Expr("IF (
                ccgp.price IS NOT NULL, 
                ROUND(ROUND(ccgp.price * cwd.rate, 4) / {$rate}, 4), 
                {$price}
            )");
        }
        $currency   = $indexerHelper->getCurrencyExpr('cw.website_id');
        
        $columns    = array(
            'entity_id'         => new Zend_Db_Expr('e.entity_id'), 
            'currency'          => $currency, 
            'store_id'          => new Zend_Db_Expr('cs.store_id'), 
            'price'             => new Zend_Db_Expr($price), 
        );
        $group              = array(
            'e.entity_id', 
            'cr.currency_to', 
            'cs.store_id', 
        );
        
        $where              = '(cw.website_id <> 0)';
        $select->where($where)
            ->columns($columns)
            ->group($group);
        if (!empty($entityIds)) {
            $select->where('e.entity_id IN(?)', $entityIds);
        }
        $adapter->delete($indexTable);
        $query = $select->insertFromSelect($indexTable);
        
        $adapter->query($query);
        return $this;
    }
    /**
     * Prepare compound price index table
     * 
     * @param int|array $entityIds
     * 
     * @return self
     */
    protected function _prepareCompoundPriceIndex($entityIds = null)
    {
        return $this->__prepareCompoundPriceIndex(
            $entityIds, 
            'price', 
            $this->_getTable('catalog/product_compound_price'), 
            $this->getProductPriceIndexerHelper()->getCompoundPriceIndexTable()
        );
    }
    /**
     * Prepare compound special price index table
     *
     * @param int|array $entityIds the entity ids limitation
     * 
     * @return self
     */
    protected function _prepareCompoundSpecialPriceIndex($entityIds = null)
    {
        return $this->__prepareCompoundPriceIndex(
            $entityIds, 
            'special_price', 
            $this->_getTable('catalog/product_compound_special_price'), 
            $this->getProductPriceIndexerHelper()->getCompoundSpecialPriceIndexTable()
        );
    }
    /**
     * Reindex all
     * 
     * @return self
     */
    protected function _reindexAll()
    {
        $this->_useIdxTable(true);
        $this->_emptyTable($this->_getIdxTable());
        $this->_prepareWebsiteDateTable();
        
        $this->_prepareCompoundPriceIndex();
        $this->_prepareCompoundSpecialPriceIndex();
        
        $this->_prepareTierPriceIndex();
        $this->_prepareGroupPriceIndex();

        $indexers = $this->_getTypeIndexers();
        foreach ($indexers as $indexer) {
            $indexer->reindexAll();
        }
        $this->_syncData();

        return $this;
    }
    /**
     * Refresh entities index
     * 
     * @param array $changedIds
     * 
     * @return array
     */
    protected function _reindex($changedIds = array())
    {
        $this->_emptyTable($this->_getIdxTable());
        $this->_prepareWebsiteDateTable();
        $select             = $this->_connection
            ->select()
            ->from(
                $this->_getTable('catalog/product'), 
                array('entity_id', 'type_id')
            )
            ->where('entity_id IN(?)', $changedIds);
        $pairs              = $this->_connection->fetchPairs($select);
        $byType             = array();
        foreach ($pairs as $productId => $productType) {
            $byType[$productType][$productId] = $productId;
        }

        $compositeIds       = array();
        $notCompositeIds    = array();

        foreach ($byType as $productType => $entityIds) {
            $indexer            = $this->_getIndexer($productType);
            if ($indexer->getIsComposite()) {
                $compositeIds += $entityIds;
            } else {
                $notCompositeIds += $entityIds;
            }
        }

        if (!empty($notCompositeIds)) {
            $select = $this->_connection
                ->select()
                ->from(
                    array('l' => $this->_getTable('catalog/product_relation')), 
                    'parent_id'
                )
                ->join(
                    array('e' => $this->_getTable('catalog/product')), 
                    'e.entity_id = l.parent_id', 
                    array('type_id')
                )
                ->where('l.child_id IN(?)', $notCompositeIds);
            $pairs              = $this->_connection->fetchPairs($select);
            foreach ($pairs as $productId => $productType) {
                if (!in_array($productId, $changedIds)) {
                    $changedIds[]                       = $productId;
                    $byType[$productType][$productId]   = $productId;
                    $compositeIds[$productId]           = $productId;
                }
            }
        }

        if (!empty($compositeIds)) {
            $this->_copyRelationIndexData($compositeIds, $notCompositeIds);
        }
        
        $this->_prepareCompoundPriceIndex();
        $this->_prepareCompoundSpecialPriceIndex();
        
        $this->_prepareTierPriceIndex($compositeIds + $notCompositeIds);
        $this->_prepareGroupPriceIndex($compositeIds + $notCompositeIds);

        $indexers = $this->_getTypeIndexers();
        foreach ($indexers as $indexer) {
            if (!empty($byType[$indexer->getTypeId()])) {
                $indexer->reindexEntity($byType[$indexer->getTypeId()]);
            }
        }

        $this->_syncData($changedIds);
        return $compositeIds + $notCompositeIds;
    }
}