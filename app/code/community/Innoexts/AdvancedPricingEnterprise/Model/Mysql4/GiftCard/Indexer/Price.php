<?php
/**
 * Innoexts
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the InnoExts Commercial License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://innoexts.com/commercial-license-agreement
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@innoexts.com so we can send you a copy immediately.
 * 
 * @category    Innoexts
 * @package     Innoexts_AdvancedPricingEnterprise
 * @copyright   Copyright (c) 2011 Innoexts (http://www.innoexts.com)
 * @license     http://innoexts.com/commercial-license-agreement  InnoExts Commercial License
 */

/**
 * Gift card products price indexer resource
 * 
 * @category   Innoexts
 * @package    Innoexts_AdvancedPricingEnterprise
 * @author     Innoexts Team <developers@innoexts.com>
 */
class Innoexts_AdvancedPricingEnterprise_Model_Mysql4_GiftCard_Indexer_Price 
    extends Enterprise_GiftCard_Model_Mysql4_Indexer_Price 
{
    /**
     * Get advanced pricing helper
     * 
     * @return Innoexts_AdvancedPricing_Helper_Data
     */
    protected function getAdvancedPricingHelper()
    {
        return Mage::helper('advancedpricing');
    }
    /**
     * Get version helper
     * 
     * @return Innoexts_Core_Helper_Version
     */
    protected function getVersionHelper()
    {
        return $this->getAdvancedPricingHelper()
            ->getVersionHelper();
    }
    /**
     * Get price indexer helper
     * 
     * @return Innoexts_AdvancedPricing_Helper_Catalog_Product_Price_Indexer
     */
    protected function getProductPriceIndexerHelper()
    {
        return $this->getAdvancedPricingHelper()
            ->getProductPriceIndexerHelper();
    }
    /**
     * Prepare final price data
     * 
     * @param int|array $entityIds
     * 
     * @return self
     */
    protected function _prepareFinalPriceData($entityIds = null)
    {
        $indexerHelper      = $this->getProductPriceIndexerHelper();
        $adapter            = $this->_getWriteAdapter();
        $this->_prepareDefaultFinalPriceTable();
        
        $select             = $indexerHelper->getFinalPriceSelect($adapter);
        $select->where('e.type_id=?', $this->getTypeId());
        $status             = $adapter->quoteInto('=?', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        $this->_addAttributeToSelect($select, 'status', 'e.entity_id', 'cs.store_id', $status, true);
        
        $select->columns(array('tax_class_id' => new Zend_Db_Expr('0')));
        
        $indexerHelper->addPriceJoins($adapter, $select);
        
        $allowOpenAmount    = $this->_addAttributeToSelect($select, 'allow_open_amount', 'e.entity_id', 'cs.store_id');
        $openAmountMin      = $this->_addAttributeToSelect($select, 'open_amount_min', 'e.entity_id', 'cs.store_id');
        $amountsAttribute   = $this->_getAttribute('giftcard_amounts');
        
        $select->joinLeft(
            array('gca' => $this->getTable('enterprise_giftcard/amount')), 
            implode(' AND ', array(
                '(gca.entity_id = e.entity_id)', 
                '(gca.attribute_id = '.$amountsAttribute->getAttributeId().')', 
                '(gca.website_id = cw.website_id OR gca.website_id = 0)', 
            )), 
            array()
        );
        
        $amounts            = new Zend_Db_Expr("MIN(IF(gca.value_id IS NULL, NULL, gca.value))");
        $openAmount         = new Zend_Db_Expr("MIN(IF(
            {$allowOpenAmount} = 1, 
            IF({$openAmountMin} > 0, {$openAmountMin}, 0), 
            NULL
        ))");
        $price              = new Zend_Db_Expr(
            "ROUND(IF(
                {$openAmount} IS NULL, 
                IF({$amounts} IS NULL, 0, {$amounts}), 
                IF(
                    {$amounts} IS NULL, 
                    {$openAmount}, 
                    IF({$openAmount} > {$amounts}, {$amounts}, {$openAmount})
                )
            ), 4)"
        );
        $null               = new Zend_Db_Expr('NULL');
                    
        $select->columns(array(
            'price'            => $null, 
            'final_price'      => $price, 
            'min_price'        => $price, 
            'max_price'        => $null, 
            'tier_price'       => $null, 
            'base_tier'        => $null, 
        ));
        if ($this->getVersionHelper()->isGe1700()) {
            $select->columns(array(
                'group_price'      => new Zend_Db_Expr('gp.price'), 
                'base_group_price' => new Zend_Db_Expr('gp.price'), 
            ));
        }
        $select->columns(array(
            'currency'      => $indexerHelper->getCurrencyExpr('cw.website_id'), 
            'store_id'      => new Zend_Db_Expr('cs.store_id'), 
        ));
        
        $select->group(array(
            'e.entity_id', 
            'cg.customer_group_id', 
            'cw.website_id', 
            $indexerHelper->getCurrencyExpr('cw.website_id'), 
            'cs.store_id', 
        ));
        
        if (!is_null($entityIds)) {
            $select->where('e.entity_id IN(?)', $entityIds);
        }
        $eventData = array(
            'select'            => $select, 
            'entity_field'      => new Zend_Db_Expr('e.entity_id'), 
            'website_field'     => new Zend_Db_Expr('cw.website_id'), 
            'store_field'       => new Zend_Db_Expr('cs.store_id'), 
            'currency_field'    => $indexerHelper->getCurrencyExpr('cw.website_id'), 
        );
        Mage::dispatchEvent('prepare_catalog_product_index_select', $eventData);
        $query = $select->insertFromSelect($this->_getDefaultFinalPriceTable());
        $adapter->query($query);
        return $this;
    }
    /**
     * Apply custom option
     * 
     * @return self
     */
    protected function _applyCustomOption()
    {
        $this->getProductPriceIndexerHelper()
            ->applyCustomOption(
                $this->_getWriteAdapter(), 
                $this->_getDefaultFinalPriceTable(), 
                $this->_getCustomOptionAggregateTable(), 
                $this->_getCustomOptionPriceTable(), 
                $this->useIdxTable()
            );
        return $this;
    }
    /**
     * Mode price data to index table
     *
     * @return self
     */
    protected function _movePriceDataToIndexTable()
    {
        $this->getProductPriceIndexerHelper()
            ->movePriceDataToIndexTable(
                $this->_getWriteAdapter(), 
                $this->_getDefaultFinalPriceTable(), 
                $this->getIdxTable(), 
                $this->useIdxTable()
            );
        return $this;
    }
}