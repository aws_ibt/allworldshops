<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Sales
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom Sales
 *
 * @category Mage
 * @package  Mage_Sales
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Sales_Block_Sales_Order_Totals extends Mage_Sales_Block_Order_Totals
{


    /**
     * Add new total to totals array after specific total or before last total by default
     * Move tax to after shipping
     *
     * @param Varien_Object          $total total
     * @param null|string|last|first $after after
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    public function addTotal(Varien_Object $total, $after=null)
    {
        if ($total->getCode() == 'tax') {
            $after = 'shipping';
        }
        return parent::addTotal($total, $after);
    }
}
