<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Api
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom
 *
 * @category  Bluecom
 * @package   Bluecom_Alipay
 * @copyright Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 */
class Bluecom_Alipay_Model_Source_Language
{
    /**
     * Source Data for configuration
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'EN', 'label' => Mage::helper('alipay')->__('English')),
            array('value' => 'FR', 'label' => Mage::helper('alipay')->__('French')),
            array('value' => 'DE', 'label' => Mage::helper('alipay')->__('German')),
            array('value' => 'IT', 'label' => Mage::helper('alipay')->__('Italian')),
            array('value' => 'ES', 'label' => Mage::helper('alipay')->__('Spain')),
            array('value' => 'NL', 'label' => Mage::helper('alipay')->__('Dutch')),
        );
    }
}



