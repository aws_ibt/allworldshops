<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Api
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom
 *
 * @category  Bluecom
 * @package   Bluecom_Alipay
 * @copyright Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 */
class Bluecom_Alipay_Model_Source_Bank
{
    /**
     * Source Data for configuration
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'ALIPAY', 'label' => Mage::helper('alipay')->__('Alipay'), 'bp' => '0px 0px'),

            array('value' => 'BOCB2C', 'label' => Mage::helper('alipay')->__('Bank of China'), 'bp' => '0px -30px'),
            array('value' => 'ICBCB2C',
                  'label' => Mage::helper('alipay')->__('Industrial and Commercial Bank of China'),
                  'bp'    => '0px -60px'),
            array('value' => 'CMB', 'label' => Mage::helper('alipay')->__('China Merchants Bank'), 'bp' => '0px -90px'),
            array('value' => 'CCB', 'label' => Mage::helper('alipay')->__('China Construction Bank'),
                  'bp'    => '0px -120px'),

            array('value' => 'ABC', 'label' => Mage::helper('alipay')->__('Agricultural Bank of China'),
                  'bp'    => '0px -150px'),
            array('value' => 'SPDB', 'label' => Mage::helper('alipay')->__('SPD Bank'), 'bp' => '0px -570px'),
            array('value' => 'CIB', 'label' => Mage::helper('alipay')->__('Xingye Bank'), 'bp' => '0px -330px'),
            array('value' => 'GDB', 'label' => Mage::helper('alipay')->__('Guangdong Development Bank'),
                  'bp'    => '0px -390px'),
            array('value' => 'SDB', 'label' => Mage::helper('alipay')->__('Shenzhen Development Bank'),
                  'bp'    => '0px -540px'),
            array('value' => 'CMBC', 'label' => Mage::helper('alipay')->__('China Mingshen Banking'),
                  'bp'    => '0px -360px'),

            array('value' => 'COMM', 'label' => Mage::helper('alipay')->__('Bank of Communications'),
                  'bp'    => '0px -180px'),
            array('value' => 'CITIC', 'label' => Mage::helper('alipay')->__('Zhongxin Bank'), 'bp' => '0px -690px'),
            array('value' => 'CEBBANK', 'label' => Mage::helper('alipay')->__('Guangda Bank'), 'bp' => '0px -300px'),
            array('value' => 'NBBANK', 'label' => Mage::helper('alipay')->__('Bank of Ning Bo'), 'bp' => '0px -480px'),
            array('value' => 'HZCBB2C', 'label' => Mage::helper('alipay')->__('Hangzhou Bank'), 'bp' => '0px -450px'),
            array('value' => 'SHBANK', 'label' => Mage::helper('alipay')->__('Shanghai Bank'), 'bp' => '0px -720px'),
            array('value' => 'SPABANK', 'label' => Mage::helper('alipay')->__('Ping An Bank'), 'bp' => '0px -510px'),

            array('value' => 'BJRCB', 'label' => Mage::helper('alipay')->__('Beijing Rural Commercial Bank'),
                  'bp'    => '0px -660px'),
            array('value' => 'FDB', 'label' => Mage::helper('alipay')->__('Fudian Bank'), 'bp' => '0px -600px'),
            array('value' => 'PSBC-DEBIT', 'label' => Mage::helper('alipay')->__('Postal Savings Bank of China'),
                  'bp'    => '0px -630px'),
            array('value' => 'BJBANK', 'label' => Mage::helper('alipay')->__('Beijing Bank'), 'bp' => '0px -240px'),

        );
    }

    /**
     * Get the Blank name from code value
     *
     * @param string $code code
     *
     * @return string|boolean
     */
    public function getLabel($code)
    {

        foreach ($this->toOptionArray() as $bank) {
            if ($code == $bank['value']) {
                return $bank['label'];
            }
        }
        return false;
    }
}