<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Api
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom
 *
 * @category  Bluecom
 * @package   Bluecom_Alipay
 * @copyright Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 */
class Bluecom_Alipay_Model_Observer
{
    /**
     * Cancel order that do not paid ( neither online payment or check on money ) in a perid time.
     *
     * @return void
     */
    public function orderCancelationActive()
    {
        $cancel_pending_orders = Mage::getStoreConfig('payment/alipay/cancel_pending_orders');
        if ($cancel_pending_orders) {
            $cancel_pending_orders = intval($cancel_pending_orders);
            /* @var $_orderList Mage_Sales_Model_Resource_Order_Collection */
            $_orderList = Mage::getModel('sales/order')->getCollection();
            $_orderList->addFieldToFilter('status', array('eq' => 'pending'));
            $_orderList->getSelect()->where(
                "DATE_ADD(updated_at, INTERVAL $cancel_pending_orders hour) < '" . Varien_Date::now() . "' "
            );
            if ($_orderList->getSize() > 0) {
                foreach ($_orderList as $_order) {
                    echo $_order->getId() . '<br />';
                    $order = Mage::getModel("sales/order")->load($_order->getId());
                    if ($order->canCancel()) {
                        $order->cancel()->save();
                    }
                }
                Mage::log(
                    sprintf("%d orders was canceled.", $_orderList->getSize()), null, "order-cancelation-active.log"
                );
            } else {
                Mage::log("0 orders was canceled.", null, "order-cancelation-active.log");
            }
        }

    }

}
