<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Api
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom
 *
 * @category  Bluecom
 * @package   Bluecom_Alipay
 * @copyright Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 */
class Bluecom_Alipay_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'alipay';
    protected $_formBlockType = 'alipay/form';
    protected $_infoBlockType = 'alipay/info';

    const RETURN_CODE_ACCEPTED = 'Success';
    const RETURN_CODE_TEST_ACCEPTED = 'Success';
    const RETURN_CODE_ERROR = 'Fail';

    protected $_isGateway = false;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canCapturePartial = false;
    protected $_canRefund = false;
    protected $_canVoid = false;
    protected $_canUseInternal = false;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;

    protected $_order = null;

    /**
     *  Returns Target URL
     *
     * @return string Target URL
     */
    public function getAlipayUrl()
    {
        $url = $this->getConfigData('transport') . '://' . $this->getConfigData('gateway');
        return $url;
    }

    /**
     *  Return back URL
     *
     * @return string URL
     */
    protected function getReturnURL()
    {
        return Mage::getUrl('checkout/onepage/success', array('_secure' => true));
    }

    /**
     *  Return URL for Alipay success response
     *
     * @return string URL
     */
    protected function getSuccessURL()
    {
        return Mage::getUrl('checkout/onepage/success', array('_secure' => true));
    }

    /**
     *  Return URL for Alipay failure response
     *
     * @return string URL
     */
    protected function getErrorURL()
    {
        return Mage::getUrl('alipay/payment/error', array('_secure' => true));
    }

    /**
     *  Return URL for Alipay notify response
     *
     * @return string URL
     */
    protected function getNotifyURL()
    {
        return Mage::getUrl('alipay/payment/notify/', array('_secure' => true));
    }

    /**
     * Capture payment
     *
     * @param Varien_Object $payment payment
     * @param int           $amount  amount
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function capture(Varien_Object $payment, $amount)
    {
        $payment->setStatus(self::STATUS_APPROVED)->setLastTransId($this->getTransactionId());
        return $this;
    }

    /**
     *  Form block description
     *
     * @param string $name name
     *
     * @return object
     */
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('alipay/form_payment', $name);
        $block->setMethod($this->_code);
        $block->setPayment($this->getPayment());
        return $block;
    }

    /**
     *  Return Order Place Redirect URL
     *
     * @return string Order Redirect URL
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('alipay/payment/redirect');
    }

    /**
     *  Return Standard Checkout Form Fields for request to Alipay
     *
     * @return array Array of hidden form fields
     */
    public function getStandardCheckoutFormFields()
    {
        $session = Mage::getSingleton('checkout/session');

        $order = $this->getOrder();
        if (!($order instanceof Mage_Sales_Model_Order)) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }

        $amount = sprintf('%.2f', $order->getBaseGrandTotal());
        if ($order->getBaseCurrencyCode() != 'CNY') {
            if ($convert = Mage::getSingleton('directory/currency')->load($order->getOrderCurrencyCode())) {
                if (!$convert->getRate('CNY')) {
                    die('Currency rate conversion failed, please contact administrator to setup currency rate first.');
                }
                $amount = $convert->convert($amount, "CNY");
                $amount = sprintf('%.2f', $amount);
            }
        }
        if (in_array(
            $this->getConfigData('service_type'), array('create_direct_pay_by_user', 'create_direct_pay_bank_by_user')
        )) {
            $parameter = array(
                'service'           => 'create_direct_pay_by_user',
                'partner'           => $this->getConfigData('partner_id'),
                'return_url'        => $this->getReturnURL(),
                'notify_url'        => $this->getNotifyURL(),
                '_input_charset'    => 'utf-8',
                'subject'           => $order->getRealOrderId(),
                'body'              => $order->getRealOrderId(),
                'out_trade_no'      => $this->orderNoToOutTradeNo($order->getRealOrderId()), // order ID
                'logistics_fee'     => '0.00', //because magento has shipping system, it has included shipping price
                'logistics_payment' => 'BUYER_PAY',  //always
                'logistics_type'    => 'EXPRESS', //Only three shipping method:POST,EMS,EXPRESS
                'price'             => $amount,
                'defaultbank'       => 'ICBCBTB',
                'payment_type'      => '1',
                'quantity'          => '1',
                // For the moment, the parameter of price is total price, so the quantity is 1.
                'show_url'          => Mage::getUrl(),
                'seller_email'      => $this->getConfigData('seller_email')
            );
        } else {
            if ($this->getConfigData('service_type') == 'create_partner_trade_by_buyer') {
                $parameter = array(
                    'service'           => $this->getConfigData('service_type'),
                    'partner'           => $this->getConfigData('partner_id'),
                    'return_url'        => $this->getReturnURL(),
                    'notify_url'        => $this->getNotifyURL(),
                    '_input_charset'    => 'utf-8',
                    'subject'           => $order->getRealOrderId(),
                    'body'              => $order->getRealOrderId(),
                    'out_trade_no'      => $this->orderNoToOutTradeNo($order->getRealOrderId()), // order ID
                    'logistics_fee'     => '0.00', //because magento has shipping system, it has included shipping price
                    'logistics_payment' => 'BUYER_PAY',  //always
                    'logistics_type'    => 'EXPRESS', //Only three shipping method:POST,EMS,EXPRESS
                    'price'             => $amount,
                    "currency"          => $order->getBaseCurrencyCode(),
                    'payment_type'      => '1',
                    'quantity'          => '1',
                    // For the moment, the parameter of price is total price, so the quantity is 1.
                    'show_url'          => Mage::getUrl(),
                    'seller_email'      => $this->getConfigData('seller_email'),
                    'receive_address'   => $order->getBillingAddress()->format('oneline'),
                    'receive_mobile'    => $order->getBillingAddress()->getTelephone(),
                    'receive_name'      => $order->getBillingAddress()->getName(),
                    'receive_zip'       => $order->getBillingAddress()->getPostcode(),
                );
            } else {
                $parameter = array(
                    'service'           => $this->getConfigData('service_type'),
                    'partner'           => $this->getConfigData('partner_id'),
                    'return_url'        => $this->getReturnURL(),
                    'notify_url'        => $this->getNotifyURL(),
                    '_input_charset'    => 'utf-8',
                    'subject'           => $order->getRealOrderId(),
                    'body'              => $order->getRealOrderId(),
                    'out_trade_no'      => $this->orderNoToOutTradeNo($order->getRealOrderId()), // order ID
                    'logistics_fee'     => '0.00', //because magento has shipping system, it has included shipping price
                    'logistics_payment' => 'BUYER_PAY',  //always
                    'logistics_type'    => 'EXPRESS', //Only three shipping method:POST,EMS,EXPRESS
                    'price'             => $amount,
                    "currency"          => $order->getBaseCurrencyCode(),
                    'payment_type'      => '1',
                    'quantity'          => '1',
                    // For the moment, the parameter of price is total price, so the quantity is 1.
                    'show_url'          => Mage::getUrl(),
                    'seller_email'      => $this->getConfigData('seller_email')
                );
            }
        }
        $defaultbank = $order->getPayment()->getData('alipay_defaultbank');
        if ($defaultbank) {
            $parameter['paymethod'] = 'bankPay';
            $parameter['defaultbank'] = $defaultbank;
        }
        if ($defaultbank == 'ALIPAY') {
            $parameter['paymethod'] = 'bankPay';
            $parameter['defaultbank'] = '';
        }
        $parameter = Mage::helper('alipay')->para_filter($parameter);
        $security_code = $this->getConfigData('security_code');
        $sign_type = 'MD5';

        $sort_array = array();
        $arg = "";
        $sort_array = Mage::helper('alipay')->arg_sort($parameter); //$parameter

        while (list ($key, $val) = each($sort_array)) {
            $arg .= $key . "=" . $val . "&";
        }

        $prestr = substr($arg, 0, count($arg) - 2);

        $mysign = Mage::helper('alipay')->sign($prestr . $security_code);

        $fields = array();
        $sort_array = array();
        $arg = "";
        $sort_array = Mage::helper('alipay')->arg_sort($parameter); //$parameter
        while (list ($key, $val) = each($sort_array)) {
            $fields[$key] = $val;
        }
        $fields['sign'] = $mysign;
        $fields['sign_type'] = $sign_type;
        return $fields;
    }

    /**
     *  Save invoice for order
     *
     * @param Mage_Sales_Model_Order $order order
     *
     * @return boolean Can save invoice or not
     */
    public function saveInvoice(Mage_Sales_Model_Order $order)
    {
        if ($order->canInvoice()) {
            $convertor = Mage::getModel('sales/convert_order');
            $invoice = $convertor->toInvoice($order);
            foreach ($order->getAllItems() as $orderItem) {
                if (!$orderItem->getQtyToInvoice()) {
                    continue;
                }
                $item = $convertor->itemToInvoiceItem($orderItem);
                $item->setQty($orderItem->getQtyToInvoice());
                $invoice->addItem($item);
            }
            $invoice->collectTotals();
            $invoice->register()->capture();
            Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder())
                ->save();
            return true;
        }
        return false;
    }

    /**
     *  Set corresponding payment gateway data
     *
     * @param array $data data
     *
     * @return Mage_Payment_Model_Info
     */
    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setAlipayDefaultbank($data->getAlipayDefaultbank());
        return $this;
    }

    /**
     * OrderNoToOutTradeNo
     *
     * @param int $orderNo orderNo
     *
     * @return string
     */
    public function orderNoToOutTradeNo($orderNo)
    {
        $alipay_order_prefix = $this->getConfigData('alipay_order_prefix');
        return $alipay_order_prefix . $orderNo;
    }

    /**
     * OutTradeNoToOrderNo
     *
     * @param int $outTradeNo outTradeNo
     *
     * @return string
     */
    public function outTradeNoToOrderNo($outTradeNo)
    {
        $alipay_order_prefix = $this->getConfigData('alipay_order_prefix');
        return substr($outTradeNo, strlen($alipay_order_prefix));
    }
}

