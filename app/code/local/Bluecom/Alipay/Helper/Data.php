<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Admin
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom
 *
 * @category  Bluecom
 * @package   Bluecom_Alipay
 * @copyright Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 */
class Bluecom_Alipay_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Alipay signature (md5)
     *
     * @param string $prestr prestr
     * 
     * @return string
     */
    public function sign($prestr)
    {
        $mysign = md5($prestr);
        return $mysign;
    }

    /**
     * Remove the parameter that doesn't need to be signed
     *
     * @param unknown_type $parameter parameter
     *
     * @return array
     */
    public function para_filter($parameter)
    {
        $para = array();
        while (list ($key, $val) = each($parameter)) {
            if ($key == "sign" || $key == "sign_type" || $val == "") {
                continue;
            } else {
                $para[$key] = $parameter[$key];
            }

        }
        return $para;
    }

    /**
     * Sort the array by key, plus reset the array pointer
     *
     * @param array $array array
     *
     * @return array
     */
    public function arg_sort($array)
    {
        ksort($array);
        reset($array);
        return $array;
    }
}
