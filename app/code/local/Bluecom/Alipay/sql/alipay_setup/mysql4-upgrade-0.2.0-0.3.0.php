<?php
/**
 * Bluecom
 *
 * @category  Bluecom
 * @package   Bluecom_Alipay
 * @copyright Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 */

/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
    $setup = new Mage_Sales_Model_Mysql4_Setup('sales_setup');
    $setup->addAttribute('quote_payment', 'alipay_defaultbank', array());
    $setup->addAttribute('order_payment', 'alipay_defaultbank', array());
    /**
     *      $setup->addAttribute('quote', 'alipay_pay_method', array('type' => 'varchar'));
            $setup->addAttribute('quote', 'alipay_pay_bank', array('type' => 'varchar'));
        
            $setup->addAttribute('order', 'alipay_pay_method', array('type' => 'varchar'));
            $setup->addAttribute('order', 'alipay_pay_bank', array('type' => 'varchar'));
     */
$this->endSetup();