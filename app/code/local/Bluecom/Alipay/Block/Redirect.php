<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Api
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom
 *
 * @category  Bluecom
 * @package   Bluecom_Alipay
 * @copyright Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 */
class Bluecom_Alipay_Block_Redirect extends Mage_Core_Block_Abstract
{
    /**
     * Render the html output for redirecting user to Alipay payment page
     *
     * @return string
     */
    protected function _toHtml()
    {
        Mage::app()->getResponse()->setHeader('Content-type', 'text/html; charset=utf-8');
        $standard = Mage::getModel('alipay/paymentMethod');
        $form = new Varien_Data_Form();
        $form->setAction($standard->getAlipayUrl())
            ->setId('alipay_payment_checkout')
            ->setName('alipay_payment_checkout')
            ->setMethod('GET')
            ->setUseContainer(true);
        foreach ($standard->setOrder($this->getOrder())->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $formHTML = $form->toHtml();

        $html = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>';
        $html .= $this->__('You will be redirected to Alipay in a few seconds.');
        $html .= $formHTML;
        $html .= '<script type="text/javascript">document.getElementById("alipay_payment_checkout").submit();</script>';
        $html .= '</body></html>';

        return $html;
    }
}
