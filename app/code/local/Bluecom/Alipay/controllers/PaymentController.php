<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Core
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom
 *
 * @category  Bluecom
 * @package   Bluecom_Alipay
 * @copyright Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 */
class Bluecom_Alipay_PaymentController extends Mage_Core_Controller_Front_Action
{
    /**
     * Order instance
     */
    protected $_order;

    /**
     * GetOrder
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if ($this->_order == null) {
            $session = Mage::getSingleton('checkout/session');
            $this->_order = Mage::getModel('sales/order');
            $this->_order->loadByIncrementId($session->getLastRealOrderId());
        }
        return $this->_order;
    }

    /**
     * When a customer chooses Alipay on Checkout/Payment page
     *
     * @return bool
     */
    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setAlipayPaymentQuoteId($session->getQuoteId());

        $order = $this->getOrder();

        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }

        $order->addStatusToHistory(
            $order->getStatus(),
            Mage::helper('alipay')->__('Customer was redirected to Alipay')
        );
        $order->save();

        $this->getResponse()
            ->setBody(
                $this->getLayout()
                    ->createBlock('alipay/redirect')
                    ->setOrder($order)
                    ->toHtml()
            );

        $session->unsQuoteId();
    }

    /**
     * Page that receives alipay's notification
     *
     * @return void
     */
    public function notifyAction()
    {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $method = 'post';
        } else {
            if ($this->getRequest()->isGet()) {
                $postData = $this->getRequest()->getQuery();
                $method = 'get';
            } else {
                return;
            }
        }
        //file_put_contents(dirname(__FILE__).'/log/r'.$_REQUEST['out_trade_no'].'.log', print_r($postData,true),FILE_APPEND);
        /* @var $alipay Bluecom_Alipay_Model_PaymentMethod */
        $alipay = Mage::getModel('alipay/paymentMethod');

        $partner = $alipay->getConfigData('partner_id');
        $security_code = $alipay->getConfigData('security_code');
        $mysign = '';
        $transport = $alipay->getConfigData('transport');

        if ($transport == "https") {
            $gateway = "https://www.alipay.com/cooperate/gateway.do?";
        } else {
            $gateway = "http://notify.alipay.com/trade/notify_query.do?";
        }

        $veryfy_result = '';
        $post = Mage::helper('alipay')->para_filter($postData);
        $sort_post = Mage::helper('alipay')->arg_sort($post);

        $arg = "";
        while (list ($key, $val) = each($sort_post)) {
            $arg .= $key . "=" . $val . "&";
        }
        $prestr = "";
        $prestr = substr($arg, 0, count($arg) - 2);  //去掉最后一个&号
        $mysign = Mage::helper('alipay')->sign($prestr . $security_code);
        $incrementId = $alipay->outTradeNoToOrderNo($postData['out_trade_no']);
        if (in_array(
            $alipay->getConfigData('service_type'),
            array('create_direct_pay_by_user', 'create_direct_pay_bank_by_user')
        )
            && $postData['trade_status'] == 'TRADE_FINISHED'
        ) {//即时到账,买家付款成功
            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($incrementId);
            $order->setAlipayTradeno($postData['trade_no']);
            if ($alipay->saveInvoice($order)) {
                $order->addStatusToHistory(
                    $alipay->getConfigData('create_direct_pay_by_user_order_status_trade_finished'),
                    Mage::helper('alipay')->__('买家付款成功。')
                );
            } else {
                $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, 'Save invoice fail.');
            }
            try {
                $order->save();
                echo "success";
                $order->sendNewOrderEmail();
                //$this->_redirect('alipay/payment/success');
            } catch (Exception $e) {
                echo "fail";
            }
        } else {
            if (in_array(
                $alipay->getConfigData('service_type'),
                array('create_direct_pay_by_user', 'create_direct_pay_bank_by_user')
            )
                && $postData['trade_status'] == 'TRADE_SUCCESS'
            ) {
                $order = Mage::getModel('sales/order');
                $order->loadByIncrementId($incrementId);
                $order->setAlipayTradeno($postData['trade_no']);
                if ($alipay->saveInvoice($order)) {
                    $order->addStatusToHistory(
                        $alipay->getConfigData('create_direct_pay_by_user_order_status_trade_finished'),
                        Mage::helper('alipay')->__('买家付款成功。')
                    );
                } else {
                    $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE, 'Save invoice fail.');
                }
                try {
                    $order->save();
                    echo "success";
                    $order->sendNewOrderEmail();
                    //$this->_redirect('alipay/payment/success');
                } catch (Exception $e) {
                    echo "fail";
                }
            } else {
                if ($alipay->getConfigData('service_type') == 'create_partner_trade_by_buyer'
                    && $postData['trade_status'] == 'WAIT_BUYER_PAY'
                ) {//纯担保交易,等待买家付款
                    $order = Mage::getModel('sales/order');
                    $order->loadByIncrementId($incrementId);
                    $order->setAlipayTradeno($postData['trade_no']);
                    $order->setEmailSent(false);
                    $order->addStatusToHistory(
                        $order->getStatus(),
                        Mage::helper('alipay')->__('等待买家付款。')
                    );
                    try {
                        $order->save();
                        echo "success";
                    } catch (Exception $e) {
                        echo "fail";
                    }
                } else {
                    if ($alipay->getConfigData('service_type') == 'create_partner_trade_by_buyer'
                        && $postData['trade_status'] == 'WAIT_SELLER_SEND_GOODS'
                    ) {//纯担保交易,买家付款成功,等待卖家发货
                        $order = Mage::getModel('sales/order');
                        $order->loadByIncrementId($incrementId);
                        $order->setAlipayTradeno($postData['trade_no']);
                        $order->setEmailSent(false);
                        $order->addStatusToHistory(
                            $alipay->getConfigData('create_partner_trade_by_buyer_order_status_wait_seller_send_goods'),
                            Mage::helper('alipay')->__('买家付款成功,等待卖家发货。')
                        );
                        try {
                            $order->save();
                            echo "success";
                        } catch (Exception $e) {
                            echo "fail";
                        }
                    } else {
                        if ($alipay->getConfigData('service_type') == 'create_partner_trade_by_buyer'
                            && $postData['trade_status'] == 'WAIT_BUYER_CONFIRM_GOODS'
                        ) {//纯担保交易,卖家已经发货等待买家确认
                            $order = Mage::getModel('sales/order');
                            $order->loadByIncrementId($incrementId);
                            $order->setAlipayTradeno($postData['trade_no']);
                            $order->setEmailSent(false);
                            $order->addStatusToHistory(
                                $alipay->getConfigData(
                                    'create_partner_trade_by_buyer_order_status_wait_buyer_confirm_goods'
                                ),
                                Mage::helper('alipay')->__('卖家已经发货等待买家确认。')
                            );
                            try {
                                $order->save();
                                echo "success";
                            } catch (Exception $e) {
                                echo "fail";
                            }
                        } else {
                            if ($alipay->getConfigData('service_type') == 'create_partner_trade_by_buyer'
                                && $postData['trade_status'] == 'TRADE_FINISHED'
                            ) {//纯担保交易,买家已经确认收货
                                $order = Mage::getModel('sales/order');
                                $order->loadByIncrementId($incrementId);
                                $order->setAlipayTradeno($postData['trade_no']);
                                if ($alipay->saveInvoice($order)) {
                                    $order->addStatusToHistory(
                                        Mage_Sales_Model_Order::STATE_COMPLETE,
                                        Mage::helper('alipay')->__('买家已付款,交易成功结束。')
                                    );
                                } else {
                                    $order->addStatusToHistory(
                                        Mage_Sales_Model_Order::STATE_COMPLETE, 'Save invoice fail.'
                                    );
                                }
                                try {
                                    $order->save();
                                    //$this->_redirect('alipay/payment/success');
                                    echo "success";
                                } catch (Exception $e) {
                                    echo "fail";
                                }
                            } else {
                                echo "fail";
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *  Success payment page
     *
     * @return void
     */
    public function successAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getAlipayPaymentQuoteId());
        $session->unsAlipayPaymentQuoteId();
        $order = $this->getOrder();
        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }
        $order->addStatusToHistory(
            $order->getStatus(),
            Mage::helper('alipay')->__('Customer successfully returned from Alipay')
        );
        //$order->sendNewOrderEmail(); email was sent in Alipay notify
        //$order->setEmailSent(true);

        $order->save();
        $this->_redirect('checkout/onepage/success');
    }

    /**
     *  Failure payment page
     * 
     * @return void
     */
    public function errorAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $errorMsg = Mage::helper('alipay')->__(' There was an error occurred during paying process.');

        $order = $this->getOrder();

        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }
        if ($order instanceof Mage_Sales_Model_Order && $order->getId()) {
            $order->addStatusToHistory(
                Mage_Sales_Model_Order::STATE_CANCELED,
                Mage::helper('alipay')->__('Customer returned from Alipay.') . $errorMsg
            );
            $order->save();
        }

        $this->loadLayout();
        $this->renderLayout();
        Mage::getSingleton('checkout/session')->unsLastRealOrderId();
    }

}
