<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Core
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom Page Helper Data
 *
 * @author Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Page_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * GetMenuCategory
     *
     * @return bool|Mage_Core_Model_Abstract|mixed
     */
    public function getMenuCategory()
    {
        if ($_menuCategory = Mage::registry('menu_category')) {
            return $_menuCategory;
        }
        $_specialCategory = Mage::getSingleton('core/session')->getSpecialCategory();
        $udLParam = Mage::app()->getRequest()->getParam('udL');
        $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($udLParam);
        if (isset($_specialCategory['id']) && $_specialCategory['id'] && $correctUdL) {
            $_category = Mage::getModel('catalog/category')
                ->load($_specialCategory['id']);
            if ($_category) {
                return $_category;
            }
        }

        return false;
    }

    /**
     * GetConnection
     *
     * @param string $type type
     *
     * @return mixed
     */
    protected function _getConnection($type = 'core_read')
    {
        return Mage::getSingleton('core/resource')->getConnection($type);
    }

    /**
     * GetProductCategorySource
     *
     * @return bool|mixed
     */
    public function getProductCategorySource()
    {
        $source = 0;

        if (Mage::getSingleton('core/session')->getShopByCountry()) {
            $source = Mage::getSingleton('core/session')->getShopByCountry();
        }

        if ($source > 0) {
            $product_category_source = Mage::registry('product_category_source');
            if (null == $product_category_source) {
                $connection = $this->_getConnection('core_read');

                $attribute_source_id = Mage::getModel('eav/config')->getAttribute('catalog_product', 'source')->getId();
                $attribute_status_id = Mage::getModel('eav/config')->getAttribute('catalog_product', 'status')->getId();
                $sql_lang = "SELECT ccp.category_id,COUNT(ccp.product_id) AS count_product_source FROM catalog_category_product as ccp
                            INNER JOIN catalog_product_entity_int AS cpei
                            ON ccp.product_id = cpei.entity_id AND cpei.attribute_id = ".(int)$attribute_source_id." and cpei.value = ".(int)$source."
                            INNER JOIN catalog_product_entity_int AS cpei2
                            ON ccp.product_id = cpei2.entity_id AND cpei2.attribute_id = ".(int)$attribute_status_id." and cpei2.value = 1
                            GROUP BY ccp.category_id";

                $datas_query      = $connection->fetchAll($sql_lang);
                foreach ($datas_query as $data) {
                    $product_category_source[$data['category_id']] = $data['count_product_source'];
                }

                Mage::register('product_category_source', $product_category_source);
            }
        } else {
            return false;
        }
        return $product_category_source;
    }

    /**
     * Check udL Param is special category
     *
     * @param $udl
     *
     * @return bool|Mage_Core_Model_Abstract
     */
    public function checkCorrectUdLParam($udl)
    {
        if (is_numeric($udl) && $udl) {
            /** @var Mage_Catalog_Model_Category $category */
            $_category = $_category = Mage::getModel('catalog/category')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($udl);

            if ($_category && $_category->getIsSpecialShop()) {
                return $_category;
            }
        }
        return false;
    }

    /**
     * Change Param udL not correct in url
     *
     * @param $urlBefore
     * @param $udL
     *
     * @return mixed|string
     */
    public function changeParamUdLCorrect($urlBefore, $udL)
    {
        if ($udL) {
            $url = parse_url($urlBefore);
            $query = isset($url['query']) ? $url['query'] : '';
            if ($query != '') {
                parse_str($query, $output);
                if (isset($output['udL'])) {
                    if ($output['udL'] != $udL) {
                        $urlBefore = str_replace('udL=' . $output['udL'], 'udL=' . $udL, $urlBefore);
                        return $urlBefore;
                    } else {
                        return $urlBefore;
                    }
                } else {
                    foreach ($output as $key => $value) {
                        $keyUdL = preg_replace('/[^A-Za-z0-9\-]/', '', $key);
                        if ($keyUdL === 'udL') {
                            $urlBefore = str_replace($key .'=', 'udL=', $urlBefore);
                            return $urlBefore;
                        }
                    }
                    return $urlBefore . '&udL=' . $udL;
                }
            } else {
                return $urlBefore . '?udL=' . $udL;
            }
        }
        return '';
    }
}
