<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Core
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom Page Model Observer
 *
 * @author Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Page_Model_Observer
{
    /**
     * Add body class
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return int
     */
    public function addBodyClass(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        if (get_class($block) == 'Mage_Page_Block_Html') {

            $udLParam = Mage::app()->getRequest()->getParam('udL');
            $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($udLParam);
            if ($correctUdL) {
                $_specialCategory = Mage::getSingleton('core/session')->getSpecialCategory();
            }

            $currentClass = explode(' ', $block->getBodyClass());

            if (isset($_specialCategory['classes'])) {
                if (!in_array($_specialCategory['classes'], $currentClass)) {
                    $block->addBodyClass($_specialCategory['classes']);
                }
            }

            if (!isset($_specialCategory)) {
                $_category = Mage::registry('current_category');

                if ($_category) {
                    if ($_category->getData('level') == 2) {
                        $_menuCategory = $_category;

                        $layout = $_menuCategory->getData('custom_layout_update');
                        if ($layout) {
                            $xml = "<?xml version='1.0'?><document>" . $layout . "</document>";
                            $xml = simplexml_load_string($xml);
                            $classes = $xml->xpath('//reference[@name="root"]/action[@method="addBodyClass"]/classname');

                            if (sizeof($classes)) {
                                $body_class = (string)$classes[0];
                            }
                            if (isset($body_class)) {
                                if (in_array($body_class, $currentClass)) {
                                    $bodyClass = str_replace($body_class, '', $block->getBodyClass());
                                    $block->setBodyClass($bodyClass);
                                }
                            }
                        }

                    }
                }
            }
            //add class to show banner follow country - AWS-372
            if (Mage::app()->getRequest()->getParam('source')) {
                $source = Mage::app()->getRequest()->getParam('source');
                $block->addBodyClass('source-' . $source);
            } else if (Mage::app()->getRequest()->getParam('rssour')) {
                $block->addBodyClass('rssour');
            }
        }
    }

    /**
     * Init header home page
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return int
     */
    public function initHeaderHomepage(Varien_Event_Observer $observer)
    {
        if ($this->_referenIsHome() || $this->_getIsHomePage()) {

            Mage::getSingleton('core/session')->unsSpecialCategory();
            Mage::getSingleton('core/session')->unsUpdateLayout();
            // delete cookie udL
            Mage::getModel('core/cookie')->delete('udL');
        } elseif (!Mage::app()->getRequest()->getParam('udL')) {
            Mage::getSingleton('core/session')->unsSpecialCategory();
            Mage::getSingleton('core/session')->unsUpdateLayout();
        }
    }

    /**
     * Init header special page
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return int
     */
    public function initHeaderSpecialPage(Varien_Event_Observer $observer)
    {
        $updateLayout = Mage::app()->getRequest()->getParam('udL');
        $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($updateLayout);

        // check udL param is special category
        if ($correctUdL) {
            $_category = Mage::getModel('catalog/category')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($updateLayout);

        } else {
            $_category = Mage::registry('current_category');
        }

        if ($correctUdL && $_category->getIsSpecialShop()) {

            if ($_category->getData('level') == 2) {
                $_menuCategory = $_category;
                $_imgUrl = $_category->getLogoSpecialShop();
                Mage::app()->getRequest()->setPost('source', null);
            } elseif ($_category->getData('level') == 3) {
                $_menuCategory = $_category->getParentCategory();
                $_imgUrl = $_menuCategory->getLogoSpecialShop();
            } else {
                $_menuCategory = $_category->getParentCategory()->getParentCategory();
                $_imgUrl = $_menuCategory->getLogoSpecialShop();
            }

            $template_url = 'page/html/header.phtml';
            $body_class = '';
            $name_store = '';

            $layout = $_menuCategory->getData('custom_layout_update');
            if ($layout) {
                $xml = "<?xml version='1.0'?><document>" . $layout . "</document>";
                $xml = simplexml_load_string($xml);
                $template = $xml->xpath('//reference[@name="header"]/action[@method="setTemplate"]/template');
                $classes = $xml->xpath('//reference[@name="root"]/action[@method="addBodyClass"]/classname');
                $name_store = $xml->xpath('//reference[@name="head"]/action[@method="setTitle"]/title');

                if (sizeof($template)) {
                    $template_url = (string)$template[0];
                }
                if (sizeof($classes)) {
                    $body_class = (string)$classes[0];
                }
                if (sizeof($name_store)) {
                    $name_store = (string)$name_store[0];
                }
            }

            if (Mage::registry('menu_category')) {
                Mage::unregister('menu_category');
            }
            Mage::register('menu_category', $_menuCategory);

            $_imgUrl = $_imgUrl ? Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/category/' . $_imgUrl : '';
            $_logo_link = $_menuCategory->getUrl($_menuCategory);

            Mage::getSingleton('core/session')->setSpecialCategory(
                array(
                    'name' => $_menuCategory->getName(),
                    'name_store' => $name_store,
                    'id' => $_menuCategory->getId(),
                    'logo_url' => $_imgUrl,
                    'logo_link' => $_logo_link,
                    'template' => $template_url,
                    'classes' => $body_class
                )
            );

        } elseif ($_category && $_category->getIsSpecialShop()) {
            if (!$correctUdL) {
                Mage::getSingleton('core/session')->unsetSpecialCategory();
                $_category->setData('display_mode', 'PRODUCTS');
                $_category->setData('landing_page', null);
                $_category->setData('is_anchor', 1);
                $_category->setData('page_layout', null);

            }
        } else {
            Mage::getSingleton('core/session')->unsetSpecialCategory();
        }
    }

    /**
     * Check reference url is home page
     *
     * @return bool
     */
    private function _referenIsHome()
    {

        return Mage::app()->getRequest()->getServer('HTTP_REFERER') == Mage::getUrl('');
    }

    /**
     * Check current url is home page
     *
     * @return bool
     */
    private function _getIsHomePage()
    {
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controller = Mage::app()->getRequest()->getControllerName();
        $action = Mage::app()->getRequest()->getActionName();
        if ($routeName == 'cms' && $controller == 'index' && $action == 'index') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check param udL when redirect with code 301
     *
     *
     * @param Varien_Event_Observer $observer
     */
    public function forceDefaultRedirectUrl(Varien_Event_Observer $observer)
    {
        $url = $observer->getTransport()->getUrl();
        $code = $observer->getTransport()->getCode();

        if ($code === 301) {
            /**
             * Check param udL is exits
             * If exists add to url
             */
            $udLParam = Mage::app()->getRequest()->getParam('udL');

            $helperPage = Mage::helper('bluecom_page');
            $correctUdL = $helperPage->checkCorrectUdLParam($udLParam);

            // check udL param is special category
            if ($correctUdL) {

                $urlCheck = $helperPage->changeParamUdLCorrect($url, $udLParam);

                if ($urlCheck != '') {
                    $url = $urlCheck;
                }
            }

            $observer->getTransport()->setUrl($url);
        }
    }

}