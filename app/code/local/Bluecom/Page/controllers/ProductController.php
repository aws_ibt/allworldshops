<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Review
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

require_once 'Mage/Review/controllers/ProductController.php';
/**
 * Bluecom Page ProductController
 *
 * @category Mage
 * @package  Mage_Review
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Page_ProductController extends Mage_Review_ProductController
{

    /**
     * Load specific layout handles by product type id
     *
     * @param object $product product
     *
     * @return mixed
     */
    protected function _initProductLayout($product)
    {
        $update = $this->getLayout()->getUpdate();

        $update->addHandle('default');
        $this->addActionLayoutHandles();


        $update->addHandle('PRODUCT_TYPE_'.$product->getTypeId());

        if ($product->getPageLayout()) {
            $this->getLayout()->helper('page/layout')
                ->applyHandle($product->getPageLayout());
        }

        $this->loadLayoutUpdates();
        if ($product->getPageLayout()) {
            $this->getLayout()->helper('page/layout')
                ->applyTemplate($product->getPageLayout());
        }

        // check product custom design
        $_category = Mage::registry('current_category');
        if ($_category && $_category->getData('custom_apply_to_products') == '1') {
            $product->setCustomLayoutUpdate($_category->getData('custom_layout_update'));
        }

        $update->addUpdate($product->getCustomLayoutUpdate());
        $this->generateLayoutXml()->generateLayoutBlocks();
    }
}