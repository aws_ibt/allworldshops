<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Page
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom Page Block Html Header
 *
 * @category Mage
 * @package  Mage_Page
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Page_Block_Html_Header extends Mage_Page_Block_Html_Header
{
    /**
     * Get website by country
     *
     * @return array
     */
    public function getWebsiteByCountry()
    {
        $countries = array();
        $websites = Mage::app()->getWebsites();
        foreach ($websites as $website) {
            if ($website->getCode() != 'gb') {
                $country = array('name' => str_replace("AWS", "", $website->getName()),'url' => $website->getDefaultStore()->getBaseUrl());
                $countries[] = $country;
            }
        }
        return $countries;
    }

    /**
     * Count wish list customer
     *
     * @return int
     */
    public function countWishListCustomer()
    {
        return $this->helper('wishlist')->getItemCount();
    }

    /**
     * Get logo and menu for special shops
     */
    public function _construct()
    {
        $_specialCategory = Mage::getSingleton('core/session')->getSpecialCategory();
        $udLParam = Mage::app()->getRequest()->getParam('udL');
        $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($udLParam);
        if ($_specialCategory && $correctUdL) {
            $this->setTemplate($_specialCategory['template']);
        } else {
            $this->setTemplate('page/html/header.phtml');
        }
    }

    /**
     * Prepare layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $_specialCategory = Mage::getSingleton('core/session')->getSpecialCategory();
        $udLParam = Mage::app()->getRequest()->getParam('udL');
        $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($udLParam);
        if ($_specialCategory && $correctUdL && isset($_specialCategory['name_store']) && $_specialCategory['name_store']) {
            if (false !== $this->getLayout()->getBlock('head')) {
                $this->getLayout()->getBlock('head')->setTitle(ucfirst($_specialCategory['name_store']));
            }
        }
        return parent::_prepareLayout();
    }

    /**
     * Get logo src
     *
     * @return string
     */
    public function getLogoSrc()
    {
        $_specialCategory = Mage::getSingleton('core/session')->getSpecialCategory();
        $udLParam = Mage::app()->getRequest()->getParam('udL');
        $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($udLParam);
        if ($_specialCategory && $correctUdL && isset($_specialCategory['logo_url']) && $_specialCategory['logo_url']) {
            return $_specialCategory['logo_url'];
        }
        // Check country code
        $currentSource = $this->getCurrentSource();
        if ($currentSource) {
            $urlImgLogo = '';
            switch ($currentSource['value']) {
                case 30:
                    // china
                    $urlImgLogo = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/allworldshops/global/images/logo/cn_logo.png';
                    break;
                case 31:
                    //Australia
                    $urlImgLogo = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/allworldshops/global/images/logo/au_logo.png';
                    break;
                case 32:
                    //New Zealand
                    $urlImgLogo = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/allworldshops/global/images/logo/nz_logo.png';
                    break;
				case 3182:
                    //Japan
                    $urlImgLogo = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/allworldshops/global/images/logo/jp_logo.png';
                    break;		
            }
            return $urlImgLogo;
        }
        return parent::getLogoSrc();
    }

    /**
     * Get logo link
     *
     * @return mixed
     */
    public function getLogoLink()
    {
        $_specialCategory = Mage::getSingleton('core/session')->getSpecialCategory();
        $udLParam = Mage::app()->getRequest()->getParam('udL');
        $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($udLParam);
        if ($_specialCategory && $correctUdL && isset($_specialCategory['logo_link']) && $_specialCategory['logo_link']) {
            return $_specialCategory['logo_link'];
        }
    }


    /**
     * Get logo src small
     *
     * @return string
     */
    public function getLogoSrcSmall()
    {
        $_specialCategory = Mage::getSingleton('core/session')->getSpecialCategory();
        $udLParam = Mage::app()->getRequest()->getParam('udL');
        $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($udLParam);
        if ($_specialCategory && $correctUdL && isset($_specialCategory['logo_url']) && $_specialCategory['logo_url']) {
            return $_specialCategory['logo_url'];
        }
        // Check country code
        $currentSource = $this->getCurrentSource();
        if ($currentSource) {
            $urlImgLogo = '';
            switch ($currentSource['value']) {
                case 30:
                    // china
                    $urlImgLogo = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/allworldshops/global/images/logo/cn_logo.png';
                    break;
                case 31:
                    //Australia
                    $urlImgLogo = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/allworldshops/global/images/logo/au_logo.png';
                    break;
                case 32:
                    //New Zealand
                    $urlImgLogo = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/allworldshops/global/images/logo/nz_logo.png';
                    break;
				case 3182:
                    //Japan
                    $urlImgLogo = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'frontend/allworldshops/global/images/logo/jp_logo.png';
                    break;	
            }
            return $urlImgLogo;
        }
        return parent::getLogoSrcSmall();
    }

    /**
     * Get source attribute to show the Shop by Country on header
     *
     * @return bool
     */
    public function getProductSources()
    {
        $sourceAttribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'source');
        if ($sourceAttribute->getId()) {
            $data = array();
            $sources = $sourceAttribute->getSource()->getAllOptions();
            foreach ($sources as $source) {
                if ($source['value']) {
                    $data[] = $source;
                }
            }
            return $data;
        }
        return false;
    }

    /**
     * Get current source
     *
     * @return bool|mixed
     * @throws Exception
     */
    public function getCurrentSource()
    {
        $sources = $this->getProductSources();
        if ($sources) {
            $sourceId = $this->getRequest()->getParam('source', 0);
            foreach ($sources as $source) {
                if ($source['value'] == $sourceId) {
                    return $source;
                }
            }
        }
        return false;
    }

    /**
     * @param string $data
     * @param $id
     * @param array $tags
     * @param null $lifetime
     * @return bool
     */
    protected function _saveCache($data, $id, $tags = array(), $lifetime = null) {
        return false;
    }
}
