<?php
/**
 * Upgrade 0.1.1
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$cn = Mage::app()->getStore('cn_en')->getId();
Mage::getModel('cms/block')->setStoreId($cn)->load('social_sharing')->setContent(
    '
<ul class="social-button">
<li><a id="btn-weibo" class="btn-weibo" href="#" target="_blank"><em>Weibo</em></a></li>
<li><a id="btn-qq" class="btn-qq" href="#" target="_blank"><em>qq</em></a></li>
</ul>
<script type="text/javascript">// <![CDATA[
document.getElementById("btn-weibo").href = "http://service.weibo.com/share/share.php?url=" + encodeURIComponent(window.location.href);
document.getElementById("btn-qq").href = "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=" + encodeURIComponent(window.location.href);
// ]]></script>
'
)->save();

$installer->endSetup();