<?php
/**
 * Install 0.1.0
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$storeGroupModel = Mage::getModel('core/store_group');
$catModel = Mage::getModel('catalog/category');

// set global categories
/*$storeGB = $storeGroupModel->getCollection()
    ->addFieldToFilter('name', 'AWS Global')
    ->getFirstItem();*/

// all websites use the same category structure
$stores = $storeGroupModel->getCollection();

foreach ($stores as $store) {
    $storeId = $store->getGroupId();
    $rootCatId = $store->getRootCategoryId();

    $parentCategory = Mage::getModel('catalog/category')->load($rootCatId);

    $category1 = Mage::getModel('catalog/category')->setName('Apparel & Footwear')
        ->setUrlKey('apparel-footwear')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $category2 = Mage::getModel('catalog/category')->setName('Clothing')
        ->setUrlKey('clothing')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category1->getPath())
        ->save();
    $data = array('T-Shirt', 'Evening Dresses', 'Kids Clothing');
    foreach ($data as $name) {
        $category3 = Mage::getModel('catalog/category')->setName($name)
            ->setUrlKey($name)
            ->setIsActive(1)
            ->setDisplayMode('PRODUCTS')
            ->setIsAnchor(1)
            ->setStoreId($storeId)
            ->setPath($category2->getPath())
            ->save();
    }

    $category1 = Mage::getModel('catalog/category')->setName('Honey')
        ->setUrlKey('honey')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $data = array('Arataki Honey', 'UMF Manuka Honey', 'Comvita');
    foreach ($data as $name) {
        $category2 = Mage::getModel('catalog/category')->setName($name)
            ->setUrlKey($name)
            ->setIsActive(1)
            ->setDisplayMode('PRODUCTS')
            ->setIsAnchor(1)
            ->setStoreId($storeId)
            ->setPath($category1->getPath())
            ->save();
    }

    $category1 = Mage::getModel('catalog/category')->setName('Toy')
        ->setUrlKey('toy')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $data = array('3 Sprouts', 'All Blacks');
    foreach ($data as $name) {
        $category2 = Mage::getModel('catalog/category')->setName($name)
            ->setUrlKey($name)
            ->setIsActive(1)
            ->setDisplayMode('PRODUCTS')
            ->setIsAnchor(1)
            ->setStoreId($storeId)
            ->setPath($category1->getPath())
            ->save();
    }

    $category1 = Mage::getModel('catalog/category')->setName('Baby')
        ->setUrlKey('baby')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $data = array('Aromatherapy', 'Bags');
    foreach ($data as $name) {
        $category2 = Mage::getModel('catalog/category')->setName($name)
            ->setUrlKey($name)
            ->setIsActive(1)
            ->setDisplayMode('PRODUCTS')
            ->setIsAnchor(1)
            ->setStoreId($storeId)
            ->setPath($category1->getPath())
            ->save();
    }

    $category1 = Mage::getModel('catalog/category')->setName('Beauty')
        ->setUrlKey('beauty')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $category2 = Mage::getModel('catalog/category')->setName('Paw Paw')
        ->setUrlKey('paw-paw')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category1->getPath())
        ->save();

    $category1 = Mage::getModel('catalog/category')->setName('Health')
        ->setUrlKey('health')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $category2 = Mage::getModel('catalog/category')->setName('Symptom')
        ->setUrlKey('symptom')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category1->getPath())
        ->save();

    $category1 = Mage::getModel('catalog/category')->setName('NZ')
        ->setUrlKey('nz')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $category2 = Mage::getModel('catalog/category')->setName('Apparel & Footwear')
        ->setUrlKey('apparel-footwear')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category1->getPath())
        ->save();
    $category3 = Mage::getModel('catalog/category')->setName('Clothing')
        ->setUrlKey('clothing')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category2->getPath())
        ->save();

    $category1 = Mage::getModel('catalog/category')->setName('AU')
        ->setUrlKey('au')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $category2 = Mage::getModel('catalog/category')->setName('Apparel & Footwear')
        ->setUrlKey('apparel-footwear')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category1->getPath())
        ->save();
    $category3 = Mage::getModel('catalog/category')->setName('Clothing')
        ->setUrlKey('clothing')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category2->getPath())
        ->save();

    $category1 = Mage::getModel('catalog/category')->setName('CN')
        ->setUrlKey('cn')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($parentCategory->getPath())
        ->save();
    $category2 = Mage::getModel('catalog/category')->setName('Apparel & Footwear')
        ->setUrlKey('apparel-footwear')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category1->getPath())
        ->save();
    $category3 = Mage::getModel('catalog/category')->setName('Clothing')
        ->setUrlKey('clothing')
        ->setIsActive(1)
        ->setDisplayMode('PRODUCTS')
        ->setIsAnchor(1)
        ->setStoreId($storeId)
        ->setPath($category2->getPath())
        ->save();
}

$installer->endSetup();