<?php

/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

/**
 * Category controller
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */

require_once 'Mage/Catalog/controllers/CategoryController.php';


class Bluecom_Catalog_CategoryController extends Mage_Catalog_CategoryController {
	/**
	 * Category view action
	 */
	public function viewAction()
	{
		if ($category = $this->_initCatagory()) {
			$design = Mage::getSingleton('catalog/design');
			$settings = $design->getDesignSettings($category);

			// apply custom design
			if ($settings->getCustomDesign()) {
				$design->applyCustomDesign($settings->getCustomDesign());
			}

			Mage::getSingleton('catalog/session')->setLastViewedCategoryId($category->getId());

			$update = $this->getLayout()->getUpdate();
			$update->addHandle('default');

			if (!$category->hasChildren()) {
				$update->addHandle('catalog_category_layered_nochildren');
			}

			$this->addActionLayoutHandles();
			$update->addHandle($category->getLayoutUpdateHandle());
			$update->addHandle('CATEGORY_' . $category->getId());
			$this->loadLayoutUpdates();

			$updateLayout = 0;
            $_specialCategory = null;
            $udLParam = Mage::app()->getRequest()->getParam('udL');
            $correctUdL = Mage::helper('bluecom_page')->checkCorrectUdLParam($udLParam);

            // check udL param is special category
            if ($correctUdL) {
				$updateLayout = $udLParam;
//                $_specialCategory = Mage::getSingleton('core/session')->getSpecialCategory();
			}

			if ($updateLayout != 0) {
                if ($category->getIsSpecialShop() && $category->getData('level') == 2) {
                    // apply custom layout update once layout is loaded
                    // update layout special category home page by param category
                    $settingsUDL = $design->getDesignSettings($correctUdL);
                    if ($layoutUpdates = $settingsUDL->getLayoutUpdates()) {
                        if (is_array($layoutUpdates)) {
                            foreach ($layoutUpdates as $layoutUpdate) {
                                $update->addUpdate($layoutUpdate);
                            }
                        }
                    }
                } else {
                    // update layout special category by this category
                    if ($layoutUpdates = $settings->getLayoutUpdates()) {
                        if (is_array($layoutUpdates)) {
                            foreach ($layoutUpdates as $layoutUpdate) {
                                $update->addUpdate($layoutUpdate);
                            }
                        }
                    }
                }
			}
			$this->generateLayoutXml()->generateLayoutBlocks();
			// apply custom layout (page) template once the blocks are generated
            // update normal category
			if ($updateLayout != 0)
			{
				if ($settings->getPageLayout()) {
					$this->getLayout()->helper('page/layout')->applyTemplate($settings->getPageLayout());
				}
			}


			if ($root = $this->getLayout()->getBlock('root')) {
				$root->addBodyClass('categorypath-' . $category->getUrlPath())
					->addBodyClass('category-' . $category->getUrlKey());
			}

			$this->_initLayoutMessages('catalog/session');
			$this->_initLayoutMessages('checkout/session');
			$this->renderLayout();
		}
		elseif (!$this->getResponse()->isRedirect()) {
			$this->_forward('noRoute');
		}
	}
}