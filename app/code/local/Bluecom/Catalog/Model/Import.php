<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Directory
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom Catalog Model Import
 *
 * @author Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Catalog_Model_Import
    extends Mage_Directory_Model_Currency_Import_Abstract
{

    protected $_url = 'http://api.fixer.io/latest?base=%1$s&symbols=%2$s';
    protected $_messages = array();
    protected $_timeout = 15;

    /**
     * HTTP client
     *
     * @var Varien_Http_Client
     */
    protected $_httpClient;

    /**
     * Bluecom_Catalog_Model_Import constructor.
     *
     * @return bool
     */
    public function __construct()
    {
        $this->_httpClient = new Varien_Http_Client();
    }

    /**
     * Convert
     *
     * @param string $currencyFrom currencyFrom
     * @param string $currencyTo   currencyTo
     * @param int    $retry        retry
     *
     * @return float|null|string
     */
    protected function _convert($currencyFrom, $currencyTo, $retry = 0)
    {
        $url = sprintf($this->_url, $currencyFrom, $currencyTo);

        try {
            $response = $this->_httpClient
                ->setUri($url)
                ->setConfig(array('timeout' => $this->_timeout ))
                ->request('GET')
                ->getBody();

            $converted = json_decode($response);
            $rate = $converted->rates->$currencyTo;

            if (!$rate) {
                $this->_messages[] = Mage::helper('directory')->__('Cannot retrieve rate from %s.', $url);
                return null;
            }

            // test for bcmath to retain precision
            if (function_exists('bcadd')) {
                return bcadd($rate, '0', 12);
            }

            return (float) $rate;
        } catch (Exception $e) {
            if ($retry == 0) {
                return $this->_convert($currencyFrom, $currencyTo, 1);
            } else {
                $this->_messages[] = Mage::helper('directory')->__('Cannot retrieve rate from %s.', $url);
                return null;
            }
        }
    }

}