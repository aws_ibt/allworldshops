<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Catalog
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */


/**
 * Bluecom Catalog Model Observer
 *
 * @category Mage
 * @package  Mage_Catalog
 * @author   Magento Core Team <core@magentocommerce.com>`
 */
class Bluecom_Catalog_Model_Observer extends Mage_Catalog_Model_Observer
{
    /**
     * FilterByShopCountry
     *
     * @param object $observer observer
     *
     * @return bool;
     */
    public function filterByShopCountry(&$observer)
    {


        if (Mage::app()->getRequest()->getParam('rssour')) {
            Mage::getSingleton('core/session')->unsShopByCountry();
        }

        if (Mage::app()->getRequest()->getParam('source')) {
            $source = Mage::app()->getRequest()->getParam('source');
            Mage::getSingleton('core/session')->setShopByCountry($source);
            Mage::app()->getRequest()->setPost('source', $source);
        } else {
            if ($this->_getIsHomePage()) {
                Mage::getSingleton('core/session')->unsShopByCountry();
            } else {
                if (Mage::getSingleton('core/session')->getShopByCountry()) {
                    $source = Mage::getSingleton('core/session')->getShopByCountry();
                    Mage::app()->getRequest()->setPost('source', $source);
                }
            }
        }

	    if (Mage::app()->getRequest()->getParam('udL')) {
		    Mage::getSingleton('core/session')->unsShopByCountry();
	    }
    }

    /**
     * Check is Home Page
     *
     * @return bool
     */
    private function _getIsHomePage()
    {
        return Mage::getUrl('') == Mage::getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true));
    }

    /**
     * AddMoreAttributeCatForFlatSelect
     *
     * @param object $observer observer
     *
     * @return mixed
     */
    public function addMoreAttributeCatForFlatSelect(&$observer)
    {
        $data = $observer->getEvent()->getData();

        $select = $data['select'];
        $columns = $select->getPart('columns');
        $columns[] = array('main_table','image',null);
        $columns[] = array('main_table','is_special_shop',null);
        $select->setPart('columns', $columns);
        $data['select'] = $select;
        $observer->getEvent()->setData($data);

        return $observer;
    }

    /**
     * AddCategoriesToMenu
     *
     * @param array|Varien_Data_Tree_Node_Collection $categories         categories
     * @param Varien_Data_Tree_Node                  $parentCategoryNode parentCategoryNode
     * @param Mage_Page_Block_Html_Topmenu           $menuBlock          menuBlock
     * @param bool|false                             $addTags            addTags
     *
     * @return mixed
     */
    protected function _addCategoriesToMenu($categories, $parentCategoryNode, $menuBlock, $addTags = false)
    {
        $categoryModel = Mage::getModel('catalog/category');
        foreach ($categories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }

            $nodeId = 'category-node-' . $category->getId();

            $categoryModel->setId($category->getId());
            if ($addTags) {
                $menuBlock->addModelTags($categoryModel);
            }

            $tree = $parentCategoryNode->getTree();
            $categoryData = array(
                'name' => $category->getName(),
                'id' => $nodeId,
                'cate_id' => $category->getId(),
                'url' => Mage::helper('catalog/category')->getCategoryUrl($category),
                'is_active' => $this->_isActiveMenuCategory($category),
                'image' => $category->getImage(),
                'is_special_shop' => $category->getIsSpecialShop(),
            );
            $categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $parentCategoryNode);
            $parentCategoryNode->addChild($categoryNode);

            $flatHelper = Mage::helper('catalog/category_flat');
            if ($flatHelper->isEnabled() && $flatHelper->isBuilt(true)) {
                $subcategories = (array)$category->getChildrenNodes();
            } else {
                $subcategories = $category->getChildren();
            }

            $this->_addCategoriesToMenu($subcategories, $categoryNode, $menuBlock, $addTags);
        }
    }

    /**
     * AssignBestSellerProduct
     *
     * @return bool
     */
    public function assignBestSellerProduct()
    {
        $date = new Zend_Date();
        $date_now = Zend_Date::now();
        $toDate = $date->setDay($date_now->get('d'))->getDate()->get('Y-MM-dd');
        $fromDate = $date->subMonth(1)->getDate()->get('Y-MM-dd');

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*');

        $collection->getSelect()
            ->join(
                array('aggregation' => $collection->getResource()->getTable('sales/bestsellers_aggregated_daily')),
                "e.entity_id = aggregation.product_id AND aggregation.period BETWEEN '{$fromDate}' AND '{$toDate}'",
                array('SUM(aggregation.qty_ordered) AS sold_quantity')
            )
            ->group('e.entity_id')
            ->order(array('sold_quantity DESC', 'e.created_at'));

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);

        $special_hotkeys   = Mage::helper('bluecom_initsetup')->getListSpecialShops();
        $hotkey_phrase     = Mage::helper('bluecom_initsetup')->getHotProductPhrase();
        $rootCategoryId = Mage::app()->getStore("gb_en")->getRootCategoryId();

        $special_categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter('parent_id', array('eq' => $rootCategoryId))
            ->addAttributeToFilter(
                'url_key', array('in' => $special_hotkeys)
            )
            ->load();

        $special_hotkeys_id = array();
        foreach ($special_categories as $special_category) {
            $special_hotkeys_id[$special_category->getId()] = $special_category->getUrlKey();
        }
        $special_hotkeys_id[$rootCategoryId] = 'gb';

        if (count($special_hotkeys_id) > 10) {
            exit;
        }

        $hot_product_categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter(
                'url_key', array('eq' => $hotkey_phrase)
            )
            ->load();

        if (count($hot_product_categories) > 10) {
            exit;
        }

        $hot_product_parent_ids = array();
        $hot_product_parent_objs = array();
        foreach ($hot_product_categories as $hot_product_category) {
            $iParentId = $hot_product_category->getParentCategory()->getId();
            if (array_key_exists($iParentId, $special_hotkeys_id)) {
                $hot_product_parent_ids[$iParentId] = $hot_product_category->getId();
                $hot_product_parent_objs[$hot_product_category->getId()] = $hot_product_category;
            }
        }


        $products_best_seller  = $collection->load();
        $hot_product_mapping_ids = array();

        if (count($products_best_seller)) {
            $special_hotkeys_id_compare =  array_keys($special_hotkeys_id);
            if (($key = array_search($rootCategoryId, $special_hotkeys_id_compare)) !== false) {
                unset($special_hotkeys_id_compare[$key]);
            }
            foreach ($products_best_seller as $product_best_seller) {
                if (null != $product_best_seller->getId()) {
                    if ($product_best_seller->getTypeId() == "simple") {
                        $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($product_best_seller->getId());
                        if (!$parentIds) {
                            $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product_best_seller->getId());
                        }
                        if (isset($parentIds[0])) {
                            $product_parent = Mage::getModel('catalog/product')->load($parentIds[0]);
                            if ($product_parent->getId()) {
                                $product_best_seller = $product_parent;
                            }
                        }
                    }

                    if (count(array_intersect($product_best_seller->getCategoryIds(), $special_hotkeys_id_compare)) >= 1) {
                        $special_category_ids = array_intersect($product_best_seller->getCategoryIds(), $special_hotkeys_id_compare);
                        foreach ($special_category_ids as $special_category_id) {
                            $special_category_hot_id = $hot_product_parent_ids[$special_category_id];
                            if (!isset($hot_product_mapping_ids[$special_category_hot_id])) {
                                $hot_product_mapping_ids[$special_category_hot_id] = array();
                            }
                            if (!in_array($product_best_seller->getId(), $hot_product_mapping_ids[$special_category_hot_id])) {
                                $hot_product_mapping_ids[$special_category_hot_id][] = $product_best_seller->getId();
                            }
                        }
                    }
                    $special_category_hot_id = $hot_product_parent_ids[$rootCategoryId];
                    if (!isset($hot_product_mapping_ids[$special_category_hot_id])) {
                        $hot_product_mapping_ids[$special_category_hot_id] = array();
                    }
                    if (!in_array($product_best_seller->getId(), $hot_product_mapping_ids[$special_category_hot_id])) {
                        $hot_product_mapping_ids[$special_category_hot_id][] = $product_best_seller->getId();
                    }
                }
            }
        }

        foreach ($hot_product_mapping_ids as $hot_product_category_id => $product_ids) {
            $hot_product_category_obj = $hot_product_parent_objs[$hot_product_category_id];
            if ($hot_product_category_obj) {
                $old_product_ids_pos = $hot_product_category_obj->getProductsPosition();
                $old_product_ids = array_keys($old_product_ids_pos);

                $remove_product_ids = array_diff($old_product_ids, $product_ids);
                $add_product_ids = array_diff($product_ids, $old_product_ids);

                foreach ($remove_product_ids as $remove_product_id) {
                    Mage::getSingleton('catalog/category_api')->removeProduct($hot_product_category_id, $remove_product_id);
                }

                foreach ($add_product_ids as $add_product_id) {
                    Mage::getSingleton('catalog/category_api')->assignProduct($hot_product_category_id, $add_product_id);
                }
            }
        }

        $process = Mage::getModel('index/indexer')->getProcessByCode('catalog_category_product');
        $process->reindexAll();
    }
}
