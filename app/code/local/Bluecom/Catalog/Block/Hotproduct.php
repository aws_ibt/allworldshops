<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Catalog
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */


/**
 * Bluecom Catalog Product Abstract Block
 *
 * @category Mage
 * @package  Mage_Catalog
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Catalog_Block_Hotproduct
    extends Mage_Catalog_Block_Product_Abstract
    implements Mage_Widget_Block_Interface
{
    /**
     * Default value for products count that will be shown
     */
    const DEFAULT_PRODUCTS_COUNT = 10;

    protected $_featureLists;

    /**
     * Get list products of feature category
     *
     * @return mixed
     */
    public function getFeatureProductsCollection()
    {
        if ($this->_featureLists === null) {
            $categoryID = $this->getProductsCategory();
            if (!sizeof($categoryID)) {
                return null;
            }
            $limit = $this->getColumnCount();
            if (!$limit) {
                $limit = self::DEFAULT_PRODUCTS_COUNT;
            }
            $collection = Mage::getModel('catalog/category')->load($categoryID[0])
                ->getProductCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', 4)
                ->setPageSize($limit);

            $sourceId = $this->getRequest()->getParam('source', 0);
            if ($sourceId) {
                $collection->addAttributeToFilter('source', $sourceId);
            }

            $this->_featureLists = $collection;
        }
        return $this->_featureLists;
    }

    /**
     * Get id category parameter in widget
     *
     * @return array
     */
    public function getProductsCategory()
    {
        $ret = array();
        foreach (explode(',', $this->getData('category')) as $cat) {
            $value = explode('/', $cat);
            if (sizeof($value) > 1) {
                $ret[] = intval($value[1]);
            }
        }
        return $ret;
    }

    /**
     * GetColumnCount
     *
     * @return mixed
     */
    public function getColumnCount()
    {
        return $this->getQuantity();
    }
}
