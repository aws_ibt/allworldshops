<?php

/**
 * Class Bluecom_Catalog_Block_Seo_Sitemap_Tree_Category
 */
class Bluecom_Catalog_Block_Seo_Sitemap_Tree_Category extends Mage_Catalog_Block_Seo_Sitemap_Tree_Category
{
    /**
     * Initialize categories collection
     *
     * @return Mage_Catalog_Block_Seo_Sitemap_Category
     */
    protected function _prepareLayout()
    {
        $helper = Mage::helper('catalog/category');
        /* @var $helper Mage_Catalog_Helper_Category */
        $parent = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load(Mage::app()->getStore()->getRootCategoryId());
        $this->_storeRootCategoryPath = $parent->getPath();
        $this->_storeRootCategoryLevel = $parent->getLevel();
        $this->prepareCategoriesToPages();
        $collection = $this->getTreeCollection();
        // change sort category in site map
        $collection->getSelect()->order('request_path');
        $collection->getSelect()->order('path');
        $this->setCollection($collection);
        return $this;
    }
}