<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Core
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom Catalog Helper Data
 *
 * @author Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * GetPreviousProduct
     *
     * @return bool
     */
    public function getPreviousProduct()
    {
        $productId = Mage::registry('current_product')->getId();
        $category = Mage::registry('current_category');

        if ($category) {
            $categoryId = $category->getData('entity_id');
            $_category = Mage::getModel('catalog/category')->load($categoryId);

            $_products_data = null;
            if (Mage::getSingleton('core/session')->getShopByCountry()) {
                $source = Mage::getSingleton('core/session')->getShopByCountry();
                $_products_data = $_category->getProductCollection()->addAttributeToFilter('source', $source)->addAttributeToSort('position', 'ASC')->getData();
            } else {
                $_products_data = $_category->getProductCollection()->addAttributeToSort('position', 'ASC')->getData();
            }

            $keys =array();
            $values = array();
            $index = 0;
            foreach ($_products_data as $_product_data) {
                $keys[$_product_data['entity_id']] = $index;
                $values[]                          = $_product_data['entity_id'];
                $index++;
            }

            if (isset($keys[$productId])) {
                return $this->getAvailableId($keys[$productId] - 1, $values, true);
            }
        }
        return false;
    }

    /**
     * GetNextProduct
     *
     * @return bool
     */
    public function getNextProduct()
    {
        $productId = Mage::registry('current_product')->getId();
        $category = Mage::registry('current_category');

        if ($category) {
            $categoryId = $category->getData('entity_id');
            $_category = Mage::getModel('catalog/category')->load($categoryId);

            $_products_data = null;
            if (Mage::getSingleton('core/session')->getShopByCountry()) {
                $source = Mage::getSingleton('core/session')->getShopByCountry();
                $_products_data = $_category->getProductCollection()->addAttributeToFilter('source', $source)->addAttributeToSort('position', 'ASC')->getData();
            } else {
                $_products_data = $_category->getProductCollection()->addAttributeToSort('position', 'ASC')->getData();
            }

            $keys =array();
            $values = array();
            $index = 0;
            foreach ($_products_data as $_product_data) {
                $keys[$_product_data['entity_id']] = $index;
                $values[]                          = $_product_data['entity_id'];
                $index++;
            }

            if (isset($keys[$productId])) {
                return $this->getAvailableId($keys[$productId] + 1, $values, false);
            }
        }
        return false;
    }

    /**
     * GetAvailableId
     *
     * @param string    $key  key
     * @param array     $list list
     * @param bool|true $prev prev
     *
     * @return bool
     */
    public function getAvailableId($key, $list, $prev = true)
    {
        if (!array_key_exists($key, $list)) {
            return false;
        }

        $id = $list[$key];

        $product = Mage::getModel('catalog/product');
        $product->load($id);
        if ($product->getStatus() == 1) {
            return $product->getProductUrl();
        } else {
            $prevId = $prev ? $key - 1 : $key + 1;
            return $this->getAvailableId($prevId, $list, $prev);
        }
    }
}
