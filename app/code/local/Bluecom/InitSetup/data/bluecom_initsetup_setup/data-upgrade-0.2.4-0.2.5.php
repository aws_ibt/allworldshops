<?php
/**
 * Upgrade 0.2.4
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$gb = Mage::app()->getStore('gb_en')->getId();
Mage::getModel('cms/block')->setStoreId($gb)->load('social_sharing')->setContent(
    '
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56fe4989a292ec77"></script>
<div class="addthis_sharing_toolbox"></div>
'
);

$installer->endSetup();