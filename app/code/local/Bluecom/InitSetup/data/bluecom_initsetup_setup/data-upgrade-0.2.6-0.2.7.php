<?php
/**
 * Upgrade 0.2.6
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$cn = Mage::app()->getStore('cn_en')->getId();
Mage::getModel('cms/block')->setStoreId($cn)->load('social_sharing')->setContent(
    '
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57020bd485383c75"></script>
<div class="addthis_sharing_toolbox"></div>
'
)->save();

$kr = Mage::app()->getStore('ko_en')->getId();
Mage::getModel('cms/block')->setStoreId($kr)->load('social_sharing')->setContent(
    '
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57020f0a06ea0757"></script>
<div class="addthis_sharing_toolbox"></div>
'
)->save();

$jp = Mage::app()->getStore('jp_en')->getId();
Mage::getModel('cms/block')->setStoreId($jp)->load('social_sharing')->setContent(
    '
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57020f52144d62f9"></script>
<div class="addthis_sharing_toolbox"></div>
'
)->save();

$installer->endSetup();