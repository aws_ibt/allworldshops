<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Core
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom_InitSetup_Helper_Data
 *
 * @author Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_InitSetup_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * GetNumberHotProductHomepage
     *
     * @param null $stordId storeId
     *
     * @return mixed
     */
    public function getNumberHotProductHomepage($stordId = null)
    {
        return Mage::getStoreConfig('bluecom_initsetup_catalog/catalog/number_hot_product', $stordId);
    }

    /**
     *GetListSpecialShops
     *
     * @param null $stordId storeId
     *
     * @return array
     */
    public function getListSpecialShops($stordId = null)
    {
        return explode(",", Mage::getStoreConfig('bluecom_initsetup_catalog/catalog/list_special_shops', $stordId));
    }

    /**
     * GetHotProductPhrase
     *
     * @param null $stordId stordId
     *
     * @return array
     */
    public function getHotProductPhrase($stordId = null)
    {
        return explode(",", Mage::getStoreConfig('bluecom_initsetup_catalog/catalog/hotproductphrase', $stordId));
    }
}
