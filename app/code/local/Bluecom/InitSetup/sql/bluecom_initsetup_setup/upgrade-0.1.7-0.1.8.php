<?php
/**
 * Upgrade 0.1.7
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$pageModel = Mage::getModel('cms/page')->load('home');
if ($pageModel->getId() != '') {
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"7\"}}
                <div class=\"home-gb-shop pull-right\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"8\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"9\"}}</div>
                </div>
                <div class=\"row home-shop-2\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"10\"}}{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"11\"}}{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"12\"}}</div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"13\"}}</div>
                <div class=\"row shop-world\">
                <div class=\"page-title\">
                <h2><span>SHOP THE WORLD!</span></h2>
                </div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"14\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"15\"}}</div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"16\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"17\"}}</div>
                </div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span></h2>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">View all</a></p>
                </div>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span></h2>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">View all</a></p>
                {{block type=\"customblock/hotproduct\" name=\"customblock_hotproduct\" template=\"customblock/hotproduct.phtml\" }}</div>
                </div>
                </div>
                </div>";

    $pageModel->setContent($content);
    $pageModel->save();
}

$installer->endSetup();