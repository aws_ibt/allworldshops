<?php
/**
 * Upgrade 0.3.4
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


$blockModel = Mage::getModel('cms/block')->load('homepage-gb-banner');
if ($blockModel->getId() != '') {
    $content = "<div class=\"home-banner col-sm-12\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-1.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-2.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Shop Now</a></p>
                </div>
                </div>
                </li>
                </ul>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-banner-toy')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-banner col-sm-12\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-3.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-4.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Shop Now</a></p>
                </div>
                </div>
                </li>
                </ul>
                </div>
                </div>";

    $staticBlock = array(
        'title' => 'Banner Special Toy',
        'identifier' => 'homepage-gb-banner-toy',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-banner-honey')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-banner col-sm-12\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-5.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-6.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Shop Now</a></p>
                </div>
                </div>
                </li>
                </ul>
                </div>
                </div>";

    $staticBlock = array(
        'title' => 'Banner Special Honey',
        'identifier' => 'homepage-gb-banner-honey',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-banner-baby')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-banner col-sm-12\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-7.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-8.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Shop Now</a></p>
                </div>
                </div>
                </li>
                </ul>
                </div>
                </div>";

    $staticBlock = array(
        'title' => 'Banner Special Baby',
        'identifier' => 'homepage-gb-banner-baby',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-banner-beauty')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-banner col-sm-12\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-2.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-1.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Shop Now</a></p>
                </div>
                </div>
                </li>
                </ul>
                </div>
                </div>";

    $staticBlock = array(
        'title' => 'Banner Special Beauty',
        'identifier' => 'homepage-gb-banner-beauty',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-banner-health')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-banner col-sm-12\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-2.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-3.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Shop Now</a></p>
                </div>
                </div>
                </li>
                </ul>
                </div>
                </div>";

    $staticBlock = array(
        'title' => 'Banner Special Health',
        'identifier' => 'homepage-gb-banner-health',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-banner-sheepskin')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-banner col-sm-12\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-4.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-5.png\"}}\" height=\"400\" width=\"792\" /></a>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Shop Now</a></p>
                </div>
                </div>
                </li>
                </ul>
                </div>
                </div>";

    $staticBlock = array(
        'title' => 'Banner Special Sheepskin',
        'identifier' => 'homepage-gb-banner-sheepskin',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

$installer->endSetup();
