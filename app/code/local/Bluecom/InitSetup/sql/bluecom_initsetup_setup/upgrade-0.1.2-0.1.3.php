<?php
/**
 *Upgrade 0.1.2
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

if (Mage::getModel('cms/block')->load('homepage-gb-banner')->getId() == null) {
    $content = "<div class=\"home-banner pull-left\">
            <img src=\"{{media url=\"wysiwyg/home/home-banner.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.
                    Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Start shopping</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Banners',
        'identifier' => 'homepage-gb-banner',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-honey-shop')->getId() == null) {
    //footer block 2
    $content = "<div class=\"col-12 honey-shop\">
                <img src=\"{{media url=\"wysiwyg/home/home-honey-shop.png\"}}\" alt=\"The honey shop\">
            </div>";

    $staticBlock = array(
        'title' => 'Honey Shop',
        'identifier' => 'homepage-gb-honey-shop',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-baby-shop')->getId() == null) {
    //footer block 2
    $content = "<div class=\"col-12 baby-shop\">
                <img src=\"{{media url=\"wysiwyg/home/home-baby-store.png\"}}\" alt=\"The Baby store\">
            </div>";

    $staticBlock = array(
        'title' => 'Baby Shop',
        'identifier' => 'homepage-gb-baby-shop',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('homepage-gb-beauty-shop')->getId() == null) {
    $content = "<div class=\"col-sm-4 first\">
            <img src=\"{{media url=\"wysiwyg/home/home-beauty-shop.png\"}}\" alt=\"The Beauty Shop\">
        </div>";

    $staticBlock = array(
        'title' => 'Beauty Shop',
        'identifier' => 'homepage-gb-beauty-shop',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


//Toy Shop
if (Mage::getModel('cms/block')->load('homepage-gb-toy-shop')->getId() == null) {
    $content = "<div class=\"col-sm-4 center\">
            <img src=\"{{media url=\"wysiwyg/home/home-toy-shop.png\"}}\" alt=\"The Toy Shop\">
        </div>";

    $staticBlock = array(
        'title' => 'Toy Shop',
        'identifier' => 'homepage-gb-toy-shop',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('homepage-gb-health-shop')->getId() == null) {
    $content = "<div class=\"col-sm-4 last\">
            <img src=\"{{media url=\"wysiwyg/home/home-health-shop.png\"}}\" alt=\"The Health Shop\">
        </div>";

    $staticBlock = array(
        'title' => 'Health Shop',
        'identifier' => 'homepage-gb-health-shop',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('homepage-view-all-shops')->getId() == null) {
    $content = "<div class=\"row-bottom\"><a href=\"#\" class=\"button2 btn-transparent\">View all shops</a></div>";

    $staticBlock = array(
        'title' => 'View All Shops',
        'identifier' => 'homepage-view-all-shops',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

//shop world


if (Mage::getModel('cms/block')->load('homepage-gb-cn-shop-world')->getId() == null) {
    $content = "<div class=\"pull-left world-shop-child\">
            <img src=\"{{media url=\"wysiwyg/home/shop-world-1.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <p class=\"title\">CHINA</p>
                <p>The Middle Kingdom’s finest</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Shop the World - China',
        'identifier' => 'homepage-gb-cn-shop-world',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-au-shop-world')->getId() == null) {
    //footer block 2
    $content = "<div class=\"pull-right world-shop-child\">
            <img src=\"{{media url=\"wysiwyg/home/shop-world-2.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <p class=\"title\">AUSTRALIA</p>
                <p>Stuff form down under</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Shop the World - Australia',
        'identifier' => 'homepage-gb-au-shop-world',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-nz-shop-world')->getId() == null) {
    //footer block 2
    $content = "<div class=\"pull-left world-shop-child\">
            <img src=\"{{media url=\"wysiwyg/home/shop-world-3.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <p class=\"title\">NEW ZEALAND</p>
                <p>Lorem ipsum dolor sit amet</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Shop the World – New Zeland',
        'identifier' => 'homepage-gb-nz-shop-world',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('homepage-gb-jp-shop-world')->getId() == null) {
    //footer block 2
    $content = "<div class=\"pull-right world-shop-child\">
            <img src=\"{{media url=\"wysiwyg/home/shop-world-4.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <p class=\"title\">JAPAN</p>
                <p>Lorem ipsum dolor sit amet</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Shop the World - Japan',
        'identifier' => 'homepage-gb-jp-shop-world',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

$installer->endSetup();
