<?php
/**
 * Upgrade 0.2.9
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$pageModel = Mage::getModel('cms/page')->load('home');
if ($pageModel->getId() != '') {
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"7\"}}</div>
                <div class=\"row shop-world\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"14\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"15\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"16\"}}</div>
                <div class=\"row home-shop-2\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"13\"}}
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"8\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"9\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"30\"}}</div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"10\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"11\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"12\"}}</div>
                </div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-categories/\">View all</a></span></h2>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-hot-products\">View all</a></span></h2>
                </div>
                {{block type=\"customblock/hotproduct\" name=\"customblock_hotproduct\" template=\"customblock/hotproduct.phtml\" }}</div>
                </div>";

    $pageModel->setContent($content);
    $pageModel->save();
}


$blockModel = Mage::getModel('cms/block')->load('homepage-view-all-shops');
if ($pageModel->getId() != '') {
    $content = "<div class=\"page-title\">
                <h2><span>SHOP THE WORLD!</span>
                <span class=\"row-bottom\">
                <a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-view-all-shops\">View all shops</a></span></h2>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-cn-shop-world');
if ($pageModel->getId() != '') {
    $content = "<div class=\"col-sm-4 first world-shop-child\"><a href=\"{{store ulr=''}}cnshop/\"><img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/shop-world-1.png\"}}\" height=\"190\" width=\"590\" /></a>
                <div class=\"thumbnail\">
                <p class=\"title\">CHINA</p>
                <p>The Middle Kingdom&rsquo;s finest</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"{{store ulr=''}}cnshop/\">Explore</a></p>
                </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-au-shop-world');
if ($pageModel->getId() != '') {
    $content = "<div class=\"col-sm-4 center world-shop-child\"><a href=\"{{store ulr=''}}aushop/\"><img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/shop-world-2.png\"}}\" height=\"190\" width=\"590\" /></a>
                <div class=\"thumbnail\">
                <p class=\"title\">AUSTRALIA</p>
                <p>Stuff form down under</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"{{store ulr=''}}aushop/\">Explore</a></p>
                </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-nz-shop-world');
if ($pageModel->getId() != '') {
    $content = "<div class=\"col-sm-4 last world-shop-child\"><a href=\"{{store ulr=''}}nzshop/\"><img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/shop-world-3.png\"}}\" height=\"190\" width=\"590\" /></a>
                <div class=\"thumbnail\">
                <p class=\"title\">NEW ZEALAND</p>
                <p>Lorem ipsum dolor sit amet</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"{{store ulr=''}}nzshop/\">Explore</a></p>
                </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-banner');
if ($pageModel->getId() != '') {
    $content = "<div class=\"home-banner col-sm-12\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-1.png\"}}\" height=\"400\" width=\"792\" /></a></li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-2.png\"}}\" height=\"400\" width=\"792\" /></a></li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-3.png\"}}\" height=\"400\" width=\"792\" /></a></li>
                </ul>
                </div>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-honey-shop');
if ($pageModel->getId() != '') {
    $content = "<div class=\"col-sm-4 first\">
                <a href=\"{{store url=\"\"}}honey.html\">
                <img alt=\"The honey shop\" src=\"{{media url=\"wysiwyg/home/home-honey-shop.png\"}}\" height=\"190\" width=\"388\" /></a>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-baby-shop');
if ($pageModel->getId() != '') {
    $content = "<div class=\"col-sm-4 center\"><a href=\"{{store url=\"\"}}baby.html\"><img alt=\"The Baby store\" src=\"{{media url=\"wysiwyg/home/home-baby-store.png\"}}\" height=\"190\" width=\"388\" /></a></div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-beauty-shop');
if ($pageModel->getId() != '') {
    $content = "<div class=\"col-sm-4 first\"><a href=\"{{store url=\"\"}}beauty.html\"><img alt=\"The Beauty Shop\" src=\"{{media url=\"wysiwyg/home/home-beauty-shop.png\"}}\" height=\"190\" width=\"388\" /></a></div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-toy-shop');
if ($pageModel->getId() != '') {
    $content = "<div class=\"col-sm-4 center\"><a href=\"{{store url=\"\"}}toy.html\"><img alt=\"The Toy Shop\" src=\"{{media url=\"wysiwyg/home/home-toy-shop.png\"}}\" height=\"190\" width=\"384\" /></a></div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-health-shop');
if ($pageModel->getId() != '') {
    $content = "<div class=\"col-sm-4 last\"><a href=\"{{store url=\"\"}}health.html\"><img alt=\"The Health Shop\" src=\"{{media url=\"wysiwyg/home/home-health-shop.png\"}}\" height=\"190\" width=\"388\" /></a></div>";

    $blockModel->setContent($content);
    $blockModel->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-sheepskin-shop')->getId() == null) {
    //footer block 2
    $content = "<div class=\"col-sm-4 last\">
                <a href=\"{{store url=\"\"}}sheepskin.html\">
                <img alt=\"The honey shop\" src=\"{{media url=\"wysiwyg/home/home-sheepskin-shop.png\"}}\" height=\"190\" width=\"388\" />
                </a>
                </div>";

    $staticBlock = array(
        'title' => 'SheepSkin Shop',
        'identifier' => 'homepage-gb-sheepskin-shop',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

//footer banners
if (Mage::getModel('cms/block')->load('footer-gb-banners')->getId() == null) {
    $content = "<div class=\"row footer-banner\">
    <div class=\"footer-container-child\">
    <div class=\"banner-left col-sm-6\">
    <div class=\"content\"><a href=\"#\"><img alt=\"\" src=\"{{media url=\"wysiwyg/footer_banner1.jpg\"}}\" /></a>
    <div class=\"desc-info\">
    <h2>Get a free bow tie</h2>
    <p>with every suit</p>
    <div class=\"row-bottom\">
    <p><a class=\"button2 btn-transparent\" href=\"#\">Explore</a></p>
        </div>
    </div>
    </div>
    </div>
    <div class=\"banner-right col-sm-6\">
    <div class=\"content\"><a href=\"#\"><img alt=\"\" src=\"{{media url=\"wysiwyg/footer_banner2.jpg\"}}\" /></a>
    <div class=\"desc-info\">
    <h2>Free shipping</h2>
    <p>and 50% off</p>
    <div class=\"row-bottom\">
    <p><a class=\"button2 btn-transparent\" href=\"#\">Explore</a></p>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>";

    $staticBlock = array(
        'title' => 'Footer Banners',
        'identifier' => 'footer-gb-banners',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}



$installer->endSetup();
