<?php
/**
 * Upgrade 0.2.7
 */
$installer = $this;

$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', 'country_of_manufacture');
$installer->updateAttribute(
    $entityTypeId, $attributeId, array(
    'used_in_product_listing' => 1
    )
);