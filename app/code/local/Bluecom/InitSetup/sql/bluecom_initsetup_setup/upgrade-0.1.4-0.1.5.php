<?php
/**
 * Upgrade 0.1.4
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

if (Mage::getModel('cms/block')->load('homepage-gb-center-block-1')->getId() == null) {
    $content = " <div class=\"col-sm-4 first\">
            <img src=\"{{media url=\"wysiwyg/home/category-1.png\"}}\" alt=\"Category 1\">
            <div class=\"thumbnail\">
                <p class=\"title\">MOTHER'S DAY IDEAS</p>
                <p>Be prepared this year</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore gifts and flowers</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Categories Block 1',
        'identifier' => 'homepage-gb-center-block-1',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-center-block-2')->getId() == null) {
    //footer block 2
    $content = "<div class=\"col-sm-4 center\">
            <img src=\"{{media url=\"wysiwyg/home/category-2.png\"}}\" alt=\"Category 2\">
            <div class=\"thumbnail\">
                <p class=\"title\">HONEY STORE</p>
                <p>Bzzz...</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Shop!</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Categories Block 2',
        'identifier' => 'homepage-gb-center-block-2',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('homepage-gb-center-block-3')->getId() == null) {
    //footer block 2
    $content = "<div class=\"col-sm-4 last\">
            <img src=\"{{media url=\"wysiwyg/home/category-3.png\"}}\" alt=\"Category 3\">
            <div class=\"thumbnail\">
                <p class=\"title\">OUTDOOR</p>
                <p>Stuff for outdoors</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Learn more</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Categories Block 3',
        'identifier' => 'homepage-gb-center-block-3',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('homepage-gb-center-block-4')->getId() == null) {
    $content = "<div class=\"col-sm-4 first\">
            <img src=\"{{media url=\"wysiwyg/home/category-4.png\"}}\" alt=\"Category 4\">
            <div class=\"thumbnail\">
                <p class=\"title\">LOREM IPSUM DOLRO</p>
                <p>Ut velit mauris, egestas sed</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore gifts and flowers</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Categories Block 4',
        'identifier' => 'homepage-gb-center-block-4',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


//Toy Shop
if (Mage::getModel('cms/block')->load('homepage-gb-center-block-5')->getId() == null) {
    $content = " <div class=\"col-sm-4 center\">
            <img src=\"{{media url=\"wysiwyg/home/category-5.png\"}}\" alt=\"Category 5\">
            <div class=\"thumbnail\">
                <p class=\"title\">NULLA SOLLICITUDIN</p>
                <p>Aliquam convallis sollicitudin purus</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Shop!</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Categories Block 5',
        'identifier' => 'homepage-gb-center-block-5',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('homepage-gb-center-block-6')->getId() == null) {
    $content = "<div class=\"col-sm-4 last\">
            <img src=\"{{media url=\"wysiwyg/home/category-6.png\"}}\" alt=\"Category 6\">
            <div class=\"thumbnail\">
                <p class=\"title\">NULLA ET SAPIEN</p>
                <p>Praesent aliquam, enim at fermentum</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Learn more</a></p></div>
            </div>
        </div>";

    $staticBlock = array(
        'title' => 'Categories Block 6',
        'identifier' => 'homepage-gb-center-block-6',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


$pageModel = Mage::getModel('cms/page')->load('home');
if ($pageModel->getId() != '') {
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"7\"}}
                <div class=\"home-gb-shop pull-right\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"8\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"9\"}}</div>
                </div>
                <div class=\"row home-shop-2\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"10\"}}{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"11\"}}{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"12\"}}</div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"13\"}}</div>
                <div class=\"row shop-world\">
                <div class=\"page-title\">
                <h2>SHOP THE WORLD!</h2>
                </div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"14\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"15\"}}</div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"16\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"17\"}}</div>
                </div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2>A TRULY INTERNATIONAL AND ECLECTIC RANGE</h2>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">View all</a></p>
                </div>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                </div>";

    $pageModel->setContent($content);
    $pageModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-banner');
if ($pageModel->getId() != '') {
    $content = "<div class=\"home-banner pull-left\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-1.png\"}}\" /></a></li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-2.png\"}}\" /></a></li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-3.png\"}}\" /></a></li>
                </ul>
                </div>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}


$blockModel = Mage::getModel('cms/block')->load('homepage-gb-cn-shop-world');
if ($pageModel->getId() != '') {
    $content = "<div class=\"pull-left world-shop-child\">
            <img src=\"{{media url=\"wysiwyg/home/shop-world-1.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <p class=\"title\">CHINA</p>
                <p>The Middle Kingdom’s finest</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore</a></p></div>
            </div>
        </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}


$blockModel = Mage::getModel('cms/block')->load('homepage-gb-au-shop-world');
if ($pageModel->getId() != '') {
    $content = "<div class=\"pull-right world-shop-child\">
            <img src=\"{{media url=\"wysiwyg/home/shop-world-2.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <p class=\"title\">AUSTRALIA</p>
                <p>Stuff form down under</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore</a></p></div>
            </div>
        </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}


$blockModel = Mage::getModel('cms/block')->load('homepage-gb-nz-shop-world');
if ($pageModel->getId() != '') {
    $content = "<div class=\"pull-left world-shop-child\">
            <img src=\"{{media url=\"wysiwyg/home/shop-world-3.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <p class=\"title\">NEW ZEALAND</p>
                <p>Lorem ipsum dolor sit amet</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore</a></p></div>
            </div>
        </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}


$blockModel = Mage::getModel('cms/block')->load('homepage-gb-jp-shop-world');
if ($pageModel->getId() != '') {
    $content = "<div class=\"pull-right world-shop-child\">
            <img src=\"{{media url=\"wysiwyg/home/shop-world-4.png\"}}\" alt=\"Home Banner\">
            <div class=\"thumbnail\">
                <p class=\"title\">JAPAN</p>
                <p>Lorem ipsum dolor sit amet</p>
                <div class=\"row-bottom\"><p><a href=\"#\" class=\"button2 btn-transparent\">Explore</a></p></div>
            </div>
        </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

if (Mage::getModel('cms/block')->load('banner-category')->getId() == null) {
    $content = "<p class=\"banner-category\"><img alt=\"\" src=\"{{media url=\"wysiwyg/banner.jpg\"}}\" />
                    </p>";

    $staticBlock = array(
        'title' => 'Banner Category',
        'identifier' => 'banner-category',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('footer-banners')->getId() == null) {
    $content = "<div class=\"row footer-banner\">
<div class=\"footer-container-child\">
<div class=\"banner-left col-sm-6\">
<div class=\"content\"><img alt=\"\" src=\"{{media url=\"wysiwyg/footer_banner1.jpg\"}}\" />
<div class=\"desc-info\">
<h2>Get a free bow tie</h2>
<p>with every suit</p>
<div class=\"row-bottom\">
<p><a class=\"button2 btn-transparent\" href=\"#\">Explore</a></p>
    </div>
</div>
</div>
</div>
<div class=\"banner-right col-sm-6\">
<div class=\"content\"><img alt=\"\" src=\"{{media url=\"wysiwyg/footer_banner2.jpg\"}}\" />
<div class=\"desc-info\">
<h2>Free shipping</h2>
<p>and 50% off</p>
<div class=\"row-bottom\">
<p><a class=\"button2 btn-transparent\" href=\"#\">Explore</a></p>
</div>
</div>
</div>
</div>
</div>
</div>";

    $staticBlock = array(
        'title' => 'Footer Banners',
        'identifier' => 'footer-banners',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

$installer->endSetup();
