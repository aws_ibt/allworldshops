<?php
/**
 * Upgrade 0.2.0
 */
$installer = $this;

$installer->startSetup();

//config customer
Mage::getModel('core/config')->saveConfig('customer/account_share/scope', 0, 'default', 0);

//create customer group
$vip = Mage::getModel('customer/group')->load('VIP', 'customer_group_code');
if (!$vip->getCode()) {
    $vip->setCode('VIP')->setTaxClassId(3)->save();
}
//remove useless groups
$retailer = Mage::getModel('customer/group')->load('Retailer', 'customer_group_code');
if ($retailer->getCode()) {
    $retailer->delete();
}
$wholesale = Mage::getModel('customer/group')->load('Wholesale', 'customer_group_code');
if ($wholesale->getCode()) {
    $wholesale->delete();
}

//add customer attribute
$installer->addAttribute(
    'customer', 'age_group', array(
    'type' => 'varchar',
    'input' => 'select',
    'label' => 'Age group',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default' => '0',
    'visible_on_front' => 1,
    'source' => 'eav/entity_attribute_source_table',
    'option' => array('values' => array('18-20', '21-30', '31-40', '41-50', '50+'))
    )
);

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'age_group')
    ->setData('used_in_forms', array('adminhtml_customer', 'customer_account_create', 'customer_account_edit', 'checkout_register'))
    ->save();

$installer->addAttribute(
    'customer', 'interest', array(
    'type' => 'varchar',
    'input' => 'multiselect',
    'label' => 'Interest',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default' => '0',
    'visible_on_front' => 1,
    'source' => 'eav/entity_attribute_source_table',
    'option' => array('values' => array('Leisure', 'Beauty'))
    )
);

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'interest')
    ->setData('used_in_forms', array('adminhtml_customer', 'customer_account_create', 'customer_account_edit', 'checkout_register'))
    ->save();

$installer->endSetup();