<?php
/**
 * Upgrade 0.1.1
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// footer block 1
if (Mage::getModel('cms/block')->load('footer-gb-newsletter')->getId() == null) {
    $content = "<div class='footer-container-child'>
                    <div class='link-bottoms'>
                    <div class='newsletter-footer'>{{block type='core/template' template='newsletter/subscribe.phtml'}}</div>
                    <div class='block block-fllow'>
                    <div class='block-title'><strong><span>Fllow us</span></strong></div>
                    <p>Get great deals and discounts direct to your feed!</p>
                    <a id='button-face' href='#'><i class='icon-face'>icon</i>Fllow us on facebook</a></div>
                    </div>
                    </div>";

    $staticBlock = array(
        'title' => 'Footer Block 1',
        'identifier' => 'footer-gb-newsletter',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


if (Mage::getModel('cms/block')->load('footer-gb-shipping')->getId() == null) {
    //footer block 2
    $content = "<div class='footer-shipping'>
                    <div class='content-footer'>
                    <div class='feature'>
                    <div class='content'>
                    <div class='icon-item  first'>icon</div>
                    <div class='article-right'>
                    <h3>Free shipping</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    </div>
                    </div>
                    <div class='feature'>
                    <div class='content'>
                    <div class='icon-item second'>icon</div>
                    <div class='article-right'>
                    <h3>Customer service team</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    </div>
                    </div>
                    <div class='feature'>
                    <div class='content'>
                    <div class='icon-item last'>icon</div>
                    <div class='article-right'>
                    <h3>Return purchased items</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>";

    $staticBlock = array(
        'title' => 'Footer Block 2',
        'identifier' => 'footer-gb-shipping',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

//footer block 3
if (Mage::getModel('cms/block')->load('footer-gb-navigation')->getId() == null) {
    $content = "<div class=\"footer-container-child\">
                    <div class=\"footer-links\">
                    <ul id=\"nav\">
                    <li class=\"links-colunm\">
                    <h3 class=\"block-title\">Corporate</h3>
                    <ul>
                    <li><a href=\"{{store url=\"\"}}about-us.html/\">About us</a></li>
                    <li><a href=\"#\">Contact us</a></li>
                    <li><a href=\"#\">Site map</a></li>
                    <li><a href=\"#\">Affiliates</a></li>
                    <li><a href=\"#\">Terms and conditions</a></li>
                    <li><a href=\"#\">About webitvoucher&reg;</a></li>
                    </ul>
                    </li>
                    <li class=\"links-colunm\">
                    <h3 class=\"block-title\">Customer service</h3>
                    <ul>
                    <li><a href=\"#\">Customer service team</a></li>
                    <li><a href=\"#\">Delivery info</a></li>
                    <li><a href=\"#\">Free Delivery </a></li>
                    <li><a href=\"#\">Faq's</a></li>
                    <li><a href=\"#\">Get help</a></li>
                    </ul>
                    </li>
                    <li class=\"links-colunm\">
                    <h3 class=\"block-title\">Security &amp; payment</h3>
                    <ul>
                    <li><a href=\"#\">About webitcach&reg;</a></li>
                    <li><a href=\"#\">Privacy Policy</a></li>
                    </ul>
                    </li>
                    <li class=\"links-colunm\">
                    <h3 class=\"block-title\">Multipurpose</h3>
                    <ul class=\"desc-info\">
                    <li><span class=\"icon icon-map\">map</span>Block 32, Defu lane 10, #02 -30 Singapore - 539213</li>
                    <li><span class=\"icon icon-mail\">mail</span><a href=\"mailto:cutomerservice@allworldshops.com\">cutomerservice@allworldshops.com</a></li>
                    <li><span class=\"icon icon-fone\">fone</span>+65 65464744</li>
                    </ul>
                    </li>
                    </ul>
                    </div>
                    </div>";

    $staticBlock = array(
        'title' => 'Footer Block 3',
        'identifier' => 'footer-gb-navigation',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


//footer block 4
if (Mage::getModel('cms/block')->load('footer-gb-copyright')->getId() == null) {
    $content = "<div class=\"logo-footer\">
                   <a title=\"logo footer\" href=\"#\">logo</a>
                    </div>";

    $staticBlock = array(
        'title' => 'Footer Block 4',
        'identifier' => 'footer-gb-copyright',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


$installer->endSetup();
