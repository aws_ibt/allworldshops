<?php
/**
 * Upgrade 0.3.3
 */
$installer = $this;
$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$cat_global = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect("*")
    ->addFieldToFilter("parent_id", array("eq"=>1))->getFirstItem();

$create_category = new Mage_Catalog_Model_Category();
$create_category->setName('Hot Products');
$create_category->setUrlKey('hot-products');
$create_category->setStoreId(0);
$create_category->setIsActive(1);
$create_category->setDisplayMode('PRODUCTS');
$create_category->setIsAnchor(0);
$create_category->setIncludeInMenu(0);
$create_category->setPath($cat_global->getPath());
$create_category->save();

$installer->endSetup();


