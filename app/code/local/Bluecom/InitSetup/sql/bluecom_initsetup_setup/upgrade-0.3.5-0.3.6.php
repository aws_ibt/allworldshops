<?php
/**
 * Upgrade 0.3.5
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


if (Mage::getModel('cms/block')->load('special-homepage-toy')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"35\"}}</div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-categories/\">View all</a></span></h2>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-hot-products\">View all</a></span></h2>
                </div>
                {{widget type=\"bluecom_catalog/hotproduct\" template=\"customblock/hotproduct.phtml\"}}</div>
                </div>";

    $staticBlock = array(
        'title' => 'Special Homepage Toy',
        'identifier' => 'special-homepage-toy',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('special-homepage-honey')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"36\"}}</div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-categories/\">View all</a></span></h2>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-hot-products\">View all</a></span></h2>
                </div>
                {{widget type=\"bluecom_catalog/hotproduct\" template=\"customblock/hotproduct.phtml\"}}</div>
                </div>";

    $staticBlock = array(
        'title' => 'Special Homepage Honey',
        'identifier' => 'special-homepage-honey',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('special-homepage-baby')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"37\"}}</div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-categories/\">View all</a></span></h2>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-hot-products\">View all</a></span></h2>
                </div>
                {{widget type=\"bluecom_catalog/hotproduct\" template=\"customblock/hotproduct.phtml\"}}</div>
                </div>";

    $staticBlock = array(
        'title' => 'Special Homepage Baby',
        'identifier' => 'special-homepage-baby',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('special-homepage-beauty')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"38\"}}</div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-categories/\">View all</a></span></h2>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-hot-products\">View all</a></span></h2>
                </div>
                {{widget type=\"bluecom_catalog/hotproduct\" template=\"customblock/hotproduct.phtml\"}}</div>
                </div>";

    $staticBlock = array(
        'title' => 'Special Homepage Beauty',
        'identifier' => 'special-homepage-beauty',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('special-homepage-health')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"39\"}}</div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-categories/\">View all</a></span></h2>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-hot-products\">View all</a></span></h2>
                </div>
                {{widget type=\"bluecom_catalog/hotproduct\" template=\"customblock/hotproduct.phtml\"}}</div>
                </div>";

    $staticBlock = array(
        'title' => 'Special Homepage Health',
        'identifier' => 'special-homepage-health',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

if (Mage::getModel('cms/block')->load('special-homepage-sheepskin')->getId() == null) {
    //footer block 2
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"40\"}}</div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-categories/\">View all</a></span></h2>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-hot-products\">View all</a></span></h2>
                </div>
                {{widget type=\"bluecom_catalog/hotproduct\" template=\"customblock/hotproduct.phtml\"}}</div>
                </div>";

    $staticBlock = array(
        'title' => 'Special Homepage Sheepskin',
        'identifier' => 'special-homepage-sheepskin',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}

$installer->endSetup();
