<?php
/**
 *  Upgrade 0.1.9
 */
$installer = $this;

$installer->startSetup();

//config for paypal
Mage::getModel('core/config')->saveConfig('payment/paypal_express/active', 0, 'default', 0);
Mage::getModel('core/config')->saveConfig('paypal/general/business_account', 'info@allworldshops.com', 'default', 0);
Mage::getModel('core/config')->saveConfig('paypal/wpp/api_authentication', 0, 'default', 0);
Mage::getModel('core/config')->saveConfig('paypal/wpp/api_username', Mage::helper('core')->encrypt('info_api1.allworldshops.com'), 'default', 0);
Mage::getModel('core/config')->saveConfig('paypal/wpp/api_password', Mage::helper('core')->encrypt('VMCADEGWV7U3ES6M'), 'default', 0);
Mage::getModel('core/config')->saveConfig('paypal/wpp/api_signature', Mage::helper('core')->encrypt('AFljHQxNJmFJj6SbMmf79RzcdHNEARy70qiCNwjsVW0xvGzB.ufEkN9b'), 'default', 0);
Mage::getModel('core/config')->saveConfig('payment/paypal_express/title', 'Paypal', 'default', 0);
Mage::getModel('core/config')->saveConfig('payment/paypal_express/sort_order', 1, 'default', 0);

Mage::getModel('core/config')->saveConfig('payment/paypal_express/verify_peer', 1, 'default', 0); //only temporary disable ssl


//config for alipay
Mage::getModel('core/config')->saveConfig('payment/alipay/active', 0, 'default', 0);
Mage::getModel('core/config')->saveConfig('payment/alipay/title', 'Alipay', 'default', 0);
Mage::getModel('core/config')->saveConfig('payment/alipay/partner_id', '2088 8012 3980 9266', 'default', 0);
Mage::getModel('core/config')->saveConfig('payment/alipay/seller_email', 'info@allworldshops.com', 'default', 0);

//config worldpay


//addition setting for store view
$my_website_id= Mage::getModel('core/website')->load('my', 'code')->getId();
Mage::getModel('core/config')->saveConfig('general/locale/code', 'en_US', 'websites', $my_website_id);

$my_ko_id= Mage::getModel('core/website')->load('kr', 'code')->getId();
Mage::getModel('core/config')->saveConfig('catalog/custom_options/date_fields_order', 'y,m,d', 'websites', $my_ko_id);

$my_jp_id= Mage::getModel('core/website')->load('jp', 'code')->getId();
Mage::getModel('core/config')->saveConfig('catalog/custom_options/date_fields_order', 'y,m,d', 'websites', $my_jp_id);

$my_cn_id= Mage::getModel('core/website')->load('cn', 'code')->getId();
Mage::getModel('core/config')->saveConfig('catalog/custom_options/date_fields_order', 'y,m,d', 'websites', $my_cn_id);

$installer->endSetup();