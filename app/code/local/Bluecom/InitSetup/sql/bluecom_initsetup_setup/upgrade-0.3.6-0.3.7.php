<?php
/**
 * Upgrade 0.3.6
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-cn-shop-world');
if ($blockModel->getId() != '') {

    $content = "<div class=\"col-sm-4 first world-shop-child\" style=\"background-image: url({{media url='wysiwyg/home/shop-world-1.png'}}); background-size: cover;\">
                <a href=\"{{store ulr=''}}?source=30\"> &nbsp; </a>
                <div class=\"thumbnail\">
                <p class=\"title\">CHINA</p>
                <p>The Middle Kingdom&rsquo;s finest</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"{{store ulr=''}}?source=30\">Explore</a></p>
                </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-au-shop-world');
if ($blockModel->getId() != '') {

    $content = "<div class=\"col-sm-4 center world-shop-child\" style=\"background-image: url({{media url='wysiwyg/home/shop-world-2.png'}}); background-size: cover;\">
                <a href=\"{{store ulr=''}}?source=31\"> &nbsp; </a>
                <div class=\"thumbnail\">
                <p class=\"title\">AUSTRALIA</p>
                <p>Stuff form down under</p>
                <div class=\"row-bottom\">
                    <p><a class=\"button2 btn-transparent\" href=\"{{store ulr=''}}?source=31\">Explore</a></p>
                </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}


$blockModel = Mage::getModel('cms/block')->load('homepage-gb-nz-shop-world');
if ($blockModel->getId() != '') {

    $content = "<div class=\"col-sm-4 last world-shop-child\" style=\"background-image: url({{media url='wysiwyg/home/shop-world-3.png'}}); background-size: cover;\">
                <a href=\"{{store ulr=''}}?source=32\"> &nbsp; </a>
                <div class=\"thumbnail\" style=\"padding-bottom: 0;\">
                    <p class=\"title\">NEW ZEALAND</p>
                    <p>Lorem ipsum dolor sit amet</p>
                    <div class=\"row-bottom\">
                        <p><a class=\"button2 btn-transparent\" href=\"{{store ulr=''}}?source=32\">Explore</a></p>
                    </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$installer->endSetup();