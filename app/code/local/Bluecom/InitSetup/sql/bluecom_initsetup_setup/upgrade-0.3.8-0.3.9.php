<?php
/**
 * Upgrade 0.3.8
 */
$installer = $this;

$installer->startSetup();
Mage::getModel('core/config')->saveConfig('bluecom_initsetup_catalog/catalog/time', '0 */12 * * *', 'default', 0);
$installer->endSetup();
