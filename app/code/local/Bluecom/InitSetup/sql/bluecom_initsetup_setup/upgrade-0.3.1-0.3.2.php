<?php
/**
 * Upgrade 0.3.1
 */
$installer = $this;
$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$cat_global = Mage::getModel('catalog/category')->getCollection()->setPageSize(1)
    ->addAttributeToSelect("*")
    ->addFieldToFilter("parent_id", array("eq"=>1))->getFirstItem();

$iGlobalRootCategoryId = $cat_global->getId();
$categories_level1 = Mage::getModel('catalog/category')->load($iGlobalRootCategoryId)->getChildrenCategories();
$special_shops = Mage::helper('bluecom_initsetup')->getListSpecialShops();
foreach ($categories_level1 as $category_level1) {
    if (in_array($category_level1->getRequestPath(), $special_shops)) {
        $create_category = new Mage_Catalog_Model_Category();
        $create_category->setName('Hot Products');
        $create_category->setUrlKey('hot-products');
        $create_category->setStoreId(0);
        $create_category->setIsActive(1);
        $create_category->setDisplayMode('PRODUCTS');
        $create_category->setIsAnchor(0);
        $create_category->setIncludeInMenu(0);
        $create_category->setPath($category_level1->getPath());
        $create_category->save();
    }
}

$pageModel = Mage::getModel('cms/page')->load('home');
if ($pageModel->getId() != '') {
    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"7\"}}</div>
                <div class=\"row shop-world\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"14\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"15\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"16\"}}</div>
                <div class=\"row home-shop-2\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"13\"}}
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"8\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"9\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"30\"}}</div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"10\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"11\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"12\"}}</div>
                </div>
                <div class=\"row home-category-list\">
                <div class=\"page-title\">
                <h2><span>A TRULY INTERNATIONAL AND ECLECTIC RANGE</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-categories/\">View all</a></span></h2>
                </div>
                <div class=\"row first\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"18\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"19\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"20\"}}</div>
                <div class=\"row last\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"21\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"22\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"23\"}}</div>
                </div>
                <div class=\"row hot-product\">
                <div class=\"page-title\">
                <h2><span>HOT PRODUCTS</span><span class=\"row-bottom\"><a class=\"button2 btn-transparent\" href=\"{{store url=''}}homepage-gb-hot-products\">View all</a></span></h2>
                </div>
                {{widget type=\"bluecom_catalog/hotproduct\" template=\"customblock/hotproduct.phtml\"}}</div>
                </div>";

    $pageModel->setContent($content);
    $pageModel->save();
}

$installer->endSetup();
