<?php
/**
 * Upgrade 0.1.0
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

Mage::register('isSecureArea', true);
$default_store_view = Mage::getModel('core/store')->load('default');
$main_website_store = Mage::getModel('core/store_group')->load($default_store_view->getGroupId());
$main_website = Mage::getModel('core/website')->load($default_store_view->getWebsiteId());

$default_store_view->delete();
$main_website_store->delete();
$main_website->delete();

$iDefaultRootCat = 2;
$root_default_cat = Mage::getModel('catalog/category')->load($iDefaultRootCat)->delete();
Mage::unregister('isSecureArea');

$installer->endSetup();