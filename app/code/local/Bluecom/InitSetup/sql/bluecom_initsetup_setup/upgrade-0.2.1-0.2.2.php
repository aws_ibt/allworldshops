<?php
/**
 * Upgrade 0.2.1
 */
$installer = $this;

$installer->startSetup();

//config tax
Mage::getModel('core/config')->saveConfig('tax/classes/shipping_tax_class', 4);
Mage::getModel('core/config')->saveConfig('tax/defaults/country', 'SG');
Mage::getModel('core/config')->saveConfig('tax/cart_display/full_summary', 1);

//get the product tax class
$productTaxClass = Mage::getModel('tax/class')
    ->getCollection()
    ->addFieldToFilter('class_name', 'Taxable Goods')
    ->load()
    ->setPageSize(1)
    ->getFirstItem();

//get the customer tax class
$customerTaxClass = Mage::getModel('tax/class')
    ->getCollection()
    ->addFieldToFilter('class_name', 'Retail Customer')
    ->load()
    ->setPageSize(1)
    ->getFirstItem();

$cnCode = 'VAT-17%-China';
$jpCode = 'VAT-8%-Japan';
$krCode = 'VAT-10%-Korea';
$sgCode = 'GST-7%-Singapore';
$myCode = 'GST-6%-Malaysia';
$auCode = 'GST-10%-Australia';
$nzCode = 'GST-15%-New Zeland';
//create tax rate/zone
$chinaTaxRate = Mage::getModel('tax/calculation_rate')
    ->setData(
        array(
        'code'                  => $cnCode,
        'tax_country_id'        => 'CN',
        'tax_region_id'         => '0',
        'zip_is_range'          => '0',
        'tax_postcode'          => '*',
        'rate'                  => '17',
        )
    )->save();
$japanTaxRate = Mage::getModel('tax/calculation_rate')
    ->setData(
        array(
        'code'                  => $jpCode,
        'tax_country_id'        => 'JP',
        'tax_region_id'         => '0',
        'zip_is_range'          => '0',
        'tax_postcode'          => '*',
        'rate'                  => '8',
        )
    )->save();
$koreaTaxRate = Mage::getModel('tax/calculation_rate')
    ->setData(
        array(
        'code'                  => $krCode,
        'tax_country_id'        => 'KR',
        'tax_region_id'         => '0',
        'zip_is_range'          => '0',
        'tax_postcode'          => '*',
        'rate'                  => '10',
        )
    )->save();
$singTaxRate = Mage::getModel('tax/calculation_rate')
    ->setData(
        array(
        'code'                  => $sgCode,
        'tax_country_id'        => 'SG',
        'tax_region_id'         => '0',
        'zip_is_range'          => '0',
        'tax_postcode'          => '*',
        'rate'                  => '7',
        )
    )->save();
$malayTaxRate = Mage::getModel('tax/calculation_rate')
    ->setData(
        array(
        'code'                  => $myCode,
        'tax_country_id'        => 'MY',
        'tax_region_id'         => '0',
        'zip_is_range'          => '0',
        'tax_postcode'          => '*',
        'rate'                  => '6',
        )
    )->save();
$auTaxRate = Mage::getModel('tax/calculation_rate')
    ->setData(
        array(
        'code'                  => $auCode,
        'tax_country_id'        => 'AU',
        'tax_region_id'         => '0',
        'zip_is_range'          => '0',
        'tax_postcode'          => '*',
        'rate'                  => '10',
        )
    )->save();
$nzTaxRate = Mage::getModel('tax/calculation_rate')
    ->setData(
        array(
        'code'                  => $nzCode,
        'tax_country_id'        => 'NZ',
        'tax_region_id'         => '0',
        'zip_is_range'          => '0',
        'tax_postcode'          => '*',
        'rate'                  => '15',
        )
    )->save();

//create tax rule
Mage::getModel('tax/calculation_rule')
    ->setData(
        array(
        'code'                  => $cnCode,
        'tax_customer_class'    => array($customerTaxClass->getId()),
        'tax_product_class'     => array($productTaxClass->getId()),
        'tax_rate'              => array($chinaTaxRate->getId()),
        'priority'              => '1',
        'position'              => '1',
        )
    )->save();
Mage::getModel('tax/calculation_rule')
    ->setData(
        array(
        'code'                  => $jpCode,
        'tax_customer_class'    => array($customerTaxClass->getId()),
        'tax_product_class'     => array($productTaxClass->getId()),
        'tax_rate'              => array($japanTaxRate->getId()),
        'priority'              => '2',
        'position'              => '2',
        )
    )->save();
Mage::getModel('tax/calculation_rule')
    ->setData(
        array(
        'code'                  => $krCode,
        'tax_customer_class'    => array($customerTaxClass->getId()),
        'tax_product_class'     => array($productTaxClass->getId()),
        'tax_rate'              => array($koreaTaxRate->getId()),
        'priority'              => '3',
        'position'              => '3',
        )
    )->save();
Mage::getModel('tax/calculation_rule')
    ->setData(
        array(
        'code'                  => $sgCode,
        'tax_customer_class'    => array($customerTaxClass->getId()),
        'tax_product_class'     => array($productTaxClass->getId()),
        'tax_rate'              => array($singTaxRate->getId()),
        'priority'              => '4',
        'position'              => '4',
        )
    )->save();
Mage::getModel('tax/calculation_rule')
    ->setData(
        array(
        'code'                  => $myCode,
        'tax_customer_class'    => array($customerTaxClass->getId()),
        'tax_product_class'     => array($productTaxClass->getId()),
        'tax_rate'              => array($malayTaxRate->getId()),
        'priority'              => '5',
        'position'              => '5',
        )
    )->save();
Mage::getModel('tax/calculation_rule')
    ->setData(
        array(
        'code'                  => $auCode,
        'tax_customer_class'    => array($customerTaxClass->getId()),
        'tax_product_class'     => array($productTaxClass->getId()),
        'tax_rate'              => array($auTaxRate->getId()),
        'priority'              => '6',
        'position'              => '6',
        )
    )->save();
Mage::getModel('tax/calculation_rule')
    ->setData(
        array(
        'code'                  => $nzCode,
        'tax_customer_class'    => array($customerTaxClass->getId()),
        'tax_product_class'     => array($productTaxClass->getId()),
        'tax_rate'              => array($nzTaxRate->getId()),
        'priority'              => '7',
        'position'              => '7',
        )
    )->save();

$installer->endSetup();