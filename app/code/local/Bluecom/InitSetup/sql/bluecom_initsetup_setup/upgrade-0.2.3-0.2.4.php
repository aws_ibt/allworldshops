<?php
/**
 * Upgrade 0.2.3
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//point reward
Mage::getModel('core/config')->saveConfig('enterprise_reward/general/min_points_balance', 10);
Mage::getModel('core/config')->saveConfig('enterprise_reward/general/expiration_days', 365);
Mage::getModel('core/config')->saveConfig('enterprise_reward/general/refund_automatically', 1);
Mage::getModel('core/config')->saveConfig('enterprise_reward/points/order', 1);
Mage::getModel('core/config')->saveConfig('enterprise_reward/points/register', 10);
Mage::getModel('core/config')->saveConfig('enterprise_reward/points/invitation_customer', 100);
Mage::getModel('core/config')->saveConfig('enterprise_reward/points/review', 10);

$gbWebsiteId = Mage::getResourceModel('core/website_collection')->addFieldToFilter('code', 'gb')->setPageSize(1)->getFirstItem()->getId();
Mage::getModel('enterprise_reward/reward_rate')->setData(
    array(
    'website_id' => $gbWebsiteId,
    'customer_group_id' => 0,
    'direction' => 1,
    'value' => 100,
    'equal_value' => 1
    )
)->save();
$cnWebsiteId = Mage::getResourceModel('core/website_collection')->addFieldToFilter('code', 'cn')->setPageSize(1)->getFirstItem()->getId();
Mage::getModel('enterprise_reward/reward_rate')->setData(
    array(
    'website_id' => $cnWebsiteId,
    'customer_group_id' => 0,
    'direction' => 1,
    'value' => 100,
    'equal_value' => 1
    )
)->save();
$jpWebsiteId = Mage::getResourceModel('core/website_collection')->addFieldToFilter('code', 'jp')->setPageSize(1)->getFirstItem()->getId();
Mage::getModel('enterprise_reward/reward_rate')->setData(
    array(
    'website_id' => $jpWebsiteId,
    'customer_group_id' => 0,
    'direction' => 1,
    'value' => 100,
    'equal_value' => 1
    )
)->save();
$krWebsiteId = Mage::getResourceModel('core/website_collection')->addFieldToFilter('code', 'kr')->setPageSize(1)->getFirstItem()->getId();
Mage::getModel('enterprise_reward/reward_rate')->setData(
    array(
    'website_id' => $krWebsiteId,
    'customer_group_id' => 0,
    'direction' => 1,
    'value' => 100,
    'equal_value' => 1
    )
)->save();
$myWebsiteId = Mage::getResourceModel('core/website_collection')->addFieldToFilter('code', 'my')->setPageSize(1)->getFirstItem()->getId();
Mage::getModel('enterprise_reward/reward_rate')->setData(
    array(
    'website_id' => $myWebsiteId,
    'customer_group_id' => 0,
    'direction' => 1,
    'value' => 100,
    'equal_value' => 1
    )
)->save();

$installer->endSetup();