<?php
/**
 * Upgrade 0.1.3
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$pageModel = Mage::getModel('cms/page')->load('home');
if ($pageModel->getId() != '') {

    $content = "<div class=\"home-container\">
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"7\"}}
                <div class=\"home-gb-shop pull-right\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"8\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"9\"}}</div>
                </div>
                <div class=\"row home-shop-2\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"10\"}}{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"11\"}}{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"12\"}}</div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"13\"}}</div>
                <div class=\"row shop-world\">
                <div class=\"page-title\">
                <h2>SHOP THE WORLD!</h2>
                </div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"14\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"15\"}}</div>
                <div class=\"row\">{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"16\"}} {{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"17\"}}</div>
                </div>
                </div>";

    $pageModel->setContent($content);
    $pageModel->save();
}

$blockModel = Mage::getModel('cms/block')->load('homepage-gb-banner');
if ($pageModel->getId() != '') {

    $content = "<div class=\"home-banner pull-left\">
                <div class=\"slideshow-container\">
                <ul class=\"slideshow\">
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-1.png\"}}\" /></a></li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-2.png\"}}\" /></a></li>
                <li><a href=\"#\"> <img alt=\"Home Banner\" src=\"{{media url=\"wysiwyg/home/home-banner-3.png\"}}\" /></a></li>
                </ul>
                </div>
                <div class=\"thumbnail\">
                <h1>NATIONAL PRODUCTS TO THE WORLD</h1>
                <p>What we do best is lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor.</p>
                <div class=\"row-bottom\">
                <p><a class=\"button2 btn-transparent\" href=\"#\">Start shopping</a></p>
                </div>
                </div>
                </div>";

    $blockModel->setContent($content);
    $blockModel->save();
}

$installer->endSetup();