<?php
/**
 * Install 0.1.0
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Config Store View
$iAdminStoreId    = Mage_Core_Model_App::ADMIN_STORE_ID;
$tree_root_id = Mage_Catalog_Model_Category::TREE_ROOT_ID;
$tree_root_id_cat = Mage::getModel('catalog/category')->load($tree_root_id);

//Global Category
$root_global_cat = Mage::getModel('catalog/category');
$root_global_cat->setStoreId($iAdminStoreId)->setName('Global')->setUrlKey('Global')->setIsActive(1)->setDisplayMode('PRODUCTS');
$root_global_cat->setPath($tree_root_id_cat->getPath())->save();

//Global
$gb_website = Mage::getModel('core/website');
$gb_website->setCode('gb')->setName('AWS Global')->setSortOrder(1)->setIsDefault(1)->save();

$gpStoreGroup = Mage::getModel('core/store_group');
$gpStoreGroup->setWebsiteId($gb_website->getId())->setName('AWS Global')->setRootCategoryId($root_global_cat->getId())->save();

$gp_store = Mage::getModel('core/store');
$gp_store->setCode('gb_en')
    ->setWebsiteId($gpStoreGroup->getWebsiteId())
    ->setGroupId($gpStoreGroup->getId())
    ->setName('English')
    ->setIsActive(1)
    ->save();

//China Category
$root_china_cat = Mage::getModel('catalog/category');
$root_china_cat->setStoreId($iAdminStoreId)->setName('China')->setUrlKey('China')->setIsActive(1)->setDisplayMode('PRODUCTS');
$root_china_cat->setPath($tree_root_id_cat->getPath())->save();

//China
$cn_website = Mage::getModel('core/website');
$cn_website->setCode('cn')->setName('AWS China')->setSortOrder(2)->save();

$cnStoreGroup = Mage::getModel('core/store_group');
$cnStoreGroup->setWebsiteId($cn_website->getId())->setName('AWS Chinese')->setRootCategoryId($root_china_cat->getId())->save();

$cn_zh_store = Mage::getModel('core/store');
$cn_zh_store->setCode('cn_zh')
    ->setWebsiteId($cnStoreGroup->getWebsiteId())
    ->setGroupId($cnStoreGroup->getId())
    ->setName('Chinese')
    ->setIsActive(1)
    ->save();

$cn_en_store = Mage::getModel('core/store');
$cn_en_store->setCode('cn_en')
    ->setWebsiteId($cnStoreGroup->getWebsiteId())
    ->setGroupId($cnStoreGroup->getId())
    ->setName('English')
    ->setIsActive(1)
    ->save();


//Japan Category
$root_japan_cat = Mage::getModel('catalog/category');
$root_japan_cat->setStoreId($iAdminStoreId)->setName('Japan')->setUrlKey('Japan')->setIsActive(1)->setDisplayMode('PRODUCTS');
$root_japan_cat->setPath($tree_root_id_cat->getPath())->save();

//Japan
$jp_website = Mage::getModel('core/website');
$jp_website->setCode('jp')->setName('AWS Japan')->setSortOrder(3)->save();

$jpStoreGroup = Mage::getModel('core/store_group');
$jpStoreGroup->setWebsiteId($jp_website->getId())->setName('AWS Japanese')->setRootCategoryId($root_japan_cat->getId())->save();

$jp_jp_store = Mage::getModel('core/store');
$jp_jp_store->setCode('jp_jp')
    ->setWebsiteId($jpStoreGroup->getWebsiteId())
    ->setGroupId($jpStoreGroup->getId())
    ->setName('Japanese')
    ->setIsActive(1)
    ->save();

$jp_en_store = Mage::getModel('core/store');
$jp_en_store->setCode('jp_en')
    ->setWebsiteId($jpStoreGroup->getWebsiteId())
    ->setGroupId($jpStoreGroup->getId())
    ->setName('English')
    ->setIsActive(1)
    ->save();

//Korea Category
$root_korea_cat = Mage::getModel('catalog/category');
$root_korea_cat->setStoreId($iAdminStoreId)->setName('Korea')->setUrlKey('Korea')->setIsActive(1)->setDisplayMode('PRODUCTS');
$root_korea_cat->setPath($tree_root_id_cat->getPath())->save();

//Korea
$kr_website = Mage::getModel('core/website');
$kr_website->setCode('kr')->setName('AWS Korea')->setSortOrder(4)->save();

$krStoreGroup = Mage::getModel('core/store_group');
$krStoreGroup->setWebsiteId($kr_website->getId())->setName('AWS Korean')->setRootCategoryId($root_korea_cat->getId())->save();

$ko_kr_store = Mage::getModel('core/store');
$ko_kr_store->setCode('ko_kr')
    ->setWebsiteId($krStoreGroup->getWebsiteId())
    ->setGroupId($krStoreGroup->getId())
    ->setName('Korean')
    ->setIsActive(1)
    ->save();

$ko_en_store = Mage::getModel('core/store');
$ko_en_store->setCode('ko_en')
    ->setWebsiteId($krStoreGroup->getWebsiteId())
    ->setGroupId($krStoreGroup->getId())
    ->setName('English')
    ->setIsActive(1)
    ->save();

//Maylaysia Category
$root_maylaysia_cat = Mage::getModel('catalog/category');
$root_maylaysia_cat->setStoreId($iAdminStoreId)->setName('Malaysia')->setUrlKey('Malaysia')->setIsActive(1)->setDisplayMode('PRODUCTS');
$root_maylaysia_cat->setPath($tree_root_id_cat->getPath())->save();

//Maylaysia
$my_website = Mage::getModel('core/website');
$my_website->setCode('my')->setName('AWS Malaysia')->setSortOrder(5)->save();

$myStoreGroup = Mage::getModel('core/store_group');
$myStoreGroup->setWebsiteId($my_website->getId())->setName('AWS Malaysian')->setRootCategoryId($root_maylaysia_cat->getId())->save();

$my_en_store = Mage::getModel('core/store');
$my_en_store->setCode('my_en')
    ->setWebsiteId($myStoreGroup->getWebsiteId())
    ->setGroupId($myStoreGroup->getId())
    ->setName('English')
    ->setIsActive(1)
    ->save();


//config global setting
Mage::getModel('core/config')->saveConfig('general/country/default', 'SG');
Mage::getModel('core/config')->saveConfig('general/locale/timezone', 'Asia/Singapore');
Mage::getModel('core/config')->saveConfig('general/locale/code', 'en_US');
Mage::getModel('core/config')->saveConfig('general/locale/firstday', 1);

Mage::getModel('core/config')->saveConfig('currency/options/base', 'USD');
Mage::getModel('core/config')->saveConfig('currency/options/default', 'USD');
Mage::getModel('core/config')->saveConfig('currency/options/allow', 'USD,CNY,JPY,KRW,MYR,NZD,SGD,AUD,EUR,GBP');


Mage::getModel('core/config')->saveConfig('catalog/price/scope', '1'); //set scope website for base currency.
Mage::getModel('core/config')->saveConfig('web/url/use_store', '1'); //add store code to urls

//config store view setting
//for currency
Mage::getModel('core/config')->saveConfig('currency/options/default', 'CNY', 'websites', $cn_website->getId());
Mage::getModel('core/config')->saveConfig('currency/options/base', 'CNY', 'websites', $cn_website->getId());

Mage::getModel('core/config')->saveConfig('currency/options/default', 'JPY', 'websites', $jp_website->getId());
Mage::getModel('core/config')->saveConfig('currency/options/base', 'JPY', 'websites', $jp_website->getId());

Mage::getModel('core/config')->saveConfig('currency/options/default', 'KRW', 'websites', $kr_website->getId());
Mage::getModel('core/config')->saveConfig('currency/options/base', 'KRW', 'websites', $kr_website->getId());

Mage::getModel('core/config')->saveConfig('currency/options/default', 'MYR', 'websites', $my_website->getId());
Mage::getModel('core/config')->saveConfig('currency/options/base', 'MYR', 'websites', $my_website->getId());

//for locale
Mage::getModel('core/config')->saveConfig('general/country/default', 'CN', 'websites', $cn_website->getId());
Mage::getModel('core/config')->saveConfig('general/locale/code', 'zh_CN', 'websites', $cn_website->getId());

Mage::getModel('core/config')->saveConfig('general/country/default', 'JP', 'websites', $jp_website->getId());
Mage::getModel('core/config')->saveConfig('general/locale/code', 'ja_JP', 'websites', $jp_website->getId());

Mage::getModel('core/config')->saveConfig('general/country/default', 'KR', 'websites', $kr_website->getId());
Mage::getModel('core/config')->saveConfig('general/locale/code', 'ko_KR', 'websites', $kr_website->getId());

Mage::getModel('core/config')->saveConfig('general/country/default', 'MY', 'websites', $my_website->getId());
Mage::getModel('core/config')->saveConfig('general/locale/code', 'ms_MY', 'websites', $my_website->getId());

$installer->endSetup();