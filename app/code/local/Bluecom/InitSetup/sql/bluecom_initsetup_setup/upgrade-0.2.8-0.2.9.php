<?php
/**
 * Upgrade 0.2.8
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

//footer banners
if (Mage::getModel('cms/block')->load('footer-gb-banners')->getId() == null) {
    $content = "<div class=\"row footer-banner\">
<div class=\"footer-container-child\">
<div class=\"banner-left col-sm-6\">
<div class=\"content\"><img alt=\"\" src=\"{{media url=\"wysiwyg/footer_banner1.jpg\"}}\" />
<div class=\"desc-info\">
<h2>Get a free bow tie</h2>
<p>with every suit</p>
<div class=\"row-bottom\">
<p><a class=\"button2 btn-transparent\" href=\"#\">Explore</a></p>
    </div>
</div>
</div>
</div>
<div class=\"banner-right col-sm-6\">
<div class=\"content\"><img alt=\"\" src=\"{{media url=\"wysiwyg/footer_banner2.jpg\"}}\" />
<div class=\"desc-info\">
<h2>Free shipping</h2>
<p>and 50% off</p>
<div class=\"row-bottom\">
<p><a class=\"button2 btn-transparent\" href=\"#\">Explore</a></p>
</div>
</div>
</div>
</div>
</div>
</div>";

    $staticBlock = array(
        'title' => 'Footer Banners',
        'identifier' => 'footer-gb-banners',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}
$installer->endSetup();
