<?php
/**
 * Upgrade 0.3.7
 */
$installer = $this;
$installer->startSetup();
$attribute  = array(
    'type'          =>  'varchar',
    'label'         =>  'Logo Special Shop',
    'input'         =>  'image',
    'backend'        => 'catalog/category_attribute_backend_image',
    'global'        =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       =>  true,
    'required'      =>  false,
    'user_defined'  =>  true,
    'default'       =>  "",
    'group'         =>  "General Information"
);
$installer->addAttribute('catalog_category', 'logo_special_shop', $attribute);
$installer->endSetup();
