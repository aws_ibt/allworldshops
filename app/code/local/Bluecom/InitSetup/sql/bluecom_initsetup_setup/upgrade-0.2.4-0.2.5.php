<?php
/**
 * Upgrade 0.2.4
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

//footer block 3
if (Mage::getModel('cms/block')->load('footer-gb-navigation')->getId() == null) {
    $content = "<div class=\"footer-container-child\">
                    <div class=\"footer-links\">
                    <ul id=\"nav\">
                    <li class=\"links-colunm\">
                    <h3 class=\"block-title\">Corporate</h3>
                    <ul>
                    <li><a href=\"{{store url=\"\"}}about-us.html/\">About us</a></li>
                    <li><a href=\"#\">Contact us</a></li>
                    <li><a href=\"#\">Site map</a></li>
                    <li><a href=\"#\">Affiliates</a></li>
                    <li><a href=\"#\">Terms and conditions</a></li>
                    <li><a href=\"#\">About webitvoucher&reg;</a></li>
                    </ul>
                    </li>
                    <li class=\"links-colunm\">
                    <h3 class=\"block-title\">Customer service</h3>
                    <ul>
                    <li><a href=\"#\">Customer service team</a></li>
                    <li><a href=\"#\">Delivery info</a></li>
                    <li><a href=\"#\">Free Delivery </a></li>
                    <li><a href=\"#\">Faq's</a></li>
                    <li><a href=\"#\">Get help</a></li>
                    </ul>
                    </li>
                    <li class=\"links-colunm\">
                    <h3 class=\"block-title\">Security &amp; payment</h3>
                    <ul>
                    <li><a href=\"#\">About webitcach&reg;</a></li>
                    <li><a href=\"#\">Privacy Policy</a></li>
                    </ul>
                    </li>
                    <li class=\"links-colunm\">
                    <h3 class=\"block-title\">Multipurpose</h3>
                    <ul class=\"desc-info\">
                    <li><span class=\"icon icon-map\">map</span>Block 32, Defu lane 10, #02 -30 Singapore - 539213</li>
                    <li><span class=\"icon icon-mail\">mail</span><a href=\"mailto:cutomerservice@allworldshops.com\">cutomerservice@allworldshops.com</a></li>
                    <li><span class=\"icon icon-fone\">fone</span>+65 65464744</li>
                    </ul>
                    </li>
                    </ul>
                    </div>
                    </div>";

    $staticBlock = array(
        'title' => 'Footer Block 3',
        'identifier' => 'footer-gb-navigation',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );

    Mage::getModel('cms/block')->setData($staticBlock)->save();
}


$installer->endSetup();
