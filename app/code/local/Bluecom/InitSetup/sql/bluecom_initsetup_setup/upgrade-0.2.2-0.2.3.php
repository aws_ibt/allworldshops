<?php
/**
 * Upgrade 0.2.2
 */
$installer = $this;

$installer->startSetup();

//config inventory
Mage::getModel('core/config')->saveConfig('cataloginventory/options/show_out_of_stock', 1);
Mage::getModel('core/config')->saveConfig('cataloginventory/item_options/backorders', 2);

$installer->endSetup();