<?php
/**
 * Upgrade 0.3.0
 */
$installer = $this;
$installer->startSetup();
$attribute  = array(
    'type'          =>  'int',
    'label'         =>  'Is Special Shop',
    'input'         =>  'select',
    'source'        => 'eav/entity_attribute_source_boolean',
    'global'        =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       =>  true,
    'required'      =>  false,
    'user_defined'  =>  true,
    'default'       =>  "",
    'group'         =>  "General Information"
);
$installer->addAttribute('catalog_category', 'is_special_shop', $attribute);
$installer->endSetup();
