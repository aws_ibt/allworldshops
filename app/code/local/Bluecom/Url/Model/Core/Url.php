<?php

/**
 * Class Bluecom_Url_Model_Core_Url
 */
class Bluecom_Url_Model_Core_Url extends Mage_Core_Model_Url {

    /**
     * Build url by requested path and parameters
     *
     * @param string|null $routePath
     * @param array|null $routeParams
     * @return  string
     */
    public function getUrl($routePath = null, $routeParams = null)
    {
        $escapeQuery = false;

        /**
         * All system params should be unset before we call getRouteUrl
         * this method has condition for adding default controller and action names
         * in case when we have params
         */
        if (isset($routeParams['_fragment'])) {
            $this->setFragment($routeParams['_fragment']);
            unset($routeParams['_fragment']);
        }

        if (isset($routeParams['_escape'])) {
            $escapeQuery = $routeParams['_escape'];
            unset($routeParams['_escape']);
        }

        $query = null;
        if (isset($routeParams['_query'])) {
            $this->purgeQueryParams();
            $query = $routeParams['_query'];
            unset($routeParams['_query']);
        }

        $noSid = null;
        if (isset($routeParams['_nosid'])) {
            $noSid = (bool)$routeParams['_nosid'];
            unset($routeParams['_nosid']);
        }

        $url = $this->getRouteUrl($routePath, $routeParams);
        /**
         * Apply query params, need call after getRouteUrl for rewrite _current values
         */
        if ($query !== null) {
            if (is_string($query)) {
                $this->setQuery($query);
            } elseif (is_array($query)) {
                $this->setQueryParams($query, !empty($routeParams['_current']));
            }
            if ($query === false) {
                $this->setQueryParams(array());
            }
        }

        if ($noSid !== true) {
            $this->_prepareSessionUrl($url);
        }

        $query = $this->getQuery($escapeQuery);
        if ($query) {
            $mark = (strpos($url, '?') === false) ? '?' : ($escapeQuery ? '&amp;' : '&');
            $url .= $mark . $query;
        }

        /**
         * Check param udL is exits
         * If exists add to url
         */
        $udLParam = Mage::app()->getRequest()->getParam('udL');

        $helperPage = Mage::helper('bluecom_page');
        $correctUdL = $helperPage->checkCorrectUdLParam($udLParam);

        // check udL param is special category
        if ($correctUdL) {

            $urlCheck = $helperPage->changeParamUdLCorrect($url, $udLParam);

            $baseByCategory = Mage::getBaseUrl() . '?udL=' . $udLParam;
            if ($urlCheck != '' && $url != Mage::getBaseUrl() && $url != $baseByCategory) {
                $url = $urlCheck;
            }
        }

        if ($this->getFragment()) {
            $url .= '#' . $this->getFragment();
        }

        return $this->escape($url);
    }

    /**
     * Rebuild URL to handle the case when session ID was changed
     *
     * @param string $url
     * @return string
     */
    public function getRebuiltUrl($url)
    {
        $this->parseUrl($url);
        $port = $this->getPort();
        if ($port) {
            $port = ':' . $port;
        } else {
            $port = '';
        }
        $url = $this->getScheme() . '://' . $this->getHost() . $port . $this->getPath();

        $this->_prepareSessionUrl($url);

        $query = $this->getQuery();
        if ($query) {
            $url .= '?' . $query;
        }

        /**
         * Check param udL is exits
         * If exists add to url
         */
        $udLParam = Mage::app()->getRequest()->getParam('udL');

        $helperPage = Mage::helper('bluecom_page');
        $correctUdL = $helperPage->checkCorrectUdLParam($udLParam);

        // check udL param is special category
        if ($correctUdL) {

            $urlCheck = $helperPage->changeParamUdLCorrect($url, $udLParam);

            $baseByCategory = Mage::getBaseUrl() . '?udL=' . $udLParam;
            if ($urlCheck != '' && $url != Mage::getBaseUrl() && $url != $baseByCategory) {
                $url = $urlCheck;
            }
        }

        $fragment = $this->getFragment();
        if ($fragment) {
            $url .= '#' . $fragment;
        }

        return $this->escape($url);
    }

}