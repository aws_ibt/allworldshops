<?php
require_once 'Mage/Directory/controllers/CurrencyController.php';

/**
 * Class Bluecom_CurrencyManager_CurrencyController
 */
class Bluecom_CurrencyManager_CurrencyController extends Mage_Directory_CurrencyController {

    /**
     * Identify referer url via all accepted methods (HTTP_REFERER, regular or base64-encoded request param)
     *
     * @return string
     */
    protected function _getRefererUrl()
    {
        $refererUrl = $this->getRequest()->getServer('HTTP_REFERER');
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_REFERER_URL)) {
            $refererUrl = $url;
        }
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_BASE64_URL)) {
            $refererUrl = Mage::helper('core')->urlDecodeAndEscape($url);
        }
        $udl = $this->getRequest()->getParam('udL');
        if (!$udl) {
            if ($url = $this->getRequest()->getParam(self::PARAM_NAME_URL_ENCODED)) {
                $refererUrl = Mage::helper('core')->urlDecodeAndEscape($url);
            }
        }

        if (!$this->_isUrlInternal($refererUrl)) {
            $refererUrl = Mage::app()->getStore()->getBaseUrl();
        }
        return $refererUrl;
    }

}