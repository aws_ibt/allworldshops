<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Enterprise
 * @package     Enterprise_GoogleAnalyticsUniversal
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

class Bluecom_GoogleAnalyticsUniversal_Model_Observer extends Enterprise_GoogleAnalyticsUniversal_Model_Observer
{
	/**
     * Adds to checkout shipping address step and review step GA block with related data
     * Fired by controller_action_postdispatch_checkout event
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function setGoogleAnalyticsOnCheckout( $observer )
    {
	    if (!Mage::helper('enterprise_googleanalyticsuniversal')->isTagManagerAvailable()) {
	        return $this;
	    }
	    /** @var Mage_Checkout_OnepageController $controllerAction */
	    $controllerAction = $observer->getEvent()->getControllerAction();
	    $action = $controllerAction->getRequest()->getRequestedActionName();
	     switch ($action) {
	        case 'saveBilling':
	            $body = $controllerAction->getResponse()->getBody();
	            $body = Mage::helper('core')->jsonDecode($body, true);
	            if ($body['goto_section'] == 'shipping') {
	                $shippingBlock = $controllerAction->getLayout()
	                    ->createBlock('enterprise_googleanalyticsuniversal/list_json')
	                    ->setTemplate('googleanalyticsuniversal/checkout/step.phtml')
	                    ->setStepName('shipping');
	                $body['update_section']['name'] = 'shipping';
	                $body['update_section']['html'] = '<div id="checkout-shipping-load"></div>'
	                    . $shippingBlock->toHtml();
	                $controllerAction->getResponse()->setBody(Mage::helper('core')->jsonEncode($body));
	            }
	            break;
	         case 'saveShippingMethod':
	             $shippingOption = Mage::getSingleton('checkout/session')->getQuote()
	                 ->getShippingAddress()->getShippingDescription();
	             $blockShippingMethod = Mage::app()->getLayout()
	                 ->createBlock('enterprise_googleanalyticsuniversal/list_json')
	                 ->setTemplate('googleanalyticsuniversal/checkout/set_checkout_option.phtml')
	                 ->setStepName('shipping_method')
	                 ->setShippingOption($shippingOption);

	             $body = $controllerAction->getResponse()->getBody();
	             $body = Mage::helper('core')->jsonDecode($body, true);
	             if (!empty($body['update_section']['html'])) {
	                 $body['update_section']['html'] = $blockShippingMethod->toHtml() . $body['update_section']['html'];
	             }
	             $controllerAction->getResponse()->setBody(Mage::helper('core')->jsonEncode($body));
	             break;
	        case 'savePayment':
	            $reviewBlock = $controllerAction->getLayout()
	                ->createBlock('enterprise_googleanalyticsuniversal/list_json')
	                ->setTemplate('googleanalyticsuniversal/checkout/step.phtml')
	                ->setStepName('review');

	            $paymentMethod = Mage::getSingleton('checkout/session')->getQuote()
	                ->getPayment()->getMethod();

	             
	            if('paypal_express' == $paymentMethod){
	            	return $this;
	            }
	            $paymentOption = Mage::getStoreConfig('payment/' . $paymentMethod . '/title');
	            $blockPaymentMethod = Mage::app()->getLayout()
	                ->createBlock('enterprise_googleanalyticsuniversal/list_json')
	                ->setTemplate('googleanalyticsuniversal/checkout/set_checkout_option.phtml')
	                ->setStepName('payment')
	                ->setShippingOption($paymentOption);

	            $body = $controllerAction->getResponse()->getBody();
	            $body = Mage::helper('core')->jsonDecode($body, true);
	            if (!empty($body['update_section']['html'])) {
	                $body['update_section']['html'] = $blockPaymentMethod->toHtml()
	                    . $body['update_section']['html'] . $reviewBlock->toHtml();
	            } else {
	                $body['update_section']['html'] = $reviewBlock->toHtml();
	            }
	            $controllerAction->getResponse()->setBody(Mage::helper('core')->jsonEncode($body));
	            break;
	    }
	    return $this;    
    }
}