<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 *
 */

class Bluecom_Alipayinternational_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function setStatus($order,$status,$comment){
        $statusCollection = Mage::getResourceModel('sales/order_status_collection')->joinStates();
        $stateMap = array();
        foreach($statusCollection as $item){
            $stateMap[$item->getState()] = $item->getStatus();
        }

        if(array_key_exists($status, $stateMap)){
            $order->setState($status,$status,$comment,true);
        }else{
            foreach($stateMap as $state=>$statusArray){
                if(in_array($status, $statusArray)){
                    $order->setState($state,$status,$comment,true);
                    break;
                }
            }
        }
    }

}
