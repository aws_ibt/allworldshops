<?php

/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 *
 */
class Bluecom_Alipayinternational_PaymentController extends Mage_Core_Controller_Front_Action {

    /**
     * Order instance
     */
    protected $_order;

    /**
     *  Get order
     *
     *  @param    none
     *  @return      Mage_Sales_Model_Order
     */
    public function getOrder() {
        if ($this->_order == null) {
            $session = Mage::getSingleton('checkout/session');
            $this->_order = Mage::getModel('sales/order');
            $this->_order->loadByIncrementId($session->getLastRealOrderId());
        }
        return $this->_order;
    }

    /**
     * When a customer chooses Alipay on Checkout/Payment page
     */
    public function redirectAction() {
        $session = Mage::getSingleton('checkout/session');
        $session->setAlipayPaymentQuoteId($session->getQuoteId());

        $order = $this->getOrder();

        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }

        $order->addStatusToHistory(
                $order->getStatus(), Mage::helper('alipayinternational')->__('Customer was redirected to Alipay')
        );
        $order->save();

        $this->getResponse()
                ->setBody($this->getLayout()
                        ->createBlock('alipayinternational/redirect')
                        ->setOrder($order)
                        ->toHtml());

        $session->unsQuoteId();
    }

    /**
     * page that receives alipay's notification
     *
     *  @param    none
     *  @return      void
     */
    public function notifyAction() {
        $verify_result = Mage::getModel('alipayinternational/verify')->notify_verify();
        if(!$verify_result) echo "fail";
        $currentStore = Mage::app()->getStore()->getId();
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
        } else if ($this->getRequest()->isGet()) {
            $postData = $this->getRequest()->getQuery();
        } else {
            echo "fail";
            return;
        }
        Mage::log($postData, null, 'alipay_notify_data.log');

        /* @var $alipay Bluecom_Alipayinternational_Model_Payment */
        $alipay = Mage::getModel('alipayinternational/payment');
        $incrementId = $alipay->outTradeNoToOrderNo($postData['out_trade_no']);
        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($incrementId);
        $_helper = Mage::helper('alipayinternational');
        if($order->getStatus() == Mage_Sales_Model_Order::STATE_COMPLETE || $order->getStatus() == Mage_Sales_Model_Order::STATE_PROCESSING){
            echo "fail";
            return;
        }
        if (($alipay->getConfigData('service_type') == 'alipay.trade.direct.forcard.pay' || $alipay->getConfigData('service_type') == 'create_forex_trade') && $postData['trade_status'] == 'TRADE_SUCCESS') {
            //$order->setAlipayTradeno($postData['trade_no']);
            if ($alipay->saveInvoice($order)) {
                $_helper->setStatus(
                    $order,
                    $alipay->getConfigData('alipay_order_status_trade_success'),
                    Mage::helper('alipayinternational')->__('payment successful。 trade no:').$postData['trade_no']
                );
            } else {
                $_helper->setStatus(
                    $order,
                    Mage_Sales_Model_Order::STATE_PROCESSING,
                    'Save invoice fail.'
                    );
            }
            try {
                $order->save();
                echo "success";
                //$this->_redirect('alipayinternational/payment/success');
            } catch (Exception $e) {
                Mage::logException($e);
                echo "fail";
            }
        } elseif (($alipay->getConfigData('service_type') == 'alipay.trade.direct.forcard.pay' || $alipay->getConfigData('service_type') == 'create_forex_trade') && $postData['trade_status'] == 'TRADE_FINISHED') {
            if ($alipay->saveInvoice($order)) {
                $_helper->setStatus(
                    $order,
                    $alipay->getConfigData('alipay_order_status_trade_finished'),
                    Mage::helper('alipayinternational')->__('trade success and finished, cannot refund。 trade no:').$postData['trade_no']
                    );
            } else {
                $_helper->setStatus(
                    $order,
                    Mage_Sales_Model_Order::STATE_PROCESSING,
                    'Save invoice fail.'
                    );
            }
            try {
                $order->save();
                //$this->_redirect('alipayinternational/payment/success');
                echo "success";
            } catch (Exception $e) {
                Mage::logException($e);
                echo "fail";
            }
        } else if (($alipay->getConfigData('service_type') == 'alipay.trade.direct.forcard.pay' || $alipay->getConfigData('service_type') == 'create_forex_trade') && $postData['trade_status'] == 'TRADE_CLOSED') {
            $_helper->setStatus(
                    $order,
                    $alipay->getConfigData('alipay_order_status_expired'),
                    'Transaction closed!'
                    );
            try {
                $order->cancel()->save();
                echo "success";
                //$this->_redirect('alipayinternational/payment/success');
            } catch (Exception $e) {
                Mage::logException($e);
                echo "fail";
            }
        } else {
            echo "fail";
        }
        Mage::app()->setCurrentStore($currentStore);
    }

    /**
     *  Success payment page
     *
     *  @param    none
     *  @return      void
     */
    public function successAction() {
        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getAlipayPaymentQuoteId());
        $session->unsAlipayPaymentQuoteId();
        $order = $this->getOrder();
        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }
        $order->addStatusToHistory(
                $order->getStatus(), Mage::helper('alipayinternational')->__('Customer successfully returned from Alipay')
        );
        $order->sendNewOrderEmail();
        $order->setEmailSent(true);

        $order->save();
        $this->_redirect('checkout/onepage/success');
    }

    /**
     *  Failure payment page
     *
     *  @param    none
     *  @return      void
     */
    public function errorAction() {
        $session = Mage::getSingleton('checkout/session');
        $errorMsg = Mage::helper('alipayinternational')->__(' There was an error occurred during paying process.');

        $order = $this->getOrder();

        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }
        if ($order instanceof Mage_Sales_Model_Order && $order->getId()) {
            $order->addStatusToHistory(
                    Mage_Sales_Model_Order::STATE_CANCELED, Mage::helper('alipayinternational')->__('Customer returned from Alipay.') . $errorMsg
            );
            $order->cancel()->save();
        }

        $this->loadLayout();
        $this->renderLayout();
        Mage::getSingleton('checkout/session')->unsLastRealOrderId();
    }

    /**
     * Alipay signature (md5)
     *
     * @param string $prestr
     * @return string
     *
     * @FIXME: need to move this function into data helper
     */
    public function sign($prestr) {
        $mysign = md5($prestr);
        return $mysign;
    }

    /**
     * remove the parameter that doesn't need to be signed
     *
     * @param unknown_type $parameter
     * @return array
     *
     * @FIXME: need to move this function into data helper
     */
    public function para_filter($parameter) {
        $para = array();
        while (list ($key, $val) = each($parameter)) {
            if ($key == "sign" || $key == "sign_type" || $val == "")
                continue;
            else
                $para[$key] = $parameter[$key];
        }
        return $para;
    }

    /**
     * Sort the array by key, plus reset the array pointer
     *
     * @param array $array
     * @return array
     *
     * @FIXME: need to move this function into data helper
     */
    public function arg_sort($array) {
        ksort($array);
        reset($array);
        return $array;
    }

    /**
     * continue pay
     */
    function paynowAction() {
        $order_id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($order_id);
        header('Content-type: text/html; charset=utf-8');
        $standard = Mage::getModel('alipayinternational/payment');
        $html = '';
        $form = new Varien_Data_Form();
        $form->setAction($standard->getAlipayUrl())
                ->setId('alipay_payment_checkout')
                ->setName('alipay_payment_checkout')
                ->setMethod('GET')
                ->setUseContainer(true);
        foreach ($standard->setOrder($order)->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }
        $formHTML = $form->toHtml();

        $html .= $formHTML;
        $html .= $this->__('You will be redirected to Alipay in a few seconds.');
        $html .= '<script type="text/javascript">document.getElementById("alipay_payment_checkout").submit();</script>';
        echo $html;
    }

}
