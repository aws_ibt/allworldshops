<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Block_Redirect extends Mage_Core_Block_Abstract
{
	/**
	 * Render the html output for redirecting user to Alipay payment page
	 *
	 * @return string
	 */
	protected function _toHtml()
	{
		header('Content-type: text/html; charset=utf-8');
		$standard = Mage::getModel('alipayinternational/payment');
        $form = new Varien_Data_Form();
        $form->setAction($standard->getAlipayUrl())
            ->setId('alipay_payment_checkout')
            ->setName('alipay_payment_checkout')
            ->setMethod('GET')
            ->setUseContainer(true);
        foreach ($standard->setOrder($this->getOrder())->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }
        
        $formHTML = $form->toHtml();
        $html = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>';
        $html.= $this->__('You will be redirected to Alipay in a few seconds.');
        $html.= $formHTML;
        $html.= '<script type="text/javascript">document.getElementById("alipay_payment_checkout").submit();</script>';
        $html.= '</body></html>';
        return $html;
    }
}