<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Block_Info extends Mage_Payment_Block_Info
{

	protected function _construct() {
		parent::_construct();
		//$this->setTemplate('payment/info/purchaseorder.phtml');
	}
	
	protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }
        $info = $this->getInfo();
        $transport = new Varien_Object();
        $transport = parent::_prepareSpecificInformation($transport);
        
        $bank = $info->getAlipayDefaultbank();
        if($bank){
	        $transport->addData(array(
	            Mage::helper('alipayinternational')->__('Alipay') => Mage::getModel('alipayinternational/source_bank')->getLabel($bank)
	        ));
	    }
        return $transport;
    }
}