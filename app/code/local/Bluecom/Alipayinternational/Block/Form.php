<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $mark = Mage::getConfig()->getBlockClassName('core/template');
        $mark = new $mark;
        $mark->setTemplate('alipayinternational/mark.phtml');
        $this->setTemplate('alipayinternational/form.phtml')
                ->setMethodTitle('') // Output alipay mark, omit title
                ->setMethodLabelAfterHtml($mark->toHtml());
        parent::_construct();
    }
    
    public function getServiceType(){
        $alipay = Mage::getModel('alipayinternational/payment');
        return $alipay->getConfigData('service_type');
    }

}