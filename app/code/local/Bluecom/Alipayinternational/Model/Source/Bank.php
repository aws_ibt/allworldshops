<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Model_Source_Bank
{
	/**
	 * Source Data for configuration
	 *
	 * @return array
	 */
    public function toOptionArray()
    {
        return array(
            array('value' => 'cybs-visa', 'label' => Mage::helper('alipayinternational')->__('International card-visa') , 'bp'=>'0px 0px'),	
            array('value' => 'cybs-master', 'label' => Mage::helper('alipayinternational')->__('International card-mastercard') , 'bp'=>'0px -30px'),
           
            
        );
    }
    
    /**
     * Get the Blank name from code value
     * 
     * @param string $code
     * @return string|boolean
     */
    public function getLabel($code) {
        
        foreach($this->toOptionArray() as $bank) {
            if($code == $bank['value']) {
                return $bank['label'];
            }
        }
        return false;
    }
}