<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Model_Source_Servicetype
{
	/**
	 * Source Data for configuration
	 *
	 * @return array
	 */
    public function toOptionArray()
    {
        return array(
            
            array('value' => 'create_forex_trade', 'label' => Mage::helper('alipayinternational')->__('Alipay General International Payment'))
        );
    }
}



