<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Model_Source_Transport
{
	/**
	 * Source Data for configuration
	 * 
	 * @return array
	 */
    public function toOptionArray()
    {
        return array(
            array('value' => 'https', 'label' => Mage::helper('alipayinternational')->__('https')),
            array('value' => 'http', 'label' => Mage::helper('alipayinternational')->__('http')),
        );
    }
}