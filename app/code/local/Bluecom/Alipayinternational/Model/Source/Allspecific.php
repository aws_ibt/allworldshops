<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 *
 */


class Bluecom_Alipayinternational_Model_Source_Allspecific
{
    public function toOptionArray()
    {
        return array(
            array('value'=>0, 'label'=>Mage::helper('adminhtml')->__('All Allowed Banks')),
            array('value'=>1, 'label'=>Mage::helper('adminhtml')->__('Specific Banks')),
        );
    }
}
