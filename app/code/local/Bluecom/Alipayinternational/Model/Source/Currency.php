<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Model_Source_Currency
{
	/**
	 * Source Data for configuration
	 * 
	 * @return array
	 */
    public function toOptionArray()
    {
        return array(
            array('value' => 'order', 'label' => Mage::helper('alipayinternational')->__('Order Currency')),
            array('value' => 'base', 'label' => Mage::helper('alipayinternational')->__('Website Base Currency')),
        );
    }
}