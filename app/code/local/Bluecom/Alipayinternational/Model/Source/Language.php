<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Model_Source_Language
{
	/**
	 * Source Data for configuration
	 *
	 * @return array
	 */
    public function toOptionArray()
    {
        return array(
            array('value' => 'EN', 'label' => Mage::helper('alipayinternational')->__('English')),
            array('value' => 'FR', 'label' => Mage::helper('alipayinternational')->__('French')),
            array('value' => 'DE', 'label' => Mage::helper('alipayinternational')->__('German')),
            array('value' => 'IT', 'label' => Mage::helper('alipayinternational')->__('Italian')),
            array('value' => 'ES', 'label' => Mage::helper('alipayinternational')->__('Spain')),
            array('value' => 'NL', 'label' => Mage::helper('alipayinternational')->__('Dutch')),
        );
    }
}



