<?php

/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */
class Bluecom_Alipayinternational_Model_Verify extends Mage_Core_Model_Abstract {

    public function __construct() {
        $this->payModel = Mage::getModel('alipayinternational/payment');
        $this->transport = $this->payModel->getConfigData('transport');
        $this->partner = $this->payModel->getConfigData('partner_id');
        $this->security_code = $this->payModel->getConfigData('security_code');
        $this->sign_type = $this->payModel->getSignType();
        $this->mysign = "";
        $this->_input_charset = $this->payModel->getInputCharSet();
        if ($this->transport == "https") {
            $this->gateway = "https://mapi.alipay.com/gateway.do?";
        } else {
            $this->gateway = "https://mapi.alipay.com/gateway.do?";
        }
    }
    public function getRequestData(){
        
        $postData = Mage::app()->getRequest()->getPost();
        return $postData;
    }

    /**
     * 
     * @return boolean
     */
    public function notify_verify() {   //verify the notify_url
        $postData = $this->getRequestData();
        if ($this->transport == "https") {
            $veryfy_url = $this->gateway . "service=notify_verify" . "&partner=" . $this->partner . "&notify_id=" . $postData["notify_id"];
        } else {
            $veryfy_url = $this->gateway . "notify_id=" . $postData["notify_id"] . "&partner=" . $this->partner;
        }
        $veryfy_result = $this->get_verify($veryfy_url);
        $post = $this->para_filter($_POST);
        $sort_post = $this->arg_sort($post);
        $arg = '';
        while (list ($key, $val) = each($sort_post)) {
            $arg.=$key . "=" . $val . "&";
        }
        $prestr = substr($arg, 0, count($arg) - 2);  //remove the last &
        $this->mysign = $this->sign($prestr . $this->security_code);
        Mage::log("sign_log=" . $postData["sign"] . "&" . $this->mysign . "&" . $this->charset_decode(implode(",", $postData), $this->_input_charset), null, 'alipay_notify_data.log');
        if (preg_match("/true$/", $veryfy_result) && $this->mysign == $postData["sign"]) {
            return true;
        } else
            return false;
    }

    /**
     * 
     * @param string $url
     * @param int $time_out
     * @return array
     */
    function get_verify($url, $time_out = "60") {
        $urlarr = parse_url($url);
        $errno = "";
        $errstr = "";
        $transports = "";
        if (isset($urlarr["scheme"]) && $urlarr["scheme"] == "https") {
            $transports = "ssl://";
            $urlarr["port"] = "443";
        } else {
            $transports = "tcp://";
            $urlarr["port"] = "80";
        }
        $fp = @fsockopen($transports . $urlarr['host'], $urlarr['port'], $errno, $errstr, $time_out);
        if (!$fp) {
            die("ERROR: $errno - $errstr<br />\n");
        } else {
            fputs($fp, "POST " . $urlarr["path"] . " HTTP/1.1\r\n");
            fputs($fp, "Host: " . $urlarr["host"] . "\r\n");
            fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
            fputs($fp, "Content-length: " . strlen($urlarr["query"]) . "\r\n");
            fputs($fp, "Connection: close\r\n\r\n");
            fputs($fp, $urlarr["query"] . "\r\n\r\n");
            while (!feof($fp)) {
                $info[] = @fgets($fp, 1024);
            }
            fclose($fp);
            $info = implode(",", $info);
            $arg = '';
            
            while (list ($key, $val) = each($_POST)) {
                $arg.=$key . "=" . $val . "&";
            }
            Mage::log("log=" . $url . $this->charset_decode($info, $this->_input_charset), null, 'alipay_notify_data.log');
            Mage::log("log=" . $this->charset_decode($arg, $this->_input_charset), null, 'alipay_notify_data.log');

            return $info;
        }
    }

    /**
     *  multipule charset decode
     * 
     * @param string $input
     * @param string $_input_charset
     * @param string $_output_charset
     * @return string
     */
    public function charset_decode($input, $_input_charset, $_output_charset = "utf-8") {
        $output = "";
        if (!isset($_input_charset))
            $_input_charset = $this->_input_charset;
        if ($_input_charset == $_output_charset || $input == null) {
            $output = $input;
        } elseif (function_exists("mb_convert_encoding")) {
            $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
        } elseif (function_exists("iconv")) {
            $output = iconv($_input_charset, $_output_charset, $input);
        } else {
            $output = $input;
        }
        return $output;
    }

    function arg_sort($array) {
        ksort($array);
        reset($array);
        return $array;
    }

    function sign($prestr) {
        $sign = '';
        if ($this->sign_type == 'MD5') {
            $sign = md5($prestr);
        } elseif ($this->sign_type == 'DSA') {
            //DSA is developing, not support now.
            die(Mage::helper('alipayinternational')->__('DSA signature method to be follow-up development, please use the MD5 signature scheme'));
        } else {
            die(Mage::helper('alipayinternational')->__('Alipay does not support the type of signature %s way', $this->sign_type));
        }
        return $sign;
    }
    /**
     * remove empty kay, values and sign
     * 
     * @param string $parameter
     * @return string
     */
    function para_filter($parameter) { 
        $para = array();
        while (list ($key, $val) = each($parameter)) {
            if ($key == "sign" || $key == "sign_type" || $val == "")
                continue;
            else
                $para[$key] = $parameter[$key];
        }
        return $para;
    }

}
