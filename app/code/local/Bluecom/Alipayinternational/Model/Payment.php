<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2012-2013 Bluecom (http://www.bluecomgroup.com)
 * 
 */

class Bluecom_Alipayinternational_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
	protected $_code  = 'alipayinternational';
	protected $_formBlockType = 'alipayinternational/form';
	protected $_infoBlockType  = 'alipayinternational/info';

	const RETURN_CODE_ACCEPTED      = 'Success';
	const RETURN_CODE_TEST_ACCEPTED = 'Success';
	const RETURN_CODE_ERROR         = 'Fail';

	protected $_isGateway               = false;
	protected $_canAuthorize            = true;
	protected $_canCapture              = true;
	protected $_canCapturePartial       = false;
	protected $_canRefund               = false;
	protected $_canVoid                 = false;
	protected $_canUseInternal          = true;
	protected $_canUseCheckout          = true;
	protected $_canUseForMultishipping  = false;

	protected $_order = null;
	/**
	 *  Returns Target URL
	 *
	 *  @return	  string Target URL
	 */
	public function getAlipayUrl()
	{
		$url = $this->getConfigData('transport').'://'.$this->getConfigData('gateway');
		return $url;
	}
	
	/**
	 *  Return back URL
	 *
	 *  @return	  string URL
	 */
	protected function getReturnURL()
	{
		return Mage::getUrl('alipayinternational/payment/success', array('_secure' => true));
	}
	
	/**
	 *  Return URL for Alipay success response
	 *
	 *  @return	  string URL
	 */
	protected function getSuccessURL()
	{
		return Mage::getUrl('alipayinternational/payment/success', array('_secure' => true));
	}
	
	/**
	 *  Return URL for Alipay failure response
	 *
	 *  @return	  string URL
	 */
	protected function getErrorURL()
	{
		return Mage::getUrl('alipayinternational/payment/error', array('_secure' => true));
	}
	
	/**
	 *  Return URL for Alipay notify response
	 *
	 *  @return	  string URL
	 */
	protected function getNotifyURL()
	{
		return Mage::getUrl('alipayinternational/payment/notify/', array('_secure' => true));
	}
        /**
         * 
         * @return string sign_type
         */
        public function getSignType(){
            return "MD5";
        }
        /**
         * 
         * @return string _input_charset
         */
        public function getInputCharSet(){
            return 'utf-8';
        }
	
	/**
	 * Capture payment
	 *
	 * @param   Varien_Object $orderPayment
	 * @return  Mage_Payment_Model_Abstract
	 */
	public function capture(Varien_Object $payment, $amount)
	{
		$payment->setStatus(self::STATUS_APPROVED)->setLastTransId($this->getTransactionId());
		return $this;
	}
	
	/**
	 *  Form block description
	 *
	 *  @return	 object
	 */
	public function createFormBlock($name)
	{
		$block = $this->getLayout()->createBlock('alipayinternational/form_payment', $name);
		$block->setMethod($this->_code);
		$block->setPayment($this->getPayment());
		return $block;
	}
	
	/**
	 *  Return Order Place Redirect URL
	 *
	 *  @return	  string Order Redirect URL
	 */
	public function getOrderPlaceRedirectUrl()
	{
		return Mage::getUrl('alipayinternational/payment/redirect');
	}
	
	/**
	 *  Return Standard Checkout Form Fields for request to Alipay
	 *
	 *  @return	  array Array of hidden form fields
	 */
	public function getStandardCheckoutFormFields()
	{
	
		$order = $this->getOrder();
		if (!($order instanceof Mage_Sales_Model_Order)) {
			Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
		}
                if($this->getConfigData('service_type') =='order'){
                    $currency = $order->getOrderCurrencyCode();
                    $amount = sprintf('%.2f', $order->getGrandTotal());
                }else{
                    $currency = $order->getBaseCurrencyCode();
                    $amount = sprintf('%.2f', $order->getBaseGrandTotal());
                }
		/*if ($order->getBaseCurrencyCode() != 'CNY') {
			if ($convert = Mage::getSingleton('directory/currency')->load($order->getOrderCurrencyCode())) {
				if (!$convert->getRate('CNY')) die('Currency rate conversion failed, please contact administrator to setup currency rate first.');
				$amount = $convert->convert($amount, "CNY");
				$amount = sprintf('%.2f', $amount);
			}
		}*/
                if($this->getConfigData('service_type') == 'alipay.trade.direct.forcard.pay'){
			$parameter = array(
					'service'           => $this->getConfigData('service_type'),
					'partner'           => $this->getConfigData('partner_id'),
                                        '_input_charset'    => 'utf-8',
                                        'notify_url'        => $this->getNotifyURL(),
					'return_url'        => $this->getReturnURL(),
					'out_trade_no'      => $this->orderNoToOutTradeNo($order->getRealOrderId()), // order ID
					'subject'           => $order->getRealOrderId(),
                                        'default_bank'      => $order->getDefaultBank(),
                                        //'seller_id'         => $this->getConfigData('partner_id'),
                                        'seller_logon_id'   => $this->getConfigData('seller_email'),
                                        'total_fee'         => $amount ,
                                        'body'              => $order->getRealOrderId(),
					'show_url'          => Mage::getUrl(),
                                        'it_b_pay'          => $this->getConfigData('transaction_expire_time'),
                                        'currency'          => $currency
			);
                }else if($this->getConfigData('service_type') == "create_forex_trade"){
                    $parameter = array(
					'service'           => $this->getConfigData('service_type'),
					'partner'           => $this->getConfigData('partner_id'),
                                        '_input_charset'    => 'utf-8',
                                        'notify_url'        => $this->getNotifyURL(),
					'return_url'        => $this->getReturnURL(),
					'out_trade_no'      => $this->orderNoToOutTradeNo($order->getRealOrderId()), // order ID
					'subject'           => $order->getRealOrderId(),
                                        //'seller_id'         => $this->getConfigData('partner_id'),
                                        'seller_logon_id'   => $this->getConfigData('seller_email'),
                                        'total_fee'         => $amount ,
                                        'body'              => $order->getRealOrderId(),
					'show_url'          => Mage::getUrl(),
                                        'timeout_rule'      => $this->getConfigData('transaction_expire_time'),
                                        'currency'          => $currency
			);
                }
		$parameter = $this->para_filter($parameter);
		$security_code = $this->getConfigData('security_code');
		$sign_type = $this->getSignType();
	
		$sort_array = array();
		$arg = "";
		$sort_array = $this->arg_sort($parameter); //$parameter
		while (list ($key, $val) = each ($sort_array)) {
			$arg.=$key."=".$val."&";
		}
		$prestr = substr($arg,0,count($arg)-2);
		$mysign = $this->sign($prestr.$security_code);
		$fields = array();
		$sort_array = array();
		$arg = "";
		$sort_array = $this->arg_sort($parameter); //$parameter
		while (list ($key, $val) = each ($sort_array)) {
			$fields[$key] = $val;
		}
		$fields['sign'] = $mysign;
		$fields['sign_type'] = $sign_type;
		return $fields;
	}
	
	/**
	 * Alipay signature (md5)
	 * 
	 * @param string $prestr
	 * @return string
	 * 
	 * @FIXME: need to move this function into data helper
	 */
	public function sign($prestr) {
		$mysign = md5($prestr);
		return $mysign;
	}
	
	/**
	 * remove the parameter that doesn't need to be signed
	 * 
	 * @param unknown_type $parameter
	 * @return array
	 * 
	 * @FIXME: need to move this function into data helper
	 */
	public function para_filter($parameter) {
		$para = array();
		while (list ($key, $val) = each ($parameter)) {
			if($key == "sign" || $key == "sign_type" || $val == ""){
				continue;
			}else{
				$para[$key] = $parameter[$key];
			}
		}
		return $para;
	}
	
	/**
	 * Sort the array by key, plus reset the array pointer
	 * 
	 * @param array $array
	 * @return array
	 * 
	 * @FIXME: need to move this function into data helper
	 */
	public function arg_sort($array) {
		ksort($array);
		reset($array);
		return $array;
	}

	/**
	 *  Save invoice for order
	 *
	 *  @param    Mage_Sales_Model_Order $order
	 *  @return	  boolean Can save invoice or not
	 */
	 function saveInvoice(Mage_Sales_Model_Order $order)
	 {
             
		if ($order->canInvoice()){
			$convertor = Mage::getModel('sales/convert_order');
			$invoice = $convertor->toInvoice($order);
			foreach ($order->getAllItems() as $orderItem){
				if (!$orderItem->getQtyToInvoice()){
					continue ;
				}
				$item = $convertor->itemToInvoiceItem($orderItem);
				$item->setQty($orderItem->getQtyToInvoice());
				$invoice->addItem($item);
			}
			$invoice->collectTotals();
			$invoice->register()->capture();
			Mage::getModel('core/resource_transaction')
			->addObject($invoice)
			->addObject($invoice->getOrder())
			->save();
                        
			return true;
		}
		return false;
	}
	
	/**
	 *  Set corresponding payment gateway data
	 *
	 *  @param    array $data
	 *  @return	  Mage_Payment_Model_Info
	 */
	public function assignData($data)
	{
		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		$info = $this->getInfoInstance();
		$info->setAlipayDefaultbank($data->getAlipayDefaultbank());
		return $this;
	}
	
	public function orderNoToOutTradeNo($orderNo){
	    $alipayinternational_order_prefix = $this->getConfigData('alipayinternational_order_prefix');
	    return $alipayinternational_order_prefix.$orderNo;
	}
	public function outTradeNoToOrderNo($outTradeNo){
	    $alipay_order_prefix = $this->getConfigData('alipayinternational_order_prefix');
	    return substr($outTradeNo, strlen($alipay_order_prefix));
	}
	
}
?>