<?php
/**
 * Bluecom
 *
 * @category    Bluecom
 * @package     Bluecom_Alipayinternational
 * @copyright   Copyright (c) 2015-2016 Bluecom (http://www.bluecomgroup.com)
 * 
 */

/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$setup = new Mage_Sales_Model_Mysql4_Setup('sales_setup');
$setup->addAttribute('quote_payment', 'alipay_defaultbank', array());
$setup->addAttribute('order_payment', 'alipay_defaultbank', array());
$this->endSetup();