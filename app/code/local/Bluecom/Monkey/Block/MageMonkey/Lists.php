<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Admin
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Bluecom_Monkey_Block_MageMonkey_Lists
 */
class Bluecom_Monkey_Block_MageMonkey_Lists extends Ebizmarts_MageMonkey_Block_Customer_Account_Lists
{

    /**
     * Return HTML code for list <label> with checkbox, checked if subscribed, otherwise not
     *
     * @param array $list List data from MC
     *
     * @return string HTML code
     */
    public function listLabel($list)
    {
        $myLists = $this->getSubscribedLists();

        //if is on database it gets checked
        if (isset($list['id'])) {
            $alreadyOnList = Mage::getSingleton('monkey/asyncsubscribers')->getCollection()
                ->addFieldToFilter('lists', $list['id'])
                ->addFieldToFilter('email', $this->_getEmail())
                ->addFieldToFilter('processed', 0);
        }

        if (count($alreadyOnList) > 0) {
            $myLists[] = $list['id'];
        }

        if (isset($list['id'])) {
            $checkbox = new Varien_Data_Form_Element_Checkbox;
            $checkbox->setForm($this->getForm());
            $checkbox->setHtmlId('list-' . $list['id']);
            $checkbox->setChecked((bool)(is_array($myLists) && in_array($list['id'], $myLists)));
            $checkbox->setTitle(($checkbox->getChecked() ? $this->__('Click to unsubscribe from this list.') : $this->__('Click to subscribe to this list.')));
            $checkbox->setLabel($list['name']);

            $hname = $this->_htmlGroupName($list);
            $checkbox->setName($hname . '[subscribed]');

            $checkbox->setValue($list['id']);
            $checkbox->setClass('monkey-list-subscriber');


            return $checkbox->getElementHtml(). $checkbox->getLabelHtml();
        } else {
            return '';
        }
    }
}
