<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Admin
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Class Bluecom_ShippingMethod_Model_Mysql4_Matrixrate_Carrier_Matrixrate_Collection
 *
 * @category Mage
 * @package  Mage_Adminhtml
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_ShippingMethod_Model_Mysql4_Matrixrate_Carrier_Matrixrate_Collection extends Webshopapps_Matrixrate_Model_Mysql4_Carrier_Matrixrate_Collection
{

    /**
     * Bluecom_ShippingMethod_Model_Mysql4_Matrixrate_Carrier_Matrixrate_Collection constructor.
     */
    public function __construct()
    {
        $this->setConnection(Mage::getSingleton('core/resource')->getConnection('shipping_read'));
        //parent::__construct(Mage::getSingleton('core/resource')->getConnection('shipping_read'));
        $this->_shipTable = Mage::getSingleton('core/resource')->getTableName('matrixrate_shipping/matrixrate');
        $this->_countryTable = Mage::getSingleton('core/resource')->getTableName('directory/country');
        $this->_regionTable = Mage::getSingleton('core/resource')->getTableName('directory/country_region');
        $this->_select->from(array("s" => $this->_shipTable), array('s.*', 's.dest_country_id AS dest_country'))
            //->joinLeft(array("c" => $this->_countryTable), 'c.country_id = s.dest_country_id', 'iso3_code AS dest_country')
            ->joinLeft(array("r" => $this->_regionTable), 'r.region_id = s.dest_region_id', 'code AS dest_region')
            ->order(array("dest_country", "dest_region", "dest_zip"));
        $this->_setIdFieldName('pk');
        return $this;
    }
}