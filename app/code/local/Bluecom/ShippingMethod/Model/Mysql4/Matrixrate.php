<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Admin
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */

/**
 * Class Bluecom_ShippingMethod_Model_Mysql4_Matrixrate
 *
 * @category Mage
 * @package  Mage_Adminhtml
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_ShippingMethod_Model_Mysql4_Matrixrate
extends Unirgy_DropshipMrate_Model_Mysql4_Matrixrate
{
    /*
     * valid zone in AWS system
     * This list get from customer excel file
     * need to compare with Magento table: directory_country
     */

    protected $_countryZone = array(
        'A' => array('AU'),
        'B' => array('AS','CC','CK','TP','PF','TF','KI','NR','NC','NU','NF','WS','TK','TO','TV','VU'),
        'C' => array('BN','KH','IN','ID','MO','MY','MV','MM','PH','SG','LK','TH','VN'),
        'D' => array('CA','US'),
        'E' => array('AT','BH','BD','BE','DK','FJ','FI','FR','FX','DE','GR','HU','IE','IT','LA','LU','NL','AN','PG','ES','SE','CH','GB'),
        'F' => array('BY','BT','BR','BG','CY','EG','IS','IR','IL','JO','KW','NP','NO','OM','PK','PL','PT','QA','RO','RU','SA','SI','SB','TR','UA','AE','YE'),
        'G' => array('AF','AL','DZ','AD','AO','AI','AQ','AG','AR','AM','AW','AZ','BS','BB','BZ','BJ','BM','BO','BA','BW','BV','IO','BF','BI','CM','CV','KY','CF','TD','CL','CX','CO','KM','CG','CR','CI','HR','CU','CZ','DJ','DM','DO','EC','SV','GQ','ER','EE','ET','FK','FO','GF','GA','GM','GE','GH','GI','GL','GD','GP','GU','GT','GN','GW','GY','HT','HM','HN','IQ','JM','KZ','KE','KP','KG','LV','LB','LS','LR','LY','LI','LT','MK','MG','MW','ML','MT','MH','MQ','MR','MU','YT','MX','FM','MD','MC','MN','MS','MA','MZ','NA','NI','NE','NG','MP','PW','PA','PY','PE','PN','PR','RE','RW','KN','LC','VC','SM','ST','SN','SC','SL','SK','SO','ZA','GS','SH','PM','SD','SR','SJ','SZ','SY','TJ','TZ','TG','TT','TN','TM','TC','UG','UM','UY','UZ','VA','VE','VG','VI','WF','EH','YU','ZR','ZM','ZW'),
        'H' => array('KR'),
        'I' => array('JP','HK','TW','CN'),
        'L' => array('NZ')
    );

    /**
     * MapCountryZone
     *
     * @param string $country country
     *
     * @return int|string
     */
    protected function _mapCountryZone($country)
    {
        foreach ($this->_countryZone as $zone => $countries) {
            if (in_array($country, $countries)) {
                return $zone;
            }
        }
    }

    /**
     * GetNewRate
     *
     * @param Mage_Shipping_Model_Rate_Request $request     request
     * @param int                              $zipRangeSet zipRangeSet
     *
     * @return array
     */
    public function getNewRate(Mage_Shipping_Model_Rate_Request $request, $zipRangeSet=0)
    {
        $read = $this->_getReadAdapter();

        $table = $this->getMainTable();

        $zipSearchString = ')';

        for ($j=0;$j<10;$j++) {
            $select = $read->select()->from($table);

            $select->where("udropship_vendor IS NULL OR udropship_vendor in (0,?)", $request->getVendorId());

            if(array_key_exists($request->getDestCountryId(), $this->_countryZone)){
                $request->setDestCountryId($request->getDestCountryId());
            }
            else{
                $request->setDestCountryId($this->_mapCountryZone($request->getDestCountryId()));
            }

            switch ($j) {
            case 0:
                $select->where(
                    $read->quoteInto(" dest_country_id=? ", $request->getDestCountryId())
                    // $read->quoteInto(" AND dest_region_id=? ", $request->getDestRegionId()).
                    // $read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  ", $request->getDestCity()).
                    // $read->quoteInto(" AND ? LIKE dest_zip ) ", $postcode)
                    // $zipSearchString
                );
                break;
            case 1:
                $select->where(
                    $read->quoteInto(" (dest_country_id=? ", $request->getDestCountryId()).
                    $read->quoteInto(" AND dest_region_id=?  AND dest_city=''", $request->getDestRegionId()).
                    $zipSearchString
                );
                break;
            case 2:
                $select->where(
                    $read->quoteInto(" (dest_country_id=? ", $request->getDestCountryId()).
                    $read->quoteInto(" AND dest_region_id=? ", $request->getDestRegionId()).
                    $read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_zip='')", $request->getDestCity())
                );
                break;
            case 3:
                $select->where(
                    $read->quoteInto("  (dest_country_id=? ", $request->getDestCountryId()).
                    $read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_region_id='0'", $request->getDestCity()).
                    $zipSearchString
                );
                break;
            case 4:
                $select->where(
                    $read->quoteInto("  (dest_country_id=? ", $request->getDestCountryId()).
                    $read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_region_id='0' AND dest_zip='') ", $request->getDestCity())
                );
                break;
            case 5:
                $select->where(
                    $read->quoteInto("  (dest_country_id=? AND dest_region_id='0' AND dest_city='' ", $request->getDestCountryId()).
                    // $read->quoteInto("  AND ? LIKE dest_zip ) ", $postcode)
                    $zipSearchString
                );
                break;
            case 6:
                $select->where(
                    $read->quoteInto("  (dest_country_id=? ", $request->getDestCountryId()).
                    $read->quoteInto(" AND dest_region_id=? AND dest_city='' AND dest_zip='') ", $request->getDestRegionId())
                );
                break;

            case 7:
                $select->where(
                    $read->quoteInto("  (dest_country_id=? AND dest_region_id='0' AND dest_city='' AND dest_zip='') ", $request->getDestCountryId())
                );
                break;

            case 8:
                $select->where(
                    "  (dest_country_id='0' AND dest_region_id='0'".
                    $zipSearchString
                );
                break;

            case 9:
                $select->where(
                    "  (dest_country_id='0' AND dest_region_id='0' AND dest_zip='')"
                );
                break;
            }

            $__conditionName = $request->getMRConditionName()
                ? $request->getMRConditionName() : $request->getConditionName();

            if (is_array($__conditionName)) {
                $i = 0;
                foreach ($__conditionName as $conditionName) {
                    if ($i == 0) {
                        $select->where('condition_name=?', $conditionName);
                    } else {
                        $select->orWhere('condition_name=?', $conditionName);
                    }
                    $select->where('condition_from_value<=?', $request->getData($conditionName));


                    $i++;
                }
            } else {
                $select->where('condition_name=?', $__conditionName);
                $select->where('condition_from_value<=?', $request->getData($__conditionName));
                // custom condition for AWS
                $select->where('condition_to_value>?', $request->getData($__conditionName));
            }

            $select->where('website_id=?', $request->getWebsiteId());

            $select->order('dest_country_id DESC');
            $select->order('dest_region_id DESC');
            $select->order('dest_zip DESC');
            $select->order('condition_from_value DESC');
            /*
            pdo has an issue. we cannot use bind
            */
            //print($select->__toString());echo '<br>';
            $newdata=array();
            $row = $read->fetchAll($select);
            if (!empty($row)) {
                // have found a result or found nothing and at end of list!
                foreach ($row as $data) {
                    $newdata[]=$data;
                }
                break;
            }
        }
        return $newdata;
    }

    /**
     *UploadAndImport
     *
     * @param Varien_Object $object object
     *
     * @return bool
     *
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function uploadAndImport(Varien_Object $object)
    {
        $zones = array_keys($this->_countryZone);
        $csvFile = $this->_getUploadedCsvFile();
        $io = new Varien_Io_File();

        if (!empty($csvFile)) {

            // UNIRGY-BEGIN: retrieve vendor names for convenient import
            $vendors = array();
            $collection = Mage::getModel('udropship/vendor')->getCollection();
            foreach ($collection as $v) {
                $vendors[$v->getVendorName()] = $v->getVendorId();
            }
            // UNIRGY-END

            $csv = trim($io->read($csvFile));
            $table = $this->getMainTable();

            $websiteId = $object->getScopeId();
            $websiteModel = Mage::app()->getWebsite($websiteId);
            /*
            getting condition name from post instead of the following commented logic
            */

            $conditionName = $this->_getPostedConditionName();

            $conditionFullName = Mage::getModel('matrixrate_shipping/carrier_matrixrate')->getCode('condition_name_short', $conditionName);
            if (!empty($csv)) {
                $exceptions = array();
                $csvLines = explode("\n", $csv);
                $csvLine = array_shift($csvLines);
                $csvLine = $this->_getCsvValues($csvLine);
                if (count($csvLine) < 7) {
                    $exceptions[0] = Mage::helper('shipping')->__('Invalid Matrix Rates File Format');
                }

                $countryCodes = array();
                $regionCodes = array();
                foreach ($csvLines as $k=>$csvLine) {
                    $csvLine = $this->_getCsvValues($csvLine);
                    if (count($csvLine) > 0 && count($csvLine) < 7) {
                        $exceptions[0] = Mage::helper('shipping')->__('Invalid Matrix Rates File Format');
                    } else {
                        // UNIRGY: if the line is empty, will give an error here
                        $countryCodes[] = $csvLine[0];
                        $regionCodes[] = $csvLine[1];
                    }
                }

                if (empty($exceptions)) {
                    $data = array();
                    $countryCodesToIds = array();
                    $regionCodesToIds = array();
                    $countryCodesIso2 = array();

                    $countryCollection = Mage::getResourceModel('directory/country_collection')->addCountryCodeFilter($countryCodes)->load();
                    foreach ($countryCollection->getItems() as $country) {
                        $countryCodesToIds[$country->getData('iso3_code')] = $country->getData('country_id');
                        $countryCodesToIds[$country->getData('iso2_code')] = $country->getData('country_id');
                        $countryCodesIso2[] = $country->getData('iso2_code');
                    }

                    $regionCollection = Mage::getResourceModel('directory/region_collection')
                        ->addRegionCodeFilter($regionCodes)
                        ->addCountryFilter($countryCodesIso2)
                        ->load();

                    foreach ($regionCollection->getItems() as $region) {
                        $regionCodesToIds[$countryCodesToIds[$region->getData('country_id')]][$region->getData('code')] = $region->getData('region_id');
                    }

                    foreach ($csvLines as $k=>$csvLine) {
                        $csvLine = $this->_getCsvValues($csvLine);

                        if (empty($countryCodesToIds) || !array_key_exists($csvLine[0], $countryCodesToIds)) {
                            $countryId = '0';
                            // handle shipping zone
                            if ($csvLine[0] != '*' && $csvLine[0] != '' && !in_array($csvLine[0], $zones)) {
                                $exceptions[] = Mage::helper('shipping')->__('Invalid Country "%s" in the Row #%s', $csvLine[0], ($k+1));
                            }
                        } else {
                            $countryId = $countryCodesToIds[$csvLine[0]];
                        }

                        // handle shipping zone
                        if (in_array($csvLine[0], $zones)) {
                            $countryId = $csvLine[0];
                        }

                        if (!isset($countryCodesToIds[$csvLine[0]])
                            || !isset($regionCodesToIds[$countryCodesToIds[$csvLine[0]]])
                            || !array_key_exists($csvLine[1], $regionCodesToIds[$countryCodesToIds[$csvLine[0]]])
                        ) {
                            $regionId = '0';
                            if ($csvLine[1] != '*' && $csvLine[1] != '') {
                                $exceptions[] = Mage::helper('shipping')->__('Invalid Region/State "%s" in the Row #%s', $csvLine[1], ($k+1));
                            }
                        } else {
                            $regionId = $regionCodesToIds[$countryCodesToIds[$csvLine[0]]][$csvLine[1]];
                        }

                        // UNIRGY-CHANGE: added 10
                        if (count($csvLine)>=10) {
                            // we are searching for postcodes in ranges & including cities
                            if ($csvLine[2] == '*' || $csvLine[2] == '') {
                                $city = '';
                            } else {
                                $city = $csvLine[2];
                            }


                            if ($csvLine[3] == '*' || $csvLine[3] == '') {
                                $zip = '';
                            } else {
                                $zip = $csvLine[3];
                            }


                            if ($csvLine[4] == '*' || $csvLine[4] == '') {
                                $zip_to = '';
                            } else {
                                $zip_to = $csvLine[4];
                            }


                            if (!$this->_isPositiveDecimalNumber($csvLine[5]) || $csvLine[5] == '*' || $csvLine[5] == '') {
                                $exceptions[] = Mage::helper('shipping')->__('Invalid %s From "%s" in the Row #%s', $conditionFullName, $csvLine[5], ($k+1));
                            } else {
                                $csvLine[5] = (float)$csvLine[5];
                            }

                            if (!$this->_isPositiveDecimalNumber($csvLine[6])) {
                                $exceptions[] = Mage::helper('shipping')->__('Invalid %s To "%s" in the Row #%s', $conditionFullName, $csvLine[6], ($k+1));
                            } else {
                                $csvLine[6] = (float)$csvLine[6];
                            }

                            // UNIRGY-BEGIN
                            $d = array('website_id'=>$websiteId, 'dest_country_id'=>$countryId, 'dest_region_id'=>$regionId, 'dest_city'=>$city, 'dest_zip'=>$zip, 'dest_zip_to'=>$zip_to, 'condition_name'=>$conditionName, 'condition_from_value'=>$csvLine[5],'condition_to_value'=>$csvLine[6], 'price'=>$csvLine[7], 'delivery_type'=>$csvLine[8]);
                            if (!empty($csvLine[9])) {
                                if (is_numeric($csvLine[9])) {
                                    $d['udropship_vendor'] = $csvLine[9];
                                } else {
                                    if (empty($vendors[$csvLine[9]])) {
                                        $exceptions[] = Mage::helper('udropship')->__('Invalid Vendor on line %s (%s)', $k+1, $csvLine[9]);
                                    }
                                    $d['udropship_vendor'] = $vendors[$csvLine[9]];
                                }
                            }
                            $data[] = $d;
                            // UNIRGY-END
                        } else {
                            if ($csvLine[2] == '*' || $csvLine[2] == '') {
                                $zip = '';
                            } else {
                                $zip = $csvLine[2]."%";
                            }

                            $city='';
                            $zip_to = '';

                            if (!$this->_isPositiveDecimalNumber($csvLine[3]) || $csvLine[3] == '*' || $csvLine[3] == '') {
                                $exceptions[] = Mage::helper('shipping')->__('Invalid %s From "%s" in the Row #%s', $conditionFullName, $csvLine[3], ($k+1));
                            } else {
                                $csvLine[3] = (float)$csvLine[3];
                            }

                            if (!$this->_isPositiveDecimalNumber($csvLine[4])) {
                                $exceptions[] = Mage::helper('shipping')->__('Invalid %s To "%s" in the Row #%s', $conditionFullName, $csvLine[4], ($k+1));
                            } else {
                                $csvLine[4] = (float)$csvLine[4];
                            }

                            // UNIRGY-BEGIN
                            $d = array('website_id'=>$websiteId, 'dest_country_id'=>$countryId, 'dest_region_id'=>$regionId,  'dest_city'=>$city,'dest_zip'=>$zip,'dest_zip_to'=>$zip_to,  'condition_name'=>$conditionName, 'condition_from_value'=>$csvLine[3],'condition_to_value'=>$csvLine[4], 'price'=>$csvLine[5], 'delivery_type'=>$csvLine[6]);
                            if (!empty($csvLine[7])) {
                                if (is_numeric($csvLine[7])) {
                                    $d['udropship_vendor'] = $csvLine[7];
                                } else {
                                    if (empty($vendors[$csvLine[7]])) {
                                        $exceptions[] = Mage::helper('udropship')->__('Invalid Vendor on line %s (%s)', $k+1, $csvLine[7]);
                                    }
                                    $d['udropship_vendor'] = $vendors[$csvLine[7]];
                                }
                            }
                            $data[] = $d;
                            // UNIRGY-END
                        }


                        $dataDetails[] = array('country'=>$csvLine[0], 'region'=>$csvLine[1]);
                    }
                }
                //var_dump($exceptions);die;
                if (empty($exceptions)) {
                    $connection = $this->_getWriteAdapter();


                    $condition = array(
                        $connection->quoteInto('website_id = ?', $websiteId),
                        $connection->quoteInto('condition_name = ?', $conditionName),
                    );
                    $connection->delete($table, $condition);

                    foreach ($data as $k=>$dataLine) {
                        try {
                            $connection->insert($table, $dataLine);
                        } catch (Exception $e) {
                            //UNIRGY-BEGIN
                            $exceptions[] = Mage::helper('shipping')->__('Exception during insert on line %s: %s', $k+1, $e->getMessage());
                            /*
                                                        $exceptions[] = Mage::helper('shipping')->__('Duplicate Row #%s (Country "%s", Region/State "%s", City "%s", Zip From "%s", Zip To "%s", Delivery Type "%s", Value From "%s" and Value To "%s")', ($k+1), $dataDetails[$k]['country'], $dataDetails[$k]['region'], $dataLine['dest_city'], $dataLine['dest_zip'],  $dataLine['dest_zip_to'], $dataLine['delivery_type'], $dataLine['condition_from_value'], $dataLine['condition_to_value']);
                            */
                            //UNIRGY-END
                        }
                    }
                }
                if (!empty($exceptions)) {
                    throw new Exception("\n" . implode("\n", $exceptions));
                }
            }
        }
    }


    /**
     * GetCsvValues
     *
     * @param string $string    string
     * @param string $separator separator
     *
     * @return array
     */
    protected function _getCsvValues($string, $separator=",")
    {
        $elements = explode($separator, trim($string));
        for ($i = 0; $i < count($elements); $i++) {
            $nquotes = substr_count($elements[$i], '"');
            if ($nquotes %2 == 1) {
                for ($j = $i+1; $j < count($elements); $j++) {
                    if (substr_count($elements[$j], '"') > 0) {
                        // Put the quoted string's pieces back together again
                        array_splice($elements, $i, $j-$i+1, implode($separator, array_slice($elements, $i, $j-$i+1)));
                        break;
                    }
                }
            }
            if ($nquotes > 0) {
                // Remove first and last quotes, then merge pairs of quotes
                $qstr =& $elements[$i];
                $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
                $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
                $qstr = str_replace('""', '"', $qstr);
            }
            $elements[$i] = trim($elements[$i]);
        }
        return $elements;
    }

    /**
     * IsPositiveDecimalNumber
     *
     * @param int $n n
     *
     * @return int
     */
    protected function _isPositiveDecimalNumber($n)
    {
        return preg_match("/^[0-9]+(\.[0-9]*)?$/", $n);
    }
}
