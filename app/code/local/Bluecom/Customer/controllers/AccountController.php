<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category  Mage
 * @package   Mage_Customer
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license   http://www.magento.com/license/enterprise-edition
 */


require_once 'Mage/Customer/controllers/AccountController.php';

/**
 * Customer account controller
 *
 * @category Mage
 * @package  Mage_Customer
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Bluecom_Customer_AccountController extends Mage_Customer_AccountController
{

    /**
     * Check reference url is home page
     *
     * @return bool
     */
    private function _referenIsHome()
    {

        $httpRefferer = str_replace('/', '', Mage::app()->getRequest()->getServer('HTTP_REFERER'));
        $httpRefferer = str_replace('https', '', $httpRefferer);

        $baseUrl = str_replace('/', '', Mage::helper('core/url')->getHomeUrl());
        $baseUrl = str_replace('https', '', $baseUrl);

        $baseUrlDefault = str_replace('/', '', Mage::getStoreConfig('web/unsecure/base_url'));
        $baseUrlDefault = str_replace('https', '', $baseUrlDefault);

        if ($httpRefferer == $baseUrl || $httpRefferer == $baseUrlDefault) {

            return true;

        } else {

            return false;

        }

    }

    /**
     * Customer login form page
     *
     * @return mixed
     */
    public function loginAction()
    {
        parent::loginAction();
        $session = $this->_getSession();
        if ($this->_referenIsHome()) {
            $session->setBeforeAuthUrl($this->_getHelper('customer')->getAccountUrl());
        }

    }

    /**
     * Define target URL and redirect customer after logging in
     */
    protected function _loginPostRedirect()
    {
        $session = $this->_getSession();

        if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
            // Set default URL to redirect customer to
            $session->setBeforeAuthUrl($this->_getHelper('customer')->getAccountUrl());
            // Redirect customer to the last page visited after logging in
            if ($session->isLoggedIn()) {
                if (!Mage::getStoreConfigFlag(
                    Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
                )
                ) {
                    $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
                    if ($referer) {
                        // Rebuild referer URL to handle the case when SID was changed
                        $referer = $this->_getModel('core/url')
                            ->getRebuiltUrl($this->_getHelper('core')->urlDecodeAndEscape($referer));
                        if ($this->_isUrlInternal($referer)) {
                            $session->setBeforeAuthUrl($referer);
                        }
                    }
                } else if ($session->getAfterAuthUrl()) {
                    $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
                }
            } else {
                $session->setBeforeAuthUrl($this->_getHelper('customer')->getLoginUrl());
            }
        } else if ($session->getBeforeAuthUrl() == $this->_getHelper('customer')->getLogoutUrl()) {
            $session->setBeforeAuthUrl($this->_getHelper('customer')->getDashboardUrl());
        } else {
            if (!$session->getAfterAuthUrl()) {
                $session->setAfterAuthUrl($session->getBeforeAuthUrl());
            }
            if ($session->isLoggedIn()) {
                $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
            }
        }

        /**
         * Check session redirect udL
         */
        $redirectUrl = $session->getBeforeAuthUrl(true);

        $udLParam = $this->getRequest()->getParam('udL');

        $helperPage = Mage::helper('bluecom_page');
        $correctUdL = $helperPage->checkCorrectUdLParam($udLParam);

        // check udL param is special category
        if ($correctUdL) {

            $urlCheck = $helperPage->changeParamUdLCorrect($redirectUrl, $udLParam);

            if ($urlCheck != '') {
                $redirectUrl = $urlCheck;
            }
        }

        $this->_redirectUrl($redirectUrl);
    }

}