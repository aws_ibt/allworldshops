<?php
/**
 * Install 0.1.0
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$cnWebsiteId = Mage::app()->getStore('cn_en')->getWebsiteId();
Mage::getModel('core/config')->saveConfig('design/theme/template', 'aws_cn', 'websites', $cnWebsiteId);

$jpWebsiteId = Mage::app()->getStore('jp_en')->getWebsiteId();
Mage::getModel('core/config')->saveConfig('design/theme/template', 'aws_jp', 'websites', $jpWebsiteId);

$krWebsiteId = Mage::app()->getStore('ko_en')->getWebsiteId();
Mage::getModel('core/config')->saveConfig('design/theme/template', 'aws_kr', 'websites', $krWebsiteId);

$myWebsiteId = Mage::app()->getStore('my_en')->getWebsiteId();
Mage::getModel('core/config')->saveConfig('design/theme/template', 'aws_my', 'websites', $myWebsiteId);

$installer->endSetup();