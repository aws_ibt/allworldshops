<?php
/**
 * Upgrade 0.1.1
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$firstname2 = array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'First Name 2',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1
);

$installer->addAttribute('customer', 'firstname2', $firstname2);
$installer->addAttribute('customer_address', 'firstname2', $firstname2);

Mage::getSingleton('eav/config')->getAttribute('customer', 'firstname2')
    ->setData('used_in_forms', array('checkout_register','customer_account_create','customer_account_edit', 'adminhtml_checkout'))
    ->save();

Mage::getSingleton('eav/config')->getAttribute('customer_address', 'firstname2')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit'))
    ->save();

$lastname2 = array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'Last Name 2',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1
);

$installer->addAttribute('customer', 'lastname2', $lastname2);
$installer->addAttribute('customer_address', 'lastname2', $lastname2);

Mage::getSingleton('eav/config')->getAttribute('customer', 'lastname2')
    ->setData('used_in_forms', array('checkout_register','customer_account_create','customer_account_edit', 'adminhtml_checkout'))
    ->save();

Mage::getSingleton('eav/config')->getAttribute('customer_address', 'lastname2')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit'))
    ->save();

$installer->endSetup();