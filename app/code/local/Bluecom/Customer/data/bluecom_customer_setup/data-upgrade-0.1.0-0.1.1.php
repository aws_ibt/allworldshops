<?php
/**
 * Upgrade 0.1.0
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$installer->updateAttribute('customer', 'lastname', 'is_required', 0);
$installer->updateAttribute('customer_address', 'lastname', 'is_required', 0);

$cnWebsiteId = Mage::app()->getStore('cn_en')->getWebsiteId();
Mage::getModel('core/config')->saveConfig('design/theme/skin', 'aws_cn', 'websites', $cnWebsiteId);
Mage::getModel('core/config')->saveConfig('design/theme/default', 'aws_cn', 'websites', $cnWebsiteId);

$jpWebsiteId = Mage::app()->getStore('jp_en')->getWebsiteId();
Mage::getModel('core/config')->saveConfig('design/theme/skin', 'aws_jp', 'websites', $jpWebsiteId);
Mage::getModel('core/config')->saveConfig('design/theme/default', 'aws_jp', 'websites', $jpWebsiteId);

$krWebsiteId = Mage::app()->getStore('ko_en')->getWebsiteId();
Mage::getModel('core/config')->saveConfig('design/theme/skin', 'aws_kr', 'websites', $krWebsiteId);
Mage::getModel('core/config')->saveConfig('design/theme/default', 'aws_kr', 'websites', $krWebsiteId);

$myWebsiteId = Mage::app()->getStore('my_en')->getWebsiteId();
Mage::getModel('core/config')->saveConfig('design/theme/skin', 'aws_my', 'websites', $myWebsiteId);
Mage::getModel('core/config')->saveConfig('design/theme/default', 'aws_my', 'websites', $myWebsiteId);

$installer->endSetup();