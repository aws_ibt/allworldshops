#!/bin/sh
CWD=$(pwd)
rm -fr $CWD/app/code/community/Innoexts/AdvancedPricingEnterprise/
rm -f $CWD/adminhtml/default/default/layout/innoexts/advancedpricingenterprise.xml
rm -f $CWD/app/etc/modules/Innoexts_AdvancedPricingEnterprise.xml
rm -f $CWD/app/locale/en_US/Innoexts_AdvancedPricingEnterprise.csv
rm -fr $CWD/skin/frontend/enterprise/default/innoexts/customerlocator/
rm -fr $CWD/skin/frontend/enterprise/iphone/innoexts/customerlocator/
rm -fr $CWD/sql/Innoexts/AdvancedPricingEnterprise/
rm -fr $CWD/sh/Innoexts/AdvancedPricingEnterprise/
rm -f $CWD/var/connect/Innoexts_AdvancedPricingEnterprise.xml
