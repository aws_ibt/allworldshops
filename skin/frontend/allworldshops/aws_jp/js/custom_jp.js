$j(document).ready(function(){
    //Validate JP
    Validation.addAllThese([
        ['validate-katakana', '全角カタカナでのご入力をお願いいたします.', function(v) {
            return Validation.get('IsEmpty').test(v) || !(/[^ァ-ン　\s]+/.test(v));

        }],
        ['validate-katakana02', '全角カタカナでのご入力をお願いいたします.', function(v) {
            return Validation.get('IsEmpty').test(v) || !(/[^ァ-ン　\s]+/.test(v));

        }],
        ['validate-zip-jp', 'Please enter a valid zip code. For example 906-1234.', function (v) {
            return Validation.get('IsEmpty').test(v) || /(^\d{3}-\d{4}$)/.test(v);
        }]
    ]);
});