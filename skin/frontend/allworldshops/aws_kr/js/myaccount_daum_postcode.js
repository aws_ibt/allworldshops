var execDaumPostcode = function(){
    new daum.Postcode({
        oncomplete: function(data) {
            // Popup in part to write code to execute when you click on a search result item.

            // The combination of the address according to the rules of the exposed street address.
            var fullRoadAddr = data.roadAddress;
            var extraRoadAddr = '';

            // If you add beopjeongdong people. (Beopjeongri excluded)
            if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                extraRoadAddr += data.bname;
            }

            if(data.buildingName !== '' && data.apartment === 'Y'){
                extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }

            if(extraRoadAddr !== ''){
                extraRoadAddr = ' (' + extraRoadAddr + ')';
            }

            if(fullRoadAddr !== ''){
                fullRoadAddr += extraRoadAddr;
            }

            // Put your zip code and address information into the appropriate fields.
            document.getElementById('zip').value = data.zonecode;
            document.getElementById('street_1').value = fullRoadAddr;
            //document.getElementById('street_1').setAttribute('readonly','readonly');
            //document.getElementById('street_1').setStyle({
            //    'cursor': 'no-drop'
            //});
            document.getElementById('street_2').value = data.jibunAddress;
            document.getElementById('city').value = data.sido;
            document.getElementById('country_id').value = 'KR';

            // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
            /*if(data.autoRoadAddress) {
                //예상되는 도로명 주소에 조합형 주소를 추가한다.
                var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
                document.getElementById('guide').innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

            } else if(data.autoJibunAddress) {
                var expJibunAddr = data.autoJibunAddress;
                document.getElementById('guide').innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

            } else {
                document.getElementById('guide').innerHTML = '';
            }*/
        }
    }).open();
}