$j(document).ready(function(){
    $j('select[name="billing[country_id]"]').addClass('country-tele');
    $j('select[name="shipping[country_id]"]').addClass('country-tele');
    $j('select[name="country_id"]').addClass('country-tele');

    $j('input[name="billing[telephone]"]').addClass('telephone-input');
    $j('input[name="shipping[telephone]"]').addClass('telephone-input');
    $j('input[name="telephone"]').addClass('telephone-input');

    var teleInput = $j('.telephone-input');
    var contryInput = $j('.country-tele');
    var teleValue = $j('.telephone-input').val();

    var countryData = $j.fn.intlTelInput.getCountryData();
    $j.each(countryData, function(i, country) {
        country.name = country.name.replace(/.+\((.+)\)/,"$1");
    });
    teleInput.intlTelInput({
        dropdownContainer: "body",
        //autoHideDialCode: false,
        allowDropdown: true,
        autoPlaceholder: true,
        nationalMode: false,
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            //lookup ip local Contact page
            if($j('body').hasClass('contacts-index-index')){
                $j.get('http://ipinfo.io', function() {}, "json").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            }else{
                //look up country code
                if(contryInput.val()){
                    contryInput.change(function () {
                        teleValue = contryInput.val();
                        callback(teleValue);
                    }).change();
                }
            }
        },
        utilsScript: '',
    });
    $j(window).resize(function(){
        $j('html').trigger('click');
    });
    contryInput.change(function () {
        teleInput.intlTelInput("setCountry", $j(this).val());
    }).change();
});

