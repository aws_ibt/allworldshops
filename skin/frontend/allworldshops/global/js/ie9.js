$j(document).ready(function () {
        //ie9 placeholder
        $j('[placeholder]').focus(function() {
            var input = $j(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function() {
            var input = $j(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        }).blur().parents('form').submit(function() {
            $j(this).find('[placeholder]').each(function() {
                var input = $j(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            })
        });
        //trick click paging ie
        $j('.pages li.btn-prev a,.pages li.btn-next a.next').click(function(e){
            e.preventDefault();
        })
});
