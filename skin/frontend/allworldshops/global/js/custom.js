$j(document).ready(function(){
    //if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
    //    $j('input, select').bind('click', function () {
    //        var notzoomInput = $j('input, select');
    //        notzoomInput.css({'font-size':"16px"});
    //    })
    //}

    //filter coutry banner
    if($j('.slideshow-container').length){
        var chinaHasclass = $j('body').hasClass('source-30');
        var australiaHasclass = $j('body').hasClass('source-31');
        var new_zealandHasclass = $j('body').hasClass('source-32');
        if(!chinaHasclass && !australiaHasclass && !new_zealandHasclass){
            $j('.slideshow-container .ban-china,.slideshow-container .ban-aus,.slideshow-container .ban-nz').remove();
        }
        if(chinaHasclass){
            $j('.slideshow-container .ban-all,.slideshow-container .ban-aus,.slideshow-container .ban-nz').remove();
        }
        if(australiaHasclass){
            $j('.slideshow-container .ban-all,.slideshow-container .ban-china,.slideshow-container .ban-nz').remove();
        }
        if(new_zealandHasclass){
            $j('.slideshow-container .ban-all,.slideshow-container .ban-aus,.slideshow-container .ban-china').remove();
        }
    }

    //trigger click
    $j(".products-grid .actions").click(function(e){
        var target = $j(e.target);
        if(!target.is(':button') && !target.is('span')){
            window.location = $j(this).find('.add-to-links li:last-child a').attr('href');
        }
    });

    //
    $j('.col-2.rma-view .box-content > br:nth-of-type(1)').remove();
    //match Height
    $j('#upsell-product-table .item').matchHeight();

    $j('.product-image-thumbs').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2
            },
            320: {
                items: 2
            },
            480: {
                items: 3
            },
            768: {
                items: 3
            },
            769: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200:{
                item:3
            }
        },
        nav:true
    });

    if($j('.product-image-thumbs .owl-stage-outer .owl-stage .owl-item').length < 3){
        $j('.product-image-thumbs .owl-controls').remove();
    }

    if($j('.widget-hot-products').length == ''){
        $j('.hot-product').css('display','none')
    }
    //get selected class if roll back browser when checkout by alipay
    $j('.checkout-cart-index #select-language').val($j('.checkout-cart-index #select-language option[selected="selected"]').val());
    $j('.checkout-cart-index #select-currency').val($j('.checkout-cart-index #select-currency option[selected="selected"]').val());
    //
    $j('.catalogsearch-result-index #select-language').val($j('.catalogsearch-result-index #select-language option[selected="selected"]').val());
    $j('.catalogsearch-result-index #select-currency').val($j('.catalogsearch-result-index #select-currency option[selected="selected"]').val());
    $j('.catalogsearch-result-index #select-ui').val($j('.catalogsearch-result-index #select-ui option[selected="selected"]').val());
    $j('.catalogsearch-result-index #select-box').val($j('.catalogsearch-result-index #select-box option[selected="selected"]').val());
    //
    $j('.catalog-category-view #select-language').val($j('.catalog-category-view #select-language option[selected="selected"]').val());
    $j('.catalog-category-view #select-currency').val($j('.catalog-category-view #select-currency option[selected="selected"]').val());
    $j('.catalog-category-view #select-ui').val($j('.catalog-category-view #select-ui option[selected="selected"]').val());
    $j('.catalog-category-view #select-box').val($j('.catalog-category-view #select-box option[selected="selected"]').val());


    try {
            $j("#select-language, #select-currency").msDropDown();
        } catch(e) {
    }

    enquire.register('(max-width: ' + bp.medium + 'px)', {
        match: function () {
            $j('.skips-country-container').prepend($j('.shop-form-country'));
            $j('.skip-links .account-cart-wrapper').append($j('.header-minicart'));
            if($j('#nav-special').length){
                $j('#header-nav').prepend($j('#nav-special'));
            }
            //
            if($j('.block-content-actions .currently').length){
                $j('.block-content #narrow-by-list').css({
                   'margin-top' : '10px',
                });
                $j('.block-layered-nav .block-content-actions').css({
                    'margin-bottom' : '10px',
                    'border' : '1px solid',
                    'background' : '#fff'
                });
                $j('.block-layered-nav').css({
                    'background' : 'transparent'
                });
            }
            //
            $j('.block-subtitle--filter').on('click', function () {
                if(!$j('.block-subtitle--sort').hasClass('no-display')){
                    $j('.block-subtitle--sort').toggleClass('no-display');
                    $j('#narrow-by-list').addClass('appear');
                }else {
                    $j('.block-subtitle--sort').removeClass('no-display');
                    $j('#narrow-by-list').removeClass('appear');
                }
                if(!$j(this).hasClass('active')){
                    $j('.block-subtitle--sort').removeClass('no-display');
                    $j('#narrow-by-list').removeClass('appear');
                }else{
                    $j('#narrow-by-list').addClass('appear');
                }
            });
           $j('.block-subtitle--sort').on('click', function () {
               if(!$j('.block-subtitle--sort').hasClass('active')){
                   $j(this).toggleClass('active');
                   $j('.category-products > .toolbar').addClass('appear');
               }else {
                   $j('.block-subtitle--sort').removeClass('active');
                   $j('.category-products > .toolbar').removeClass('appear');
               }
           //     if($j('.category-products > .toolbar').hasClass('appear')){
           //         $j('.block-layered-nav--no-filters .block-content #narrow-by-list, .block-layered-nav .block-content #narrow-by-list').addClass('no-display');
           //     }
           // });
           //  $j('.block-subtitle--filter').on('click', function () {
           //      if(!$j('.block-layered-nav--no-filters .block-content #narrow-by-list, .block-layered-nav .block-content #narrow-by-list').hasClass('no-display')) {
           //          $j('.category-products > .toolbar').removeClass('appear');
           //          $j('.block-subtitle--sort.active').removeClass('active');
           //      }
            });
            $j('.messages').css({
               'display' : 'block',
            });
        },
        unmatch: function () {
            $j('.store-country-container').prepend($j('.shop-form-country'));
            $j('.top-link-header').prepend($j('.header-minicart'));
            if($j('#nav-special').length){
                $j('.nav-special-category').prepend($j('#nav-special'));
            }
        }
    });
    $j('.catalogsearch-result-index .main-content-category .page-title').insertBefore('.catalogsearch-result-index .main-content-category');
    $j('.back-top').click(function(){
        $j('body, html').animate({
            scrollTop: 0
        },300);
        return false;
    });
    if (Modernizr.mq('(min-width: 769px)')) {
        $j('.nav-primary li.level0 ul.level0').css('min-height',  $j('#nav').height() + 'px');
        // arange ul child in navigation menu
        var nth3, hnth3, hnthprev;
        $j('.nav-primary li.level0.parent').mouseover(function(){
            $j('.nav-primary li.level0.parent.menu-active ul.level0 li.level1:nth-child(3n)').each(function(){
                nth3 = $j(this);
                hnth3 = nth3.height();
                hnthprev = nth3.prev('li').height();
                if(hnth3 < hnthprev && hnthprev <= hnth3*2)
                    nth3.css('height', hnthprev + 'px');
            });
        });
        // set position for Child of menu specialty
        $j('#nav-special li.level0.parent').on('mouseover touchend', function(e){
            var offsetLi = $j(this).offset().left;
            var lGroup =  $j(this).find('.wrapper-level0').find('.group').length;

            if(lGroup>1){
               $j(this).find('.wrapper-level0').css('width',240 * lGroup + 'px');
            }
             $j(this).find('.wrapper-level0').removeClass('fl-right');
            $j(this).removeAttr('style');
            var wGroup =  $j(this).find('.wrapper-level0').width();
            if(offsetLi > $j(document).width()/2 && wGroup < $j(document).width()/2){
                //$j(this).find('.wrapper-level0').attr('style',  'right:0; left: auto');
                $j(this).find('.wrapper-level0').addClass('fl-right');
            }
            if(wGroup >  $j(document).width()/2 && ($j(document).width() - Math.floor(offsetLi)) <= wGroup+4){
                $j(this).find('.wrapper-level0').addClass('fl-right');
                if (offsetLi < wGroup/2) {
                    $j(this).css('position','static');
                }
            }

        });
        //set height for group sitemap
        $j('.page-sitemap .group').each(function(){
            var maxH = 0;
           $j(this).find('.level-0').each(function(){
               var thisH = $j(this).outerHeight();
               if(thisH > maxH){
                   maxH = thisH;
               }
           });
            $j(this).find('.level-0').css('min-height',maxH + 'px');
        });
    }
    changeSelect();
    loadInputLabel();
    /*toggle content*/
    if($j('.collapse-wrapper').length > 0){
        var wrapper_bt = $j('.collapse-wrapper');
        var links = wrapper_bt.find('.level-0');
        var link_title = links.find('.link-title');
        var block_content = links.find('.collapse-content');
        var groups = new Array(link_title, block_content);
        function toggleClasses(clickedItem, group) {
            var index = group.index(clickedItem);
            var i;
            for (i = 0; i < groups.length; i++) {
                groups[i].removeClass('current');
                groups[i].eq(index).addClass('current');
            }
            block_content.each(function () {
                $j(this)[
                    (wrapper_bt.hasClass('accordion-open') && $j(this).hasClass('current')) ? 'slideDown' : 'slideUp'
                    ]();
            });
        }

        //Toggle on tab (dt) click.
        link_title.on('click', function (e) {
            if (Modernizr.mq('(max-width: 768px)')) {
                e.preventDefault();
                if ($j(this).hasClass('current') && wrapper_bt.hasClass('accordion-open')) {
                    wrapper_bt.removeClass('accordion-open');
                } else {
                    wrapper_bt.addClass('accordion-open');
                }
                toggleClasses($j(this), link_title);
            }
        });
        $j('.link-title.first-title').click();
    }
    $j('body').on('touchend', function (event) {
        var element = $j(event.target);
        if(element.parents('.skip-link').length == 1 )
        {
            return;
        }
        if($j('body')[0].clickDisabled == 1)
            return;
        var parent = $j('.skip-content');
        var link = $j('.skip-link');

        if ((element.parents('.skip-content').length == 0)) {
            parent.removeClass('skip-active');
            link.removeClass('skip-active');
        }

    });

    if (Modernizr.mq('(max-width: 768px)')) {
        $j(".footer-links h3").on("click", function(e){
            if($j(this).parent().has("ul")) {
                e.preventDefault();
            }

            if(!$j(this).hasClass("open")) {
                // hide any open menus and remove all other classes
                $j("#nav li ul").slideUp(350);
                $j("#nav li a").removeClass("open");

                // open our new menu and add the open class
                $j(this).next("ul").slideDown(350);
                $j(this).addClass("open");
            }

            else if($j(this).hasClass("open")) {
                $j(this).removeClass("open");
                $j(this).next("ul").slideUp(350);
            }
        });
        $j('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 3,
                    nav: true
                },
                768: {
                    items: 3,
                    nav: true
                }
            }
        });
        // Configure/customize these variables.
        var showChar = 200;  // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "Show more >";
        var lesstext = "Show less";


        $j('.short-description-view .more').each(function() {
            var content = $j(this).html();

            if(content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><div class="morecontent"><span>' + h + '</span><div class="show-more"><a href="" class="morelink">' + moretext + '</a></div></div';

                $j(this).html(html);
            }

        });

        $j(".morelink").click(function(){
            if($j(this).hasClass("less")) {
                $j(this).removeClass("less");
                $j(this).html(moretext);
            } else {
                $j(this).addClass("less");
                $j(this).html(lesstext);
            }
            $j(this).parent().prev().toggle();
            $j(this).prev().toggle();
            return false;
        });
    }
    $j('.change-country select').change(function(){
        changeSelect();
    });
    jQuery(".product-price-info").prepend($j("#price-info-bottom"));


});
$j(window).resize(function(){
    if (Modernizr.mq('(min-width: 769px)')) {
        $j('.nav-primary li.level0 ul.level0').css('min-height',  $j('#nav').height() + 'px');
    }
    if (Modernizr.mq('(max-width: 768px)')) {
        $j('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 3,
                    nav: true
                },
                768: {
                    items: 3,
                    nav: true
                }
            }
        });

    }
});
function changeSelect(){
    if($j('.change-state select').css('display')== 'none'){
        $j('.change-state select').parent('.select-styled').css('display','none');
    }
    else {
        $j('.change-state select').parent('.select-styled').css('display','inline-block');
    }
}

function loadInputLabel(){
    $j('.custom-select-wrapper select').each(function(){
        _this = $j(this), _val = _this.find('option:selected').text(), _label = _this.parent().find('.select-data');
        _label.text(_val);
    })
    $j('.custom-select-wrapper select').change(function(){
        _this = $j(this), _val = _this.find('option:selected').text(), _label = _this.parent().find('.select-data');
        _label.text(_val);
    })
}