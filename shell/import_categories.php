<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

require_once 'abstract.php';

/**
 * Magento import category Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Shell_ImportCategories extends Mage_Shell_Abstract
{
    
    public function __construct() {
        parent::__construct();
        // Time limit to infinity
        set_time_limit(0);
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        if ($this->getArg('import')) {
            $this->importCategory();
        }
    }

    /**
     * Import Category
     */
    private function importCategory()
    {
        $importPath = $this->_getRootPath().'var'.DS.'import'.DS.'category'.DS;

        if (!file_exists($importPath)) {
            mkdir($importPath, 0777, true);
        }

        if ($this->getArg('file_name') != '') {
            $fileName = $this->getArg('file_name').'.csv';
        } else {
            $fileName = '';
        }

        $file = fopen($importPath.$fileName, 'r');
        if($file === false){
            echo "importCategory: File does not exist ".$importPath.$fileName.PHP_EOL;
            return;
        }
        $isVerbose = $this->getArg('v') ? true : false;
        $columnNames = fgetcsv($file);
        $counter = $counterSkipped = $counterRewrited = $counterCreated = 0;

        try {
            $this->_beforeProcess();
            /* @var $category Mage_Catalog_Model_Category */
            $category = Mage::getModel('catalog/category');

            $rootId = Mage::app()->getDefaultStoreView()->getRootCategoryId();

            $arrCategoriesCurrent = $this->getArrCategoryName();

            while (($lineAr = fgetcsv($file)) !== FALSE) {
                if (empty($lineAr)) {
                    continue;
                }
                $lineAr = array_combine($columnNames, $lineAr);
                if ($this->isCategoryExists($lineAr, $arrCategoriesCurrent, $isVerbose) === true) {
                    if ($isVerbose) {
                        $mess = 'Category with ' . $lineAr['name'] . ' name was skipped in file '.$fileName.'. The category exits' . PHP_EOL;
                        echo $mess;
                        Mage::log($mess, null, 'log_import_category.log', true);
                    }
                    $counter++;
                    $counterSkipped++;
                    continue;
                } else {
                    try {
                        if ($this->getArg('parent_id') != '') {
                            $parentId = $this->getArg('parent_id');
                        } else {
                            $parentId = $rootId;
                        }

                        $data = $this->_prepareCategoryData($lineAr['name']);
                        /* @var parent Mage_Catalog_Model_Category */
                        $parent = Mage::getModel('catalog/category')->load($parentId);
                        $category->setData($data)
                            ->setAttributeSetId($category->getDefaultAttributeSetId())
                            ->setStoreId(0)
                            ->setPath(implode('/', $parent->getPathIds()))
                            ->setParentId($parentId)
                            ->save();

                    } catch (Mage_Core_Exception $e) {
                        $mess = 'Exception: Category with Name ' . $lineAr['name'] . ' in file '.$fileName.'. Message:';
                        $mess .=Zend_Debug::dump($e->getMessage());
                        print_r($mess);
                        Mage::log($mess, null, 'log_import_category.log', true);
                    }

                    if ($isVerbose) {
                        $mess = 'Category with ' . $lineAr['name'] . ' name in file '.$fileName.' was created.' . PHP_EOL;
                        print_r($mess);
                        Mage::log($mess, null, 'log_import_category.log', true);

                    }

                    $counterCreated++;
                }
                $counter++;
            }
            $this->_afterProcess();
        } catch (Exception $e) {
            Mage::logException($e);
        }

        echo 'Category import summary:'.PHP_EOL;
        echo $counterSkipped.' pages skipped.'.PHP_EOL;
        echo $counterCreated.' pages created.'.PHP_EOL;
        echo $counter.' urls from csv processed.'.PHP_EOL.PHP_EOL;
        fclose($file);
    }

    /**
     * Process some stuff before running categories import.
     */
    protected function _beforeProcess()
    {
        // Needed for correct creation of categories url rewrites.
        $processes = Mage::getSingleton('index/indexer')->getProcessesCollection();
        $processes->walk('setMode', array(Mage_Index_Model_Process::MODE_MANUAL));
        $processes->walk('save');
    }
    /**
     * Process some stuff after running categories import.
     */
    protected function _afterProcess()
    {
        $processes = Mage::getSingleton('index/indexer')->getProcessesCollection();
        $processes->walk('setMode', array(Mage_Index_Model_Process::MODE_REAL_TIME));
        $processes->walk('save');
        echo PHP_EOL . 'Reindexing all...' . PHP_EOL;
        $processes->walk('reindexAll');
    }


    /**
     * Prepares default data of new category with specified name.
     *
     * @param string $name
     * @return array
     */
    protected function _prepareCategoryData($name)
    {
        return array(
            'name'              => trim($name),
            'is_active'         => 1,
            'include_in_menu'   => 1,
            'is_anchor'         => 0,
            'url_key'           => '',
            'description'       => '',
        );
    }

    /**
     * Check name category exists
     *
     * @param $lineData
     * @param $arrCategoriesCurrent
     * @param bool $isVerbose
     *
     * @return bool
     */
    private function isCategoryExists($lineData, $arrCategoriesCurrent, $isVerbose = false)
    {
        $name = preg_replace('#[^\w()/.%\-&]#', '', trim($lineData['name']));

        $result = false;

        foreach ($arrCategoriesCurrent as $categoryName) {
            $category = preg_replace('#[^\w()/.%\-&]#', '', trim($categoryName));
            if (strcasecmp($name, $category)==0) {
                $result = true;
                $mess = $lineData['name'] .' category the same name category current '. $categoryName .'.';
                if ($isVerbose) {
                    print_r($mess);
                }
                Mage::log($mess, null, 'log_import_category.log', true);
                break;
            }
        }

        return  $result;
    }

    /**
     * Get all name categories
     *
     * @return array
     *
     * @throws Mage_Core_Exception
     */
    private function getArrCategoryName()
    {
        $collection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name');
        $arr = [];
        foreach ($collection as $category) {
            if ($category->getName() != '') {
                $arr[] = $category->getName();
            }
        }
        return $arr;
    }

    /**
     * Usage instructions
     * 
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
    This file (import_categories.php) must be placed in "shell" folder.
    Before import place csv files that were obtained during export process into var/import/category/
    Usage:  php -f import_category.php -- [options]
    Import options:
    import                          Start import url.
        file_name                   Run data open in this file for create/update category
        -v                          Verbose option. Give you more information about import process
    Other options
    help, h                         This help
    Examples:
    php shell/import_category.php -- import --file_name filename                      //Import filename.csv
    php shell/import_category.php -- import --file_name filename --v                  //Import file filename.csv . Show detail run script
    php shell/import_category.php -- import --file_name filename --v --parent_id 3    //Import file filename.csv with category have parent category is 3. Show detail run script
USAGE;
    }
   
}

$shell = new Mage_Shell_ImportCategories();
$shell->run();
