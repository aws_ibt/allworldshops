<?php
require_once '../abstract.php';
umask(0);

class Mage_Shell_SortCategoryAlphabet extends Mage_Shell_Abstract
{

    public function __construct() {
        parent::__construct();
        // Time limit to infinity
        set_time_limit(0);
    }

    public function run()
    {
        if ($this->getArg('sort') && $this->getArg('category')) {
            $this->sortCategoriesAlphabet($this->getArg('category'));
        } else {
            echo 'Category parent is null';
        }
    }

    /**
     * Sort category by alphabet
     *
     * @param $categoryId
     */
    protected function sortCategoriesAlphabet($categoryId)
    {
        if ($this->checkCategoryExists($categoryId)) {
            $conn = Mage::getSingleton('core/resource')->getConnection('core_write');
            $this->orderCategories($categoryId, $conn);
        } else {
            echo 'category parent not exists';
        }
    }

    protected function orderCategories($parentId, $conn)
    {
        //The position field is in the main category table
        //add prefix if you have one
        $table = 'catalog_category_entity';
        //get the children for a specific category id
        $collection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('name')
            ->addFieldToFilter('parent_id', $parentId)
            ->addAttributeToSort('name','ASC');
        $categories = array();
        $sizeCollection = count($collection->getData());

        if ($sizeCollection>0) {
            //set their position
            $position = 1;
            foreach ($collection as $category) {
                $q = "UPDATE {$table} SET `position` = {$position} where `entity_id` = {$category->getId()}";
                $conn->query($q);
                echo ($category->getName() .' update position ' . $position. "\n");
                $position++;
                //sort the current category children by calling the same function recursively
                $this->orderCategories($category->getId(), $conn);
            }

        }
    }

    /**
     * Check category is exists
     *
     * @param $categoryId
     *
     * @return bool
     */
    protected function checkCategoryExists($categoryId)
    {
        $category = Mage::getModel('catalog/category')->load($categoryId);

        if ($category->getId()) {
            return true;
        }
        return false;
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f sortCategoryAlphabet.php -- [options]

  --sort                        Key Sort Category 
  --category <category_id>      The parent category sort by alphabet
  help                          This help

USAGE;
    }

}

$shell = new Mage_Shell_SortCategoryAlphabet();
$shell->run();