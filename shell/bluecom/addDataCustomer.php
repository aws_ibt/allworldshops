<?php
require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'abstract.php';

class Mage_Shell_AddDataCustomer extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        if ($option = $this->getArg('import')) {
            switch ($option) {
                case 'WebItCash':
                    $this->addWebItCash();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Add Webitcash of customer (store credit)
     */
    protected function addWebItCash()
    {
        $importPath = $this->_getRootPath().'shell'.DS.'bluecom'.DS.'Data'.DS;

        if (!file_exists($importPath)) {
            mkdir($importPath, 0777, true);
        }

        $fileName = 'customers-data.csv';

        $file = fopen($importPath.$fileName, 'r');

        if($file === false){
            echo "AddDataCustomer: File does not exist ".$importPath.$fileName.PHP_EOL;
            return;
        }

        $isVerbose = $this->getArg('v') ? true : false;
        $columnNames = fgetcsv($file);
        $counter = $counterSkipped = $counterCreated = 0;

        try {
            while (($lineAr = fgetcsv($file)) !== FALSE) {
                if (empty($lineAr)) {
                    continue;
                }
                $lineAr = array_combine($columnNames, $lineAr);
                $customerModel = Mage::getModel('customer/customer');

                $customer = $customerModel->loadByEmail(trim($lineAr['email']));

                if ($customer->getId()) {
                    $balanceModel = Mage::getModel('enterprise_customerbalance/balance');
                    $websiteId = $this->getArg('websiteId');
                    if (!$websiteId) {
                        $websiteId = Mage::app()->getStore()->getWebsiteId();
                    }
                    $balanceModel->setCustomerId($customer->getId())
                        ->setWebsiteId($websiteId)
                        ->loadByCustomer();

                    $current_balance = $balanceModel->getAmount();
                    $comment = 'Sync Data import Webitcash of customer.';
                    $amount_to_be_added = $lineAr['webitcash_amount'];
                    // add store credit
                    $balanceModel->setAmount($current_balance);
                    $balanceModel->setAmountDelta($amount_to_be_added);
                    $balanceModel->setUpdatedActionAdditionalInfo($comment);
                    $balanceModel->setHistoryAction(1); // 1= updated
                    $balanceModel->save();
                    $counterCreated++;
                    if ($isVerbose) {
                        echo 'Customer ' . trim($lineAr['email']) . 'added webitcash to Allworldshop site.'.PHP_EOL;
                    }
                } else {
                    $counterSkipped++;
                    if ($isVerbose) {
                        echo 'Customer ' . trim($lineAr['email']) . 'not exists.'.PHP_EOL;
                    }
                }
                $counter++;
            }

        } catch (Exception $e) {
            Mage::logException($e);
        }

        echo 'Customer add data summary:'.PHP_EOL;
        echo $counterSkipped.' row skipped.'.PHP_EOL;
        echo $counterCreated.' row add.'.PHP_EOL;
        echo $counter.' item from csv processed.'.PHP_EOL.PHP_EOL;

        fclose($file);

    }

     /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f addDataCustomer.php -- [options]

  import            option import (WebItCash, )
  -v                Verbose option. Give you more information about import process
  websiteId         website id to import
  help              This help

USAGE;
    }
}

$shell = new Mage_Shell_AddDataCustomer();
$shell->run();